package si.mk.kasm;

import si.mk.kasm.translators.OpCode;
import junit.framework.TestCase;

public class TestOpCode extends TestCase {

    public void testParseDb() {
        /*
        OpCode opCode = new OpCode("12,0, 13,14 ,01010B , 9FH, 23 - 4, ABF-2,\"ABCD\", \"A\", 'B', 23",
                                   OpCode.ArgumentSize.BITS_8_UNSIGNED);
        System.out.println(opCode.toString());
        assertEquals("mc: 12, 0, 13, 14, 10, 159, 0, 0, 65, 66, 67, 68, 65, 66, 23, \n" + 
                     "Arg: byteIdx = 0, expr: 12, isEvaluated: true\n" +
                     "Arg: byteIdx = 1, expr: 0, isEvaluated: true\n" +
                     "Arg: byteIdx = 2, expr: 13, isEvaluated: true\n" +
                     "Arg: byteIdx = 3, expr: 14, isEvaluated: true\n" +
                     "Arg: byteIdx = 4, expr: 01010B, isEvaluated: true\n" +
                     "Arg: byteIdx = 5, expr: 9FH, isEvaluated: true\n" +
                     "Arg: byteIdx = 6, expr: 23 - 4, isEvaluated: false\n" +
                     "Arg: byteIdx = 7, expr: ABF-2, isEvaluated: false\n" +
                     "Arg: byteIdx = 14, expr: 23, isEvaluated: true\n", opCode.toString());

        opCode = new OpCode("4, ABF-2,\"ABCD\", \"A\", 'B'",
                            OpCode.ArgumentSize.BITS_8_UNSIGNED);
        System.out.println(opCode.toString());
        assertEquals("mc: 4, 0, 65, 66, 67, 68, 65, 66, \n" + 
                     "Arg: byteIdx = 0, expr: 4, isEvaluated: true\n" +
                     "Arg: byteIdx = 1, expr: ABF-2, isEvaluated: false\n", opCode.toString());

        opCode = new OpCode("4, ABF-2,\"ABC'\", \"A\", 'B'",
                            OpCode.ArgumentSize.BITS_8_UNSIGNED);
        System.out.println(opCode.toString());
        assertEquals("mc: 4, 0, 65, 66, 67, 39, 65, 66, \n" + 
                     "Arg: byteIdx = 0, expr: 4, isEvaluated: true\n" +
                     "Arg: byteIdx = 1, expr: ABF-2, isEvaluated: false\n", opCode.toString());

        opCode = new OpCode("4, ABF-2,'ABC\"D', \"A\", \"B\"",
                            OpCode.ArgumentSize.BITS_8_UNSIGNED);
        System.out.println(opCode.toString());
        assertEquals("mc: 4, 0, 65, 66, 67, 34, 68, 65, 66, \n" + 
                     "Arg: byteIdx = 0, expr: 4, isEvaluated: true\n" +
                     "Arg: byteIdx = 1, expr: ABF-2, isEvaluated: false\n", opCode.toString());

        opCode = new OpCode("4, ABF-2,'AB  C\"D', \"A \", \" B\", \"     \"",
                            OpCode.ArgumentSize.BITS_8_UNSIGNED);
        System.out.println(opCode.toString());
        assertEquals("mc: 4, 0, 65, 66, 32, 32, 67, 34, 68, 65, 32, 32, 66, 32, 32, 32, 32, 32, \n" + 
                     "Arg: byteIdx = 0, expr: 4, isEvaluated: true\n" +
                     "Arg: byteIdx = 1, expr: ABF-2, isEvaluated: false\n", opCode.toString());

        opCode = new OpCode("\"     \"", OpCode.ArgumentSize.BITS_8_UNSIGNED);
        System.out.println(opCode.toString());
        assertEquals("mc: 32, 32, 32, 32, 32, \n", opCode.toString());
        */
    }
}
