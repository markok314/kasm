package si.mk.kasm;


import junit.framework.TestCase;

import org.junit.Before;

import si.mk.kasm.translators.TranslatorBase;

public class TestKAsm extends TestCase {

    @Before
    public void setUp() throws Exception {
    }

    
    public void testGetIndexedAddressingArgs() {
        // TranslatorBase kasm = new TranslatorBaseZ80();
        /*
        IndexedAddressingArgs args = kasm.getIndexedAddressingArgs("(IX+3)");
        assertEquals("IX", args.getRegister());
        assertEquals("3", args.getExpression());

        args = kasm.getIndexedAddressingArgs("( IX + 3 )");
        assertEquals("IX", args.getRegister());
        assertEquals("3 ", args.getExpression());
        
        args = kasm.getIndexedAddressingArgs("( IX + myConst)");
        assertEquals("IX", args.getRegister());
        assertEquals("myConst", args.getExpression());
        
        args = kasm.getIndexedAddressingArgs("( IX +   3 * 2     )");
        assertEquals("IX", args.getRegister());
        assertEquals("3 * 2     ", args.getExpression());
        
        args = kasm.getIndexedAddressingArgs("( IX +   3 + 12     )");
        assertEquals("IX", args.getRegister());
        assertEquals("3 + 12     ", args.getExpression());
        
        args = kasm.getIndexedAddressingArgs("( IX +   3 + 12   *6/(2+3)    )");
        assertEquals("IX", args.getRegister());
        assertEquals("3 + 12   *6/(2+3)    ", args.getExpression());
        
        args = kasm.getIndexedAddressingArgs("( IY +   3 + 12 *6/(ER+fgsd))");
        assertEquals("IY", args.getRegister());
        assertEquals("3 + 12 *6/(ER+fgsd)", args.getExpression());
        */
    }
}
