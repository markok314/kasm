package si.mk.kasm;

import org.cheffo.jeplite.ParseException;
import org.junit.Test;

import junit.framework.TestCase;


public class TestExprParser extends TestCase {

    @Test
    public void testBinLiterals() {
        ExprParser parser = new ExprParser();
        
        String expr = parser.replaceBinaryLiterals("a+ b");
        assertEquals("a+ b", expr);
        
        expr = parser.replaceBinaryLiterals("10b");
        assertEquals("2", expr);
        
        expr = parser.replaceBinaryLiterals(" 1111b ");
        assertEquals(" 15 ", expr);
        
        expr = parser.replaceBinaryLiterals("a+001b -11b+ 11 - 100");
        assertEquals("a+1 -3+ 11 - 100", expr);

        expr = parser.replaceBinaryLiterals("a+001b -11b+B11 - B100");
        assertEquals("a+1 -3+B11 - B100", expr);

        expr = parser.replaceBinaryLiterals("a+ 10000001b ");
        assertEquals("a+ 129 ", expr);
        
        expr = parser.replaceBinaryLiterals("a+ b1000001b ");
        assertEquals("a+ b1000001b ", expr);
        
        expr = parser.replaceBinaryLiterals("_1000001b ");
        assertEquals("_1000001b ", expr);
        
    }
    
    
    @Test
    public void testHexLiterals() {
        ExprParser parser = new ExprParser();
        
        String expr = parser.replaceHexLiterals("a+ b");
        assertEquals("a+ b", expr);
        
        expr = parser.replaceHexLiterals("ah + 12");
        assertEquals("ah + 12", expr);
        
        expr = parser.replaceHexLiterals("ah+ 12H ");
        assertEquals("ah+ 18 ", expr);
        
        expr = parser.replaceHexLiterals("wah+ bh");
        assertEquals("wah+ bh", expr);
        
        expr = parser.replaceHexLiterals("ahx+ b");
        assertEquals("ahx+ b", expr);
        
        expr = parser.replaceHexLiterals(" a+ b");
        assertEquals(" a+ b", expr);
        
        expr = parser.replaceHexLiterals(" 0ffFFH+ b");
        assertEquals(" 65535+ b", expr);
        
        
    }
    
    @Test
    public void testOperators() {
        ExprParser parser = new ExprParser();
        
        try {
            int result = parser.evaluate("(65535 << 24) >> 24");
            assertEquals(255, result);
            
        } catch (ParseException ex) {
            throw new AssertionError("Expression can not be evaluated: " + ex.getMessage());
        }
    }
}
