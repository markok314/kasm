package si.mk.kasm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class breaks assembler line into tokens. There exist the following kinds
 * of tokens:
 * 
 * <pre>
 * - labels, which must be terminated with ':' 
 * - commands, which must be followed by whitespace or EOL or comment char 
 * - operands, which must be separated by commas. Operands are not broken into 
 *   basic tokens, because expression parser is used to evaluate them.
 * </pre>
 * 
 * Assembler instructions may have 0 to 2 operands, while directives {@code DB}
 * and {@code DW} must have at least one operand.
 * 
 * Comments are stripped away and ignored.
 * 
 * @author markok
 * 
 */
class Tokenizer {

    /** Contains token and position info. */
    class Token {
        int start = 0;
        int end = 0;
        String token = null;
        char charToken = 0; // usually contains the char, where the previous
        // token was terminated
        boolean isEOL = false; // end of line


        boolean isEmpty() {
            return charToken == 0 && token == null;
        }
    }

    private final char LABEL_TERMINATOR = ':';
    private final char COMMENT_CHAR = ';';
    private final char OPERAND_SEPARATOR = ',';
    private AsmLine m_asmLine;

    private HashSet<String> m_reservedWords = new HashSet<String>();
    private boolean m_isReservedWordsChecking = true;
    private Set<String> m_instructions = new HashSet<String>();
    private Set<String> m_directives = new HashSet<String>();

    public Tokenizer(HashSet<String> reservedWords) {
        m_reservedWords.addAll(reservedWords);
    }
    
    /**
     * Sets all assembler instructions, which produce binary code. They are used 
     * during tokenizing process to distinguish them from constants.
     */
    public void setInstructions(Set<String> instructions) {
        m_instructions = instructions;
    }
    
    /**
     * Sets all assembler directives. They are used 
     * during tokenizing process to distinguish them from constants. They must 
     * be in upper case.
     *   
     * Examples:     
     *   "ORG", "DW", "$INCLUDE", ...
     */
    public void setDirectives(Set<String> directives) {
        m_directives = directives;
    }
    
    /**
     * The main method, which breaks the source line in asmLine into tokens and
     * assigns them to other members of asmLine.
     * 
     * @param asmLine
     * @throws AssemblerException
     */
    public void tokenize(AsmLine asmLine) throws AssemblerException {
        String line = asmLine.getSrcLine().trim();
        m_asmLine = asmLine;

        Token token = new Token();

        if (line.length() == 0) {
            return;
        }
        
        if (line.charAt(0) == '$') {

            // handle for directives starting with '$'
            token.start = 1;  // skip '$'
            asmLine.setDirective(true);
            if (getIdentifierToken(token, line)) {
                String directive = '$' + token.token;
                if (m_directives.contains(directive)) {
                    asmLine.setCmd(directive);
                    asmLine.setOperandsToUpperCase(false);
                    parseOperands(asmLine, line, token);
                } else {
                    throw new AssemblerException("Unknown directive: " + directive, asmLine);
                }
            } else { 
                throw new AssemblerException("'$' sign should be followed by directive!", asmLine);
            }
        } else if (getIdentifierToken(token, line)) { 
            // now we have label or mnemonic or directive or constant
            // LBL: mov A, 10
            //      mov A, 10
            //      org 3000
            // myconst equ 20
            
            if (token.charToken == LABEL_TERMINATOR) {
                // handle labels
                if (m_isReservedWordsChecking  &&  isReservedWord(token.token)) {
                    throw new AssemblerException("Reserved word must not be used as label : " + token.token, asmLine);
                }
                
                asmLine.setLabel(token.token);
                token.start++; // skip ':' after label

                if (getIdentifierToken(token, line)) { // get mnemonic
                    if (Character.isSpace(token.charToken) || token.charToken == ';' || token.isEOL) {
                        
                        if (m_directives.contains(token.token)) {
                            asmLine.setDirective(true);
                        }
                        asmLine.setCmd(token.token);
                        parseOperands(asmLine, line, token);
                    } else {
                        throw new AssemblerException("Invalid instruction!",
                                                     asmLine);
                    }
                }
                
                
            } else {
                if (!Character.isSpace(token.charToken)  &&  token.charToken != ';'  &&  !token.isEOL) {
                    throw new AssemblerException("Invalid instruction or identifier!", asmLine);
                }
                // it is mnemonic, or directive, or constant
                if (m_instructions.contains(token.token)) {
                    // it is mnemonic
                    asmLine.setCmd(token.token);
                    parseOperands(asmLine, line, token);
                } else if (m_directives.contains(token.token)) {
                    // it is mnemonic
                    asmLine.setCmd(token.token);
                    asmLine.setDirective(true);
                    parseOperands(asmLine, line, token);
                } else {
                    // it should be constant followed by directive, for example EQU, DATA, BIT, ...
                    asmLine.setLabel(token.token); 
                    if (getIdentifierToken(token, line)) { // get mnemonic
                        if (m_directives.contains(token.token)) {
                            asmLine.setDirective(true);
                            asmLine.setCmd(token.token);
                            parseOperands(asmLine, line, token);
                        } else {
                            throw new AssemblerException("Unknown assembler instruction or directive!", asmLine);
                        }
                    } else {
                        throw new AssemblerException("Invalid label or assembler instruction or directive!", asmLine);
                    }
                }
            }

        } else if (token.charToken != COMMENT_CHAR && !token.isEmpty()) {
            throw new AssemblerException("Invalid assembler syntax - line must "
                                                 + "start with identifier, directive or instruction!",
                                         asmLine);
        }
    }

    private void parseOperands(AsmLine asmLine, String line, Token token) throws AssemblerException {
        try {
            List<String> operands = getOperands(token, line, asmLine.isOperandsToUpperCase());
            asmLine.setOperands(operands);
        } catch (IllegalArgumentException ex) {
            throw new AssemblerException(ex.getMessage(), asmLine);
        }
    }


    public void setReservedWordChecking(boolean isReservedWordsChecking) {
        m_isReservedWordsChecking  = isReservedWordsChecking;
    }
    
    
    /**
     * Returns true, if word was found, false otherwise. If word was found:
     * 
     * <pre>
     * 1. token.token != null, it contains the token 
     * 2. token.charToken = char after token, if isEOL = false, 0 otherwise. 
     *    Set to ':' in case of labels
     * 3. token.isEOL depends on line end 
     * 4. token.start points to the char after token, or to ':' in case of labels
     * </pre>
     * 
     * If word was not found, there are the following possibilities:
     * 
     * <pre>
     * 1. Line was empty or contained white-spaces only. token.isEmpty() returns true.
     * 2. Start of comment was found: c = ';' 
     * 3. Any other character - error, including start of string (c = '&quot;' or c = ''')
     * </pre>
     * 
     * @param token
     *            contains position where to start looking for token in line
     * @param line
     *            source line to parse
     * @return true, if word token was found (starts with letter or '_', may
     *         contain letters, numbers and '_'
     */
    private boolean getIdentifierToken(Token token, String line) {

        char c = 0;

        int lineLen = line.length();
        
        // skip leading spaces
        while (token.start < lineLen
                && Character.isSpace(c = line.charAt(token.start))) {
            token.start++;
        }

        if (token.start == lineLen) {
            token.charToken = 0; // empty token
            token.token = null;
            return false;
        }

        if (!isIdentifierStart(c)) {
            token.charToken = c;
            return false;
        }

        token.end = token.start + 1;
        while (token.end < lineLen && isWordChar(c = line.charAt(token.end))) {
            token.end++;
        }

        if (token.end == lineLen) {
            token.isEOL = true;
        }

        token.token = line.substring(token.start, token.end).toUpperCase();
        token.start = token.end;
        token.charToken = c;
        return true;
    }


    /**
     * Returns list of operands separated by commas. All redundant spaces are
     * removed, except in string literals.
     * 
     * @param token contains position in line to start parsing
     * @param line the line beeing parsed
     * @return list of operands, may be empty
     * @throws AssemblerException
     */
    private List<String> getOperands(Token token, String line, boolean isOperandsToUpperCase) 
                                                               throws AssemblerException {
        int lineLen = line.length();
        // temporary buffer, longer than required, will be truncated later
        char tokenWOSpaces[] = new char[lineLen - token.start];
        int tokenLen = 0;

        List<String> tokens = new ArrayList<String>();
        boolean wasStringToken = false;

        int charIdx;
        for (charIdx = token.start; charIdx < lineLen; charIdx++) {
            char c = line.charAt(charIdx);

            if (c == '"' || c == '\'') { // handle single and double quoted
                // comments
                // AF' is legal string in EX AF, AF'
                if (!(tokenLen == 2 && tokenWOSpaces[0] == 'A'
                        && tokenWOSpaces[1] == 'F' && c == '\'')) {
                    token.charToken = c;
                    token.start = charIdx;
                    getString(token, line);
                    if (tokenLen > 0
                            || (token.charToken != OPERAND_SEPARATOR
                                    && !token.isEOL && token.charToken != COMMENT_CHAR)) {
                        // It is character in expression, for example: 23 + 'a',
                        // 'b' + 12, 1 + "f" +3.
                        // The string must be exactly 1 character long: 2
                        // quotes + char = 3
                        if (token.token.length() == 3) {
                            // it is expression with char code, for example 10 +
                            // 'A'
                            int charVal = token.token.charAt(1);
                            String charAsNumStr = Integer.toString(charVal);
                            for (int i = 0; i < charAsNumStr.length(); i++) {
                                tokenWOSpaces[tokenLen++] = charAsNumStr.charAt(i);
                            }
                        } else {
                            // for example , df "str",
                            throw new IllegalArgumentException("Invalid string token!");
                        }
                    } else {
                        tokens.add(token.token); // no toUpperCase() here,
                        // strings must be unchanged
                        wasStringToken = true;
                    }

                    charIdx = token.end - 1; // -1, because token.end points to
                    // char AFTER quote now and will skip it on next loop cycle
                    continue;
                }
            }

            // we are NOT in string now
            if (Character.isSpace(c)) {
                if (tokenLen == 0) {  // skip leading spaces only
                    continue;  // other spaces are not skipped here. Trailing 
                               // spaces are cut by trim() when converting to String
                }
                if ((charIdx + 1) < lineLen) {
                    char g = tokenWOSpaces[tokenLen - 1];
                    char h = line.charAt(charIdx + 1);
                    if (Character.isSpace(h)) {
                        continue; // skip multiple spaces in sequence 
                    }
                    if (!isWordChar(g)  || !isWordChar(h)) {
                        continue; // spaces must be kept only if there is no symbol between words or numbers
                    }
                }
                c = ' '; // if c is not space but for example tab, convert it to space
            }

            if (c == ';') {
                break;
            }

            if (c == ',') { // end of token
                if (wasStringToken) {
                    wasStringToken = false; // reset flag, because the next one may not be token 
                    continue;
                }
                if (tokenLen == 0) {
                    throw new AssemblerException("Missing operand before comma!",
                                                 m_asmLine);
                }
                tokens.add(getOperandString(tokenWOSpaces, tokenLen, isOperandsToUpperCase));
                tokenLen = 0;

            } else { // any other char but white space, ';' or ','
                if (wasStringToken) {
                    // for example: "str" xy,
                    throw new IllegalArgumentException("Invalid string token!");
                }
                tokenWOSpaces[tokenLen++] = c;
            }
        }

        if (!wasStringToken) {
            if (tokenLen == 0) {
                if (tokens.size() > 0) {
                    // if there was a comma (tokens.size() > 0) but no token
                    // following
                    // it (tokenLen == 0), it is an error
                    throw new AssemblerException("Missing operand after comma!",
                                                 m_asmLine);
                }
            } else {
                tokens.add(getOperandString(tokenWOSpaces, tokenLen, isOperandsToUpperCase)); 
            }
        }

        return tokens;
    }


    private String getOperandString(char[] tokenWOSpaces,
                                    int tokenLen,
                                    boolean isOperandsToUpperCase) {
        
        String operand = new String(tokenWOSpaces, 0, tokenLen).trim();
        
        if (isOperandsToUpperCase) {
            return operand.toUpperCase();
        } 
        
        return operand; 
    }

    /**
     * Should be called with <i>line[token.start] = '"' or '''</i>, and
     * <i>token.charToken == line[token.start]</i>. On return
     * <i>token.charToken</i> contains either the last character in the string
     * (isEOL == true) or the first non-whitespace character after the string
     * (isEOL == false)
     * 
     * @param token
     * @param line
     * @return false if the string is not properly terminated (EOL was found
     *         before starting quote char), thue otherwise
     * @throws AssemblerException
     */
    private void getString(Token token, String line) throws AssemblerException {

        char c = 0;
        int lineLen = line.length();
        token.end = token.start + 1;
        while (token.end < lineLen  &&  
                (c = line.charAt(token.end++)) != token.charToken);

        if (token.end == lineLen) {
            if (c != token.charToken) {
                throw new AssemblerException("String not terminated!",
                                             m_asmLine);
            }
        }

        token.token = line.substring(token.start, token.end);

        while (token.end < lineLen
                && Character.isSpace(c = line.charAt(token.end))) {
            token.end++;
        }

        if (token.end == lineLen) {
            token.isEOL = true;
        }

        token.charToken = c;
    }


    /** Returns true if c is letter or underscore. */
    private boolean isIdentifierStart(char c) {
        return c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '_';
    }


    /** Returns true, if c is letter, or underscore or number. */
    private boolean isWordChar(char c) {
        return c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a'
                && c <= 'z' || c == '_' || c == '.';
    }

    private boolean isReservedWord(String word) {
        return m_reservedWords.contains(word);
    }
}
