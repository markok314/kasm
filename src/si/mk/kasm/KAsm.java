/*
  KAsm - assembler for Z80
  Copyright (C) 2008  Marko Klopcic

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


  You can contact me at Google mail with the following prefix: markok3.14
 */

/*
 * Known bugs:
 * TODO: document and test option -r
 * TODO: define interface Tokenizer  
 */
package si.mk.kasm;

import jargs.gnu.CmdLineParser;
import jargs.gnu.CmdLineParser.IllegalOptionValueException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.cheffo.jeplite.ParseException;

import si.mk.kasm.i8051.translators.TranslatorContainer8051;
import si.mk.kasm.translators.ITranslatorContainer;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.TranslatorBase;
import si.mk.kasm.translators.OpCode.ArgumentSize;
import si.mk.kasm.z80.translators.TranslatorContainerZ80;



/**
 * This is the main class of assembler for Z80 and 8051 and derivatives.
 * 
 * TODO: If other tokenizing rules will be required, add interface ITokenizer,
 * which will be implemented by Tokenizer and new tokenizer classes. Then instantiate
 * the right class according to cmd line option (for example --tokenizer)
 *  
 * @author markok
 */
public class KAsm implements AddressProvider {

    /* 
    public static final String CMD_JP = "JP";
    public static final String CMD_INC = "INC";
    public static final String CMD_DEC = "DEC"; */ 
    
    private static final String DIR_EQU = "EQU";
    private static final String DIR_ORG = "ORG";
    private static final String DIR_END = "END";
    private static final String DIR_IF = "IF";
    private static final String DIR_ELSE = "ELSE";
    private static final String DIR_ELSEIF = "ELSEIF";
    private static final String DIR_ENDIF = "ENDIF";
    private static final String DIR_DB = "DB";
    private static final String DIR_DS = "DS";
    private static final String DIR_DW = "DW";
    private static final String DIR_INCLUDE = "$INCLUDE";

    // directives with space, by which they must be followed
    private static final String DIR_DATA = "DATA";
    private static final String DIR_BIT = "BIT";
    private static final String DIR_CODE = "CODE";
    private static final String DIR_IDATA = "IDATA";
    private static final String DIR_XDATA  = "XDATA";

    
    private static final int HEX_LINE_LEN = 16;
    private static final int MAX_MACHINE_CODE_LEN = 255; // limited, because 
    // length in output hex file is one byte only (otherwise hex output algorithm 
    // would be more complex). This limits only DB and DW directives to at most 
    // 255 / 127 item in one line, which is not a critical restriction.
    
    private static final String TARGET_Z80 = "Z80";  // this is not case sensitive
    private static final String TARGET_8051 = "8051";

    private Stack<SourceInfo> m_sourceReaders = new Stack<SourceInfo>();
    
    private String m_asmSource;
    private String m_lstFileName;
    private String m_hexFileName;
    private String m_binFileName;
    private int m_binFileTruncateOffset = 100000; // it is greater than 64k by default
    private List<AsmLine> m_program;
    private ExprParser m_exprParser = new ExprParser();
    private ITranslatorContainer m_translatorContainer;
    private Set<String> m_assigningDirectives = null; // contains directives, 
              // which assign values to constants, which not be mixed with labels
    
    private Tokenizer m_tokenizer = null;
    
    private int m_currentAddress;
    private boolean m_isEnd;
    private IfDirectiveInfo m_ifDirectiveInfo; // false only when inside conditional 
    // compilation block, which should NOT be compiled
    private Stack<IfDirectiveInfo> m_conditionalCompilationStack;

    char[] m_hex_buff2 = new char[2];
    char[] m_hex_buff4 = new char[4];
    char []m_bin2hexTable = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private boolean m_isProfiling = false;
    private boolean m_is8051 = false;
    private boolean m_isReservedWordChecking = true;
    
    public KAsm() {
        m_program = new ArrayList<AsmLine>(25000);
        m_conditionalCompilationStack = new Stack<IfDirectiveInfo>();
    }

    private void initZ80() {
        
        System.out.println("KAsm, Z80 Assembler\n(c) Marko Klopcic, Mar 2009");
        
        HashSet<String> reservedWords = new HashSet<String>(); 
        reservedWords.add("B");
        reservedWords.add("C");
        reservedWords.add("D");
        reservedWords.add("E");
        reservedWords.add("H");
        reservedWords.add("L");
        reservedWords.add("A");
        reservedWords.add("F");
        reservedWords.add("I");
        reservedWords.add("R");
        reservedWords.add("BC");
        reservedWords.add("DE");
        reservedWords.add("HL");
        reservedWords.add("AF");
        reservedWords.add("IX");
        reservedWords.add("IY");
        reservedWords.add("SP");
        
        m_tokenizer = new Tokenizer(reservedWords);

        String inst[] = { 
                "LD", "PUSH", "POP", "EX", "EXX", "LDI", "LDD", "CPI", "CPD", 
                "LDIR", "LDDR", "CPIR", "CPDR", "ADD", "ADC", "SUB", "SBC",
                "AND", "OR", "XOR", "CP", "INC", "DEC", "DAA", 
                "CPL", "CCF", "SCF", "NOP", "HALT", "DI", "EI", "NEG", "IM", 
                "RLCA", "RLA", "RRCA", "RRA", "RLC", "RRC", "RR", 
                "RL", "SLA", "SRA", "SRL", "RLD",  "RRD", "BIT", "SET", "RES", 
                "JP", "JR", "DJNZ", "CALL", "RET", "RETI", "RETN", "RST", "IN",
                "INI", "IND", "INIR", "INDR", "OUT", "OUTI", "OUTD", "OTIR",
                "OTDR"};
        Set<String> instructions = new HashSet<String>(Arrays.asList(inst));
        m_tokenizer.setInstructions(instructions);
        
        String dirs[] = {DIR_EQU, DIR_ORG, DIR_DW, DIR_DB, DIR_DS, DIR_END, DIR_IF, 
                         DIR_ELSE, DIR_ELSEIF, DIR_ENDIF, DIR_INCLUDE};
        Set<String> directives = new HashSet<String>(Arrays.asList(dirs));
        m_tokenizer.setDirectives(directives);
    
        m_tokenizer.setReservedWordChecking(m_isReservedWordChecking);
        
        String assigningDirs[] = {DIR_EQU};
        m_assigningDirectives = new HashSet<String>(Arrays.asList(assigningDirs));
        
        m_translatorContainer = new TranslatorContainerZ80(this, m_exprParser);

    }
    
    
    private void init8051() {
        
        System.out.println("KAsm, 8051 Family Assembler\n(c) Marko Klopcic, Mar 2009");
        
        m_is8051 = true;
        HashSet<String> reservedWords = new HashSet<String>(); 
        reservedWords.add("R0");
        reservedWords.add("R1");
        reservedWords.add("R2");
        reservedWords.add("R3");
        reservedWords.add("R4");
        reservedWords.add("R5");
        reservedWords.add("R6");
        reservedWords.add("R7");
        reservedWords.add("A");
        reservedWords.add("B");
        reservedWords.add("DPTR");
        reservedWords.add("PC");
        reservedWords.add("C");
        
        m_tokenizer = new Tokenizer(reservedWords);

        String inst[] = {"ADD", "ADDC", "SUBB", "INC", "DEC", "MUL", "DIV", "DA", 
                "ANL", "ORL", "XRL", "CLR", "CPL", "RL", "RLC", "RR", "RRC", 
                "SWAP", "MOV", "MOVC", "MOVX", "PUSH", "POP", "XCH", "XCHD", 
                "SETB", "ACALL", "LCALL", "CALL", "RET", "RETI", "AJMP", "LJMP", "SJMP", 
                "JMP", "JZ", "JNZ", "JC", "JNC", "JB", "JNB", "JBC", "CJNE", "DJNZ", "NOP"};
        Set<String> instructions = new HashSet<String>(Arrays.asList(inst));
        m_tokenizer.setInstructions(instructions);
        
        String dirs[] = {DIR_EQU, DIR_DATA, DIR_BIT, DIR_CODE, DIR_IDATA, DIR_XDATA, 
                DIR_ORG, DIR_DW, DIR_DB, DIR_DS, DIR_END, DIR_IF, DIR_ELSE, DIR_ELSEIF, DIR_ENDIF, DIR_INCLUDE};
        Set<String> directives = new HashSet<String>(Arrays.asList(dirs));
        m_tokenizer.setDirectives(directives);

        m_tokenizer.setReservedWordChecking(m_isReservedWordChecking);
        
        String assigningDirs[] = {DIR_EQU, DIR_DATA, DIR_BIT, DIR_CODE, DIR_IDATA, DIR_XDATA};
        m_assigningDirectives = new HashSet<String>(Arrays.asList(assigningDirs));
        
        m_translatorContainer = new TranslatorContainer8051(m_exprParser);
        
        // add SFRs
        m_exprParser.addVariable("P0", 0x80);
        m_exprParser.addVariable("SP", 0x81);
        m_exprParser.addVariable("DPL", 0x82);
        m_exprParser.addVariable("DPH", 0x83);
        m_exprParser.addVariable("TCON", 0x88);
        m_exprParser.addVariable("TMOD", 0x89);
        m_exprParser.addVariable("TL0", 0x8a);
        m_exprParser.addVariable("TL1", 0x8b);
        m_exprParser.addVariable("TH0", 0x8c);
        m_exprParser.addVariable("TH1", 0x8d);
        m_exprParser.addVariable("P1", 0x90);
        m_exprParser.addVariable("SCON", 0x98);
        m_exprParser.addVariable("SBUF", 0x99);
        m_exprParser.addVariable("P2", 0xa0);
        m_exprParser.addVariable("IE", 0xa8);
        m_exprParser.addVariable("P3", 0xb0);
        m_exprParser.addVariable("IP", 0xb8);
        m_exprParser.addVariable("PSW", 0xd0);
        m_exprParser.addVariable("ACC", 0xe0);
        m_exprParser.addVariable("B", 0xf0);

        // TCON
        m_exprParser.addVariable("TF1", 0x8f);
        m_exprParser.addVariable("TR1", 0x8e);
        m_exprParser.addVariable("TF0", 0x8d);
        m_exprParser.addVariable("TR0", 0x8c);
        m_exprParser.addVariable("IE1", 0x8b);
        m_exprParser.addVariable("IT1", 0x8a);
        m_exprParser.addVariable("IE0", 0x89);
        m_exprParser.addVariable("IT0", 0x88);
        
        // P3
        m_exprParser.addVariable("RD", 0xb7);
        m_exprParser.addVariable("WR", 0xb6);
        m_exprParser.addVariable("T1", 0xb5);
        m_exprParser.addVariable("T0", 0xb4);
        m_exprParser.addVariable("INT1", 0xb3);
        m_exprParser.addVariable("INT0", 0xb2);
        m_exprParser.addVariable("TXD", 0xb1);
        m_exprParser.addVariable("RXD", 0xb0);
        
        // IP 4-0
        m_exprParser.addVariable("PS", 0xbc);
        m_exprParser.addVariable("PT1", 0xbb);
        m_exprParser.addVariable("PX1", 0xba);
        m_exprParser.addVariable("PT0", 0xb9);
        m_exprParser.addVariable("PX0", 0xb8);
        
        // IE
        m_exprParser.addVariable("EA", 0xaf);
        m_exprParser.addVariable("ES", 0xac);
        m_exprParser.addVariable("ET1", 0xab);
        m_exprParser.addVariable("EX1", 0xaa);
        m_exprParser.addVariable("ET0", 0xa9);
        m_exprParser.addVariable("EX0", 0xa8);
        
        //SCON
        m_exprParser.addVariable("SM0", 0x9f);
        m_exprParser.addVariable("SM1", 0x9e);
        m_exprParser.addVariable("SM2", 0x9d);
        m_exprParser.addVariable("REN", 0x9c);
        m_exprParser.addVariable("TB8", 0x9b);
        m_exprParser.addVariable("RB8", 0x9a);
        m_exprParser.addVariable("TI", 0x99);
        m_exprParser.addVariable("RI", 0x98);
        
        // PSW
        m_exprParser.addVariable("CY", 0xd7);
        m_exprParser.addVariable("AC", 0xd6);
        m_exprParser.addVariable("F0", 0xd5);
        m_exprParser.addVariable("RS1", 0xd4);
        m_exprParser.addVariable("RS0", 0xd3);
        m_exprParser.addVariable("OV", 0xd2);
        // PSW.1 - reserved
        m_exprParser.addVariable("P", 0xd0);
 
    }

    private void parseCmdLineArgs(String[] args) {
        CmdLineParser cmdParser = new CmdLineParser();
        CmdLineParser.Option optHelp = 
            cmdParser.addBooleanOption('h', 
                                       "help", 
                                       "prints this help");

        CmdLineParser.Option optProcessorType = 
            cmdParser.addStringOption('s', 
                                      "processor", 
                                      "<processorType> : target processor. Currently 'z80' and\n" +
                                      "                                    '8051' are supported. If not\n" +
                                      "                                    specified, 'z80' is assumed.");
        
        
        CmdLineParser.Option optLstFileName = 
            cmdParser.addStringOption('l', 
                                      "lstname", 
                                      "<lstFileName> : name of LST output file.\n" +
                                      "                                   Default: <filename>.lst");
        
        CmdLineParser.Option optHexFileName = 
            cmdParser.addStringOption('x', 
                                      "hexname", 
                                      "<hexFileName> : name of HEX output file.\n" +
                                      "                                   Default: <filename>.hex");
        
        CmdLineParser.Option optBinFileName = 
            cmdParser.addStringOption('b', 
                                      "binname", 
                                      "<binFileName> : name of binary output file. If not specified,\n" +
                                      "                                   no binary file is created.");
        
        CmdLineParser.Option optConstants = 
            cmdParser.addBooleanOption('c', 
                                       "constants", 
                                       "add std constants to expression parser: 'e' and 'pi'");
        
        CmdLineParser.Option optFunctions = 
            cmdParser.addBooleanOption('f', 
                                       "functions", 
                                       "add std functions to expression parser: ln, acos, atan,\n" +
                                       "                   asin, cos, sin, tan, sqrt, sum, log, add umin, rand");
        
        CmdLineParser.Option optProfile = 
            cmdParser.addBooleanOption('p', 
                                       "profile", 
                                       "prints time spent in each step of compilation");
        
        CmdLineParser.Option optReserved = 
            cmdParser.addBooleanOption('r', 
                                       "reserved", 
                                       "reserved words (reg. names) may be used for labels and\n" +
                                       "                   constants.");
        
        CmdLineParser.Option optEqu = 
            cmdParser.addStringOption('e', 
                                      "equ", 
                                      "<const definition> : define constant. There must be no spaces\n" +
                                      "                                        next to '='. Example: -e COMPILE_ALL=1");

        CmdLineParser.Option optTruncate = 
            cmdParser.addStringOption('t', 
                                      "truncate", 
                                      "<len of bin file> : truncate bin file at the specified offset\n" +
                                      "                                       Example: -t 32768");
        try {
            cmdParser.parse(args);

            if (cmdParser.getOptionValue(optHelp) != null) {
                printHelp(cmdParser.toHelpString());
                System.exit(0);
            }
            if (cmdParser.getOptionValue(optConstants) != null) {
                m_exprParser.addStandardConstants();
                // add also UPPER CASE constants, because assembler is case insensitive 
                m_exprParser.addVariable("E", Math.E);
                m_exprParser.addVariable("PI", Math.PI);
            }
            if (cmdParser.getOptionValue(optReserved) != null) {
                m_isReservedWordChecking  = false;
            }
            if (cmdParser.getOptionValue(optProfile) != null) {
                m_isProfiling  = true;
            }
            if (cmdParser.getOptionValue(optFunctions) != null) {
                // add also UPPER CASE functions, because assembler is case insensitive
                m_exprParser.addFunction("SIN", new Sine());
                m_exprParser.addFunction("COS", new Cosine());
                m_exprParser.addFunction("TAN", new Tangent());
                m_exprParser.addFunction("SQRT", new Sqrt());

                m_exprParser.addFunction("LOG", new Logarithm());
                m_exprParser.addFunction("LN", new NaturalLogarithm());
            }

            String truncateValStr = (String)cmdParser.getOptionValue(optTruncate); 
            if (truncateValStr != null) {
                try {
                    m_binFileTruncateOffset = Integer.parseInt(truncateValStr);
                } catch (Exception ex) {
                    System.out.println("Truncate value (option -t, --trunacte) is not integer: " + truncateValStr);
                    System.exit(-1);
                }
            }
            
            try {
                String target = (String)cmdParser.getOptionValue(optProcessorType);
                if (target != null) {
                    target = target.toUpperCase();
                    if (target.equals(TARGET_Z80)) {
                        initZ80();
                    } else if (target.equals(TARGET_8051)) {
                        init8051();
                    } else {
                        throw new IllegalArgumentException("Invalid target type! Only 'Z80' " +
                                                           "and '8051' are supported! Specified: " + target);
                    }
                } else {
                    initZ80();  // default target
                }
                m_lstFileName = (String)cmdParser.getOptionValue(optLstFileName);
                m_hexFileName = (String)cmdParser.getOptionValue(optHexFileName);
                m_binFileName = (String)cmdParser.getOptionValue(optBinFileName);
            } catch (Exception ex) {
                System.out.println("Invalid command line option! " + ex.getMessage());
                System.exit(-1);
            }
            
            List<Object> equDirectives = cmdParser.getOptionValues(optEqu);
            for (Object equ : equDirectives) {
                if (equ instanceof String) {
                    String equStr = (String) equ;
                    String[] equItems = equStr.split("=");
                    if (equItems.length != 2) {
                        throw new IllegalOptionValueException(optEqu, "argument must be an assignment (remove spaces next to '=')"); 
                    }
                    m_exprParser.addVariable(equItems[0], Double.parseDouble(equItems[1]));
                } else {
                    throw new IllegalOptionValueException(optEqu, "argument must be an assignment (remove spaces next to '=')"); 
                }
            }

            String[] fileName = cmdParser.getRemainingArgs(); 
            if (fileName.length == 1) {
                m_asmSource = fileName[0];
                System.out.println("Compiling: " + m_asmSource + '\n');
            } else {
                System.err.println("\nSource file name is missing!");
                printHelp(cmdParser.toHelpString());
                System.exit(2);
            }
        } catch (CmdLineParser.OptionException e) {
            System.err.println('\n' + e.getMessage());
            // give stderr enough time to display the message,
            // otherwise it may appear after 'Usage'
            try {Thread.sleep(300);} catch(Exception ex){System.out.println("Interrupted!");}
            
            printHelp(cmdParser.toHelpString());
            System.exit(2);
        }
        
    }


    private void printHelp(String options) {
        System.out.println("\nUsage  : KAsm [options] <sourceFileName>");
        System.out.println(  "Examples: KAsm -f -c -e COMPILE_ALL=1 -e COMPILE_FLOAT=0 myZ80prog.asm");
        System.out.println(  "          KAsm -s 8051 -f -c -e COMPILE_ALL=1 -e COMPILE_FLOAT=0 my8051prog.asm");
        System.out.println("Options: ");
        System.out.println(options);
    }
    

    /**
     * This method reads assembler source file, breaks lines into tokens, and
     * translates them. Only expressions, which may contain labels are not 
     * evaluated here.
     * 
     * @param fileName
     * @throws IOException
     */
    private void readFile(String fileName) throws IOException {
        SourceInfo reader = 
            new SourceInfo(fileName);
        m_sourceReaders.push(reader);

        m_currentAddress = 0;
        m_ifDirectiveInfo = new IfDirectiveInfo(true, false);
        
        mainWhile:
        while (!m_isEnd) {

            String line = m_sourceReaders.peek().readLine();

            while (line == null) {  // EOF in the current file - may be nested include file
                reader = m_sourceReaders.pop();
                
                if (m_sourceReaders.isEmpty()) {
                    break mainWhile;  // EOF of the top level file, exit the loop
                }
                
                reader.close();  // close include file
                reader = m_sourceReaders.peek(); // continue with parent include or asm file
                line = reader.readLine();
            }
            
            AsmLine asmLine = new AsmLine();
            m_program.add(asmLine);  // must be added before any exception is thrown,
            // so that we get it on the output file together with error
            asmLine.setSourceFile(m_sourceReaders.peek().getFileName());
            try {
                tokenize(asmLine, line, m_sourceReaders.peek().getLineNumber());
                asmLine.setAddress(m_currentAddress);
                
                if (!translateConditional(asmLine)  &&  m_ifDirectiveInfo.m_isConditionalOn) {
                    translateCmd(asmLine); 
                    // asm lines, which have command defined, but no op code assigned
                    // were excluded by conditional compilation
                    
                    // constants defined by EQU should not be assigned current address
                    if (asmLine.getCmd() == null  ||  asmLine.hasLabel()  &&  !m_assigningDirectives.contains(asmLine.getCmd())) {
                        String label = asmLine.getLabel();
                        if (label != null) {
                            if (m_exprParser.getSymbolTable().containsKey(label)) {
                                throw new AssemblerException("Duplicate label defined: " + label, asmLine);
                            }
                            m_exprParser.addVariable(label, m_currentAddress);
                        }
                        // m_symbolTable.put(label, m_currentAddress);
                    }
                    m_currentAddress += asmLine.getMachineCodeLen();
                }
            } catch (AssemblerException ex) {
                logError(ex);
            } catch (ParseException ex) {
                logError(ex.getMessage(), asmLine);
            } catch (Exception ex) {
                logError(ex.getMessage(), asmLine);
            }
        }
        
        reader.close();
    }

    private void logError(AssemblerException ex) {
        // error message in asmLine has been set in AssemblerExeption ctor
        AsmLine asmLine = ex.getAsmLine();
        System.err.println(formatErrWrnMsg("Error", asmLine.getSourceFile(), 
                                           asmLine.getSourceLineNumber(),
                                           asmLine.getSrcLine(), ex.getMessage()));
    }


    private void logError(String msg, AsmLine asmLine) {
        asmLine.setErrorMsg(msg);
        System.err.println(formatErrWrnMsg("Error", asmLine.getSourceFile(), 
                                           asmLine.getSourceLineNumber(),
                                           asmLine.getSrcLine(), msg));
    }


    private void logWarning(String msg, AsmLine asmLine) {
        asmLine.setWarnMsg(msg);
        System.err.println(formatErrWrnMsg("Warning", asmLine.getSourceFile(), 
                                           asmLine.getSourceLineNumber(),
                                           asmLine.getSrcLine(), msg));
    }

    private String formatErrWrnMsg(String errWrn, String srcfileName, int sourceLineNo, String source, String msg) {
        return errWrn + String.format(" (%s:%d): '%s' : %s", srcfileName, sourceLineNo, source, msg);
    }
    
    
    /**
     * This method converts command to binary machine code
     * @param asmLine contains tokenized source line
     */
    private void translateCmd(AsmLine asmLine) throws AssemblerException, ParseException {
        if (asmLine.getCmd() == null) {
            return;
        }
        
        if (!asmLine.isDirective()) {
            if (!m_translatorContainer.translateCmdToOpCode(asmLine)) {
                throw new AssemblerException("Invalid assembler mnemonic!", asmLine);
            }
        } else {
            translateDirective(asmLine); 
        }
    }

    
    private void translateDirective(AsmLine asmLine) throws AssemblerException,
                                                    ParseException {
        String op1 = asmLine.getOperand1();

        // it may be assembler directive
        if (asmLine.getCmd().equals(DIR_EQU)) {
            TranslatorBase.checkPresenceOfOperands(asmLine, 1);
            String constant = asmLine.getLabel();
            if (m_exprParser.getSymbolTable().containsKey(constant)) {
                logWarning("Redefinition of constant: " + constant, asmLine);
            }
            m_exprParser.addVariable(constant, m_exprParser.evaluate(op1));
        } else if (asmLine.getCmd().equals(DIR_ORG)) {
            TranslatorBase.checkPresenceOfOperands(asmLine, 1);
            m_currentAddress = m_exprParser.evaluate(op1);

        } else if (asmLine.getCmd().equals(DIR_END)) {
            TranslatorBase.checkPresenceOfOperands(asmLine, 0);
            m_isEnd = true;
            if (!m_conditionalCompilationStack.isEmpty()) {
                throw new AssemblerException("Not all IF directives are "
                                             + " closed with ENDIF directive!", asmLine);
            }
        } else if (asmLine.getCmd().equals(DIR_DB)) {
            if (op1 == null) {
                throw new AssemblerException("At least one operand required for 'DB' directive!", 
                                             asmLine);
            }            
            handleArgumentList(asmLine, ArgumentSize.BITS_8_UNSIGNED);
        } else if (asmLine.getCmd().equals(DIR_DS)) {
            if (op1 == null) {
                throw new AssemblerException("At least one operand required for 'DS' directive!", 
                                             asmLine);
            }            
            int numBytes = m_exprParser.evaluate(op1);
            m_currentAddress += numBytes;
        } else if (asmLine.getCmd().equals(DIR_DW)) {
            if (op1 == null) {
                throw new AssemblerException("At least one operand required for 'DW' directive!", 
                                             asmLine);
            }            
            if (m_is8051 ) {
                handleArgumentList(asmLine, ArgumentSize.BITS_16_BIG_ENDIAN);
            } else {
                handleArgumentList(asmLine, ArgumentSize.BITS_16);
            }
        } else if (asmLine.getCmd().equals(DIR_INCLUDE)) {
            if (op1 == null) {
                throw new AssemblerException("At least one operand required for 'INCLUDE' directive!", 
                                             asmLine);
            }            
            if (op1.charAt(0) == '('  &&  op1.charAt(op1.length() - 1) == ')') {  
                String fileName = op1.substring(1, op1.length() - 1).trim();
                try {
                    SourceInfo reader = new SourceInfo(fileName);
                    m_sourceReaders.push(reader);
                } catch (IOException ex) {
                    throw new AssemblerException("Can not open include file: " + 
                                                 fileName + "\n" + ex.getMessage(), 
                                                 asmLine);
                }
            } else {
                throw new AssemblerException("Name of include file must be in parentheses!", 
                                             asmLine);
            }
        } else if (asmLine.getCmd().equals(DIR_BIT)) {
            // bit expressions should not contain floating point numbers
            // so it is safe to replace '.' with '+', which is required
            // for expression parser.
            String op = asmLine.getOperand1().replace('.', '+');
            asmLine.setOperand(0, op);
            addConstantToSymbolTable(asmLine);
        } else if (asmLine.getCmd().equals(DIR_DATA)) {
            addConstantToSymbolTable(asmLine);
        } else if (asmLine.getCmd().equals(DIR_CODE)) {
            addConstantToSymbolTable(asmLine);
        } else if (asmLine.getCmd().equals(DIR_IDATA)) {
            addConstantToSymbolTable(asmLine);
        } else if (asmLine.getCmd().equals(DIR_XDATA)) {
            addConstantToSymbolTable(asmLine);
        } else { 
            throw new AssemblerException("Invalid assembler directive!", asmLine);
            // process all assembler commands, which do not have variable parameters
            // op1 = removeSpacesInOperand(op1); // this is required, because one may write EX (  SP  ), HL
            // op2 = removeSpacesInOperand(op2);
        }
    }

    private void addConstantToSymbolTable(AsmLine asmLine) throws AssemblerException,
                                                          ParseException {
        TranslatorBase.checkPresenceOfOperands(asmLine, 1);

        String constant = asmLine.getLabel();
        if (m_exprParser.getSymbolTable().containsKey(constant)) {
            logWarning("Redefinition of constant: " + constant, asmLine);
        }
        m_exprParser.addVariable(constant, m_exprParser.evaluate(asmLine.getOperand1()));
    }


    /** This line handles conditional compilation assembler directives. */
    private boolean translateConditional(AsmLine asmLine) throws AssemblerException, 
                                                                 ParseException {
        if (asmLine.getCmd() == null) {
            return false;
        }
        
        String op1 = asmLine.getOperand1();
        
        if (asmLine.getCmd().equals(DIR_IF)) {
            if (m_ifDirectiveInfo.m_isConditionalOn) {
                String label = asmLine.getLabel();
                if (label != null) {
                    m_exprParser.addVariable(label, m_currentAddress);
                }
            }

            TranslatorBase.checkPresenceOfOperands(asmLine, 1);
            op1 = op1.replaceAll("=", "==");  // replace single = with double ==,
            // to be compatible with other assemblers, for example asem51. This
            // is just temporary solution - in a long term the expression parser 
            // should be rewritten for assembler.
            
            m_conditionalCompilationStack.push(m_ifDirectiveInfo); // push parent info
            
            boolean isConditionalOn = 
                (m_exprParser.evaluate(op1) != 0) & m_conditionalCompilationStack.peek().m_isConditionalOn;
            
            m_ifDirectiveInfo = new IfDirectiveInfo(isConditionalOn, isConditionalOn);
            
        } else if (asmLine.getCmd().equals(DIR_ELSEIF)) {
            TranslatorBase.checkPresenceOfOperands(asmLine, 1);
            if (m_conditionalCompilationStack.isEmpty()) {
                throw new AssemblerException("ELSE without IF statement!", asmLine);
            }
            
            IfDirectiveInfo parentIfInfo = m_conditionalCompilationStack.peek();
            
            if (m_ifDirectiveInfo.m_hasAnyOfIfOrElseifEvaluatedToTrue) {
                m_ifDirectiveInfo.m_isConditionalOn = false;
            } else {
                boolean parentConditional = parentIfInfo.m_isConditionalOn;
                op1 = op1.replaceAll("=", "==");  // replace single = with double ==
                m_ifDirectiveInfo.m_isConditionalOn = (m_exprParser.evaluate(op1) != 0) & parentConditional;
                m_ifDirectiveInfo.m_hasAnyOfIfOrElseifEvaluatedToTrue = m_ifDirectiveInfo.m_isConditionalOn; 
            }
        
        } else if (asmLine.getCmd().equals(DIR_ELSE)) {
            TranslatorBase.checkPresenceOfOperands(asmLine, 0);
            if (m_conditionalCompilationStack.isEmpty()) {
                throw new AssemblerException("ELSE without IF statement!", asmLine);
            }
            
            IfDirectiveInfo parentIfInfo = m_conditionalCompilationStack.peek();

            if (m_ifDirectiveInfo.m_hasAnyOfIfOrElseifEvaluatedToTrue) {
                m_ifDirectiveInfo.m_isConditionalOn = false;
            } else {
                m_ifDirectiveInfo.m_isConditionalOn = parentIfInfo.m_isConditionalOn;
            }
            
        } else if (asmLine.getCmd().equals(DIR_ENDIF)) {
            TranslatorBase.checkPresenceOfOperands(asmLine, 0);
            if (m_conditionalCompilationStack.size() == 0) {
                throw new AssemblerException("ENDIF without IF statement!", asmLine);
            }
            m_ifDirectiveInfo = m_conditionalCompilationStack.pop();
            
        } else {
            return false;
        }
        
        return true; // conditional directive processed
    }

    
    /** 
     * This method evaluates remaining expressions. 
     */
    private void translatePass2() {
        for (AsmLine asmLine : m_program) {
            try {
                if (asmLine.getOpCode() != null) {
                    String wrnMsg = asmLine.getOpCode().evaluateArgs(m_exprParser);
                    if (wrnMsg != null) {
                        logWarning(wrnMsg, asmLine);
                    }
                }
            } catch (ParseException ex) {
                logError(ex.getMessage(), asmLine);
            } catch (IllegalArgumentException ex) {
                logError(ex.getMessage(), asmLine);
            } catch (Exception ex) {
                logError(ex.getClass().getSimpleName() + ": " + ex.getMessage(), asmLine);
                ex.printStackTrace(); // because other types of exceptions are caused by program error 
            }
        }
    }
    
    
    
    // used for directives DB and DW - splits arguments and allocates the
    // the right number of bytes
    private void handleArgumentList(AsmLine asmLine, OpCode.ArgumentSize bitSize) throws AssemblerException {
        try {
            OpCode opCode = new OpCode(asmLine.getOperands(), bitSize);
            asmLine.setOpCode(opCode);
        } catch (Exception ex) {
            throw new AssemblerException(ex.getMessage(), asmLine);
        }
    }
        
    
    private AsmLine tokenize(AsmLine asmLine, String line, int lineNo) throws AssemblerException {

        asmLine.setSrcLine(line);
        asmLine.setSourceLineNumber(lineNo);
        
        m_tokenizer.tokenize(asmLine);
        
        return asmLine;
    }


    /** Writes compiled program to LST file. */
    private void toLstFile(String fileName) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)));
        String []offsets = new String[]{"            ", "          ", "        ", "      ", "    ", "  "};
        
        for (AsmLine asmLine : m_program) {
            if (asmLine.getOpCode() != null) {

                write4digits(writer, asmLine.getAddress());
                // writer.write(String.format("%04x", asmLine.getAddress()));
                writer.write("    ");

                short[] machineCode = asmLine.getMachineCode();
                for (short code : machineCode) {
                    write2digits(writer, code);
                    // writer.write(String.format("%02x", code & 0xff));
                }

                writer.write(offsets[machineCode.length > 5 ? 5 : machineCode.length]);
            } else {
                writer.write("                    ");
            }
            
            writer.write(asmLine.getSrcLine());

            writer.newLine();
            if (asmLine.getWarnMsg() != null) {
                writer.write(formatErrWrnMsg("Warning", m_asmSource, asmLine.getSourceLineNumber(), 
                                             asmLine.getSrcLine(), asmLine.getWarnMsg()));
                writer.newLine();
            }
            if (asmLine.getErrorMsg() != null) {
                writer.write(formatErrWrnMsg("Error", m_asmSource, asmLine.getSourceLineNumber(), 
                                             asmLine.getSrcLine(), asmLine.getErrorMsg()));
                writer.newLine();
            }
        }
        
        writer.close();
    }

    
    /** Writes compiled program in Intel HEX format. */
    private void toHexFile(String fileName) throws IOException {
        
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)));
        short bytes[] = new short[OpCode.MAX_BYTES_PER_ASM_LINE + HEX_LINE_LEN]; 
        int bytesLen = 0;
        int bytesIndex = 0;
        int address = 0;
        
        for (AsmLine asmLine : m_program) {
            try {
                
            if (asmLine.getCmd() != null) {
                // handle directives ORG and DS, which may influence HEX file
                if (asmLine.getCmd().equals(DIR_ORG)) {
                    // write what's left in the buffer from previous commands
                    writeHexLine(writer, address, bytes, bytesLen, false);
                    bytesLen = bytesIndex = 0;
                    address = m_exprParser.evaluate(asmLine.getOperand1());
                    continue;
                }
                if (asmLine.getCmd().equals(DIR_DS)) {
                    // write what's left in the buffer from previous commands
                    writeHexLine(writer, address, bytes, bytesLen, false);
                    address += bytesLen + m_exprParser.evaluate(asmLine.getOperand1());
                    bytesLen = bytesIndex = 0;
                    continue;
                }
            }
            
            if (asmLine.getOpCode() != null) {

                short[] machineCode = asmLine.getMachineCode();
                if (machineCode.length > MAX_MACHINE_CODE_LEN) {
                    throw new AssemblerException("To many items in line!", asmLine);
                }
                System.arraycopy(machineCode, 0, bytes, bytesIndex, machineCode.length);
                bytesLen += machineCode.length;
                bytesIndex += machineCode.length;
                
                while (bytesLen >= HEX_LINE_LEN) {
                    int bytesWritten = writeHexLine(writer, address, bytes, HEX_LINE_LEN, true);
                    // copy possible overflow bytes to the beginning of buffer
                    address += bytesWritten;
                    System.arraycopy(bytes, bytesWritten, bytes, 0, bytesLen - bytesWritten);
                    bytesLen = bytesLen - bytesWritten;
                    bytesIndex = bytesLen;
                }
            }
            } catch (ParseException ex) {
                logError(ex.getMessage(), asmLine);
            } catch (AssemblerException ex) {
                logError(ex);
            }
        }
        
        writeHexLine(writer, address, bytes, bytesLen, false);
        writer.write(":00000001FF");
        writer.newLine();
        writer.close();
    }


    /**
     * Writes one line of intel HEX file 
     * @param writer
     * @param address
     * @param bytes
     * @param bytesLen may be up to 255 (may happen only in case of DB/DW directives)
     * @param is16BytesChunksOnly if true, only bytesLen/16 lines are written and
     * the remaining is left in buffer for the next write
     * @return
     * @throws IOException
     */
    
    private int writeHexLine(BufferedWriter writer, 
                             int address, 
                             short[] bytes, 
                             int bytesLen,
                             boolean is16BytesChunksOnly) throws IOException {
        
        if (bytesLen == 0) {
            return 0;
        }
        int bytesWritten = 0;

        do {
            int currentBytesLen = Math.min(bytesLen - bytesWritten, HEX_LINE_LEN);
            
            if (currentBytesLen < HEX_LINE_LEN  &&  is16BytesChunksOnly) {
                return bytesWritten; // keep remaining bytes in the array
            }
            if (currentBytesLen == 0) {
                return bytesWritten;
            }
            if (currentBytesLen < 0) {
                throw new IOException("Internal error! currentBytesLen < 0: " + currentBytesLen);
            }
            
            int checksum = 0;
            writer.write(':');
            write2digits(writer, currentBytesLen);
            //writer.write(String.format("%02X", currentBytesLen & 0xff));
            checksum += currentBytesLen; 

            write4digits(writer, address);
            //writer.write(String.format("%04X", address));
            checksum += address; 
            // writer.write(String.format("%02x", (address >> 8) & 0xff));
            checksum += address >> 8; 

            writer.write("00");

            for (int i = 0; i < currentBytesLen; i++) {
                write2digits(writer, bytes[i + bytesWritten]);
                //writer.write(String.format("%02X", bytes[i + bytesWritten] & 0xff));
                checksum += bytes[i + bytesWritten]; 
            }
            bytesWritten += currentBytesLen;
            write2digits(writer, (~checksum + 1) & 0xff);
            //writer.write(String.format("%02X", (~checksum + 1) & 0xff));
            writer.newLine();
        } while (true);
    }

    
    private void write2digits(BufferedWriter writer, int number) throws IOException {
        m_hex_buff2[0] = m_bin2hexTable[(number >> 4) & 0x0f];
        m_hex_buff2[1] = m_bin2hexTable[number & 0x0f];
        writer.write(m_hex_buff2);
    }
    
    
    private void write4digits(BufferedWriter writer, int number) throws IOException {
        m_hex_buff4[0] = m_bin2hexTable[(number >> 12) & 0x0f];
        m_hex_buff4[1] = m_bin2hexTable[(number >> 8) & 0x0f];
        m_hex_buff4[2] = m_bin2hexTable[(number >> 4) & 0x0f];
        m_hex_buff4[3] = m_bin2hexTable[number & 0x0f];
        writer.write(m_hex_buff4);
    }


    /**
     * Writes compiled program to binary file, which may be 
     * directly written to memory.
     * 
     * @param binFileName
     * @throws IOException 
     */
    private void toBinFile(String binFileName, int truncateOffset) throws IOException {

        File f = new File(binFileName);

        if (f.exists()) {
            f.delete(); // RandomAccessFile does not delete file, so do it here
        }
        
        RandomAccessFile writer = new RandomAccessFile(binFileName, "rw");
        int address = 0;
        
eof:        
        for (AsmLine asmLine : m_program) {
            
            // skip lines with no machine code
            if (asmLine.getOpCode() == null) {
                continue;
            }
            
            // fill empty space with zeroes
            while (address < asmLine.getAddress()) {
                writer.writeByte(0);
                address++;
                if (address >= truncateOffset) {
                    break eof;
                }
            }
            
            // move back if ORG with smaller address is found
            if (asmLine.getAddress() < address) {
                writer.seek(address);
                address = asmLine.getAddress();
            }
            
            int len = asmLine.getMachineCode().length;
            short [] bytes = asmLine.getMachineCode();
            for (int i = 0; i < len; i++) {
                writer.write(bytes[i]);
                address++;
                if (address >= truncateOffset) {
                    break eof;
                }
            }
        }
        
        if (address >= truncateOffset) {
            System.out.println("Bin file truncated at length: " + truncateOffset);
        }
        
        writer.close();
    }

    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\n\nTokens\n");
        sb.append(    "======\n");
        for (AsmLine asmLine : m_program) {
            sb.append(asmLine.toString());
        }

        return sb.toString();
    }

    
    private void compile() throws IOException {
        long startTime = System.nanoTime();
        readFile(m_asmSource);
        if (m_isProfiling) {
            System.out.printf("-pass1: = %f ms\n", (System.nanoTime() - startTime) / 1e6);
        }
        translatePass2();
        if (m_isProfiling) {
            System.out.printf("-pass2 = %f ms\n", (System.nanoTime() - startTime) / 1e6);
        }

        String fileNameBase = m_asmSource;
        int dotIndex = fileNameBase.lastIndexOf('.'); 
        if (dotIndex >= 0) {
            fileNameBase = fileNameBase.substring(0, dotIndex);
        }
        if (m_hexFileName == null) {
            m_hexFileName = fileNameBase + ".hex";
        }
        if (m_lstFileName == null) {
            m_lstFileName = fileNameBase + ".lst";
        }
        toLstFile(m_lstFileName);
        if (m_isProfiling) {
            System.out.printf("-lst time = %f ms\n", (System.nanoTime() - startTime) / 1e6);
        }
        toHexFile(m_hexFileName);
        if (m_isProfiling) {
            System.out.printf("-hex time = %f ms\n", (System.nanoTime() - startTime) / 1e6);
        }
        if (m_binFileName != null) {
            toBinFile(m_binFileName, m_binFileTruncateOffset);
            System.out.printf("-bin time = %f ms\n", (System.nanoTime() - startTime) / 1e6);
        }
        
        if (m_isProfiling) {
            System.out.printf("time = %fms\n", (System.nanoTime() - startTime) / 1e6);
        }
    }


    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        KAsm kasm = new KAsm();
        
        kasm.parseCmdLineArgs(args);
        
        try {
            kasm.compile();
        } catch (IOException ex) {
            System.out.println("File Error: " + ex.getMessage());
        }

    }


    public int getCurrentAddress() {
        return m_currentAddress;
    }
    
    
    /**
     * This class contain information about source file - file name,
     * current line number, and reader.
     */
    class SourceInfo 
    {
        String m_fileName;
        int m_line_no;
        BufferedReader m_reader;

        SourceInfo(String fileName) throws IOException {
            m_fileName = fileName;
            m_line_no = 0;
            m_reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
        }
        
        public String getFileName() {
            return m_fileName;
        }

        String readLine() throws IOException {
            m_line_no++;
            return m_reader.readLine();
        }
        
        void close() throws IOException {
            m_reader.close();
        }
        
        int getLineNumber() {
            return m_line_no;
        }
    }
    
    
    class IfDirectiveInfo {
        
        public IfDirectiveInfo(boolean isCurrentConditionalOn,
                               boolean hasAnyOfIfOrElseifEvaluatedToTrue) {
        
            m_isConditionalOn = isCurrentConditionalOn;
            m_hasAnyOfIfOrElseifEvaluatedToTrue = hasAnyOfIfOrElseifEvaluatedToTrue;
        }
        
        boolean m_isConditionalOn; // true, when current conditional compilation is ON
        boolean m_hasAnyOfIfOrElseifEvaluatedToTrue; // used for elseif and else blocks
    }
}
