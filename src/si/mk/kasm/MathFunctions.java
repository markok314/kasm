package si.mk.kasm;

import org.cheffo.jeplite.ParseException;
import org.cheffo.jeplite.function.PostfixMathCommand;

class Cosine extends PostfixMathCommand
{
  public Cosine()
  {
    numberOfParameters = 1;
  }

  public final double operation(double[] params)
    throws ParseException
  {
    return Math.cos(params[0]);
  }
}

class Logarithm extends PostfixMathCommand
{
  public Logarithm()
  {
    numberOfParameters = 1;
  }
  private static final double LOG_10 = Math.log(10);

  public final double operation(double[] params)
    throws ParseException
  {
    return Math.log(params[0])/LOG_10;
  }
}

class NaturalLogarithm extends PostfixMathCommand
{
  public NaturalLogarithm()
  {
    numberOfParameters = 1;
  }

  public final double operation(double[] params)
    throws ParseException
  {
    return Math.log(params[0]);
  }
}

class Sine extends PostfixMathCommand
{
  public Sine()
  {
    numberOfParameters = 1;
  }

  public final double operation(double[] params)
    throws ParseException
  {
      return Math.sin(params[0]);
  }
}

class Tangent extends PostfixMathCommand
{
  public Tangent()
  {
    numberOfParameters = 1;
  }

  public final double operation(double[] params)
    throws ParseException
  {
    return Math.tan(params[0]);
  }
}


class Sqrt extends PostfixMathCommand
{
  public Sqrt()
  {
    numberOfParameters = 1;
  }

  public final double operation(double[] params)
  throws ParseException
  {
    return Math.sqrt(params[0]);
  }
}
