/*
  KAsm - assembler for Z80
  Copyright (C) 2008  Marko Klopcic

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


  You can contact me at Google mail with the following address: markok3.14
 */


package si.mk.kasm;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cheffo.jeplite.JEP;
import org.cheffo.jeplite.ParseException;

/**
 * This class is used to parse and evaluate expressions containing labels and 
 * constants. It handles specifics like hex and binary number format.
 *  
 * @author markok
 */
public class ExprParser extends JEP {

    private Pattern m_binaryNumber = Pattern.compile("[10]+[bB]");
    private Pattern m_hexNumber = Pattern.compile("[0-9][0-9a-fA-F]*[hH]");
    // private Pattern m_charNumber = Pattern.compile("['\"](.)['\"]");
    private int m_literalValue;
    
    
    public int evaluate(String expression) throws ParseException {
        // first check, if the expression contains single literal only

        if (parseLiteral(expression)) {
            return m_literalValue;
        }
        
        // first replace binary and hex literals with decimal ones:
        expression =  replaceBinaryLiterals(expression);
        expression =  replaceHexLiterals(expression);
        
        parseExpression(expression);
        
        try {
            int value = (int)Math.floor(getValue());
            return value;
        } catch (NullPointerException ex) {
            throw new ParseException("Can not evaluate expression: '" + expression + "'"); 
        } 
    }

    
    /** Replaces binary literals with decimal ones, which are understood by JEP. */
    protected String replaceBinaryLiterals(String expression) {
        Matcher matcher = m_binaryNumber.matcher(expression);
        do {
            if (matcher.find()) {
                int start = matcher.start();
                // check that it does not start in the middle of an identifier 
                if (start == 0  ||  !Character.isJavaIdentifierPart(expression.charAt(start - 1))) {
                    int end = matcher.end();
                    int val = 0;

                    for (int idx = start; idx < end - 1; idx++) {
                        val <<= 1;
                        val += expression.charAt(idx) - '0';
                    }
                    expression = expression.substring(0, start) + val + expression.substring(end);
                    matcher = m_binaryNumber.matcher(expression);
                }
            } else {
                break;
            }
        } while (true);
        
        return expression;
    }
    

    /*
     * no longer needed - char literals are replaced in tokenizer, except those
     * which stand alone (not in expression). These are evaluated in method parseLiteral()
     * 
    protected String replaceCharLiterals(String expression) {
        Matcher matcher = m_charNumber.matcher(expression);
        do {
            if (matcher.find()) {
                int start = matcher.start();
                
                int end = matcher.end();
                int val = expression.charAt(start + 1); 

                expression = expression.substring(0, start) + val + expression.substring(end);
                matcher = m_charNumber.matcher(expression);
            } else {
                break;
            }
        } while (true);
        
        return expression;
    }  */
    
    
    /** Replaces hex literals with decimal ones, which are understood by JEP. */
    protected String replaceHexLiterals(String expression) {
        Matcher matcher = m_hexNumber.matcher(expression);
        do {
            if (matcher.find()) {
                int start = matcher.start();
                if (start == 0  ||  !Character.isJavaIdentifierPart(expression.charAt(start - 1))) {
                    int end = matcher.end();
                    int val = 0;

                    for (int idx = start; idx < end - 1; idx++) {
                        val <<= 4;
                        char digit = expression.charAt(idx); 
                        val += digit <= '9' ? digit - '0' : Character.toUpperCase(digit) - 'A' + 10;
                    }
                    expression = expression.substring(0, start) + val + expression.substring(end);
                    matcher = m_hexNumber.matcher(expression);
                }
            } else {
                break;
            }
        } while (true);
        
        return expression;
    }
    
    
    /**
     * This method is much faster than JEPLite expr. parser.
     *  
     * Checks if expression is composed of literal only (dec, binary, hex or char).
     * If yes, sets m_literalValue to the value and returns true. If it returns
     * false, m_literalValue is undefined.
     * @param expr
     */
    private boolean parseLiteral(String expr) {
        boolean isExpression = true;

        // check for literal numbers
        m_literalValue = 0;
        if (expr.charAt(0) >= '0'  &&  expr.charAt(0) <= '9') {    
            char lastChar = expr.charAt(expr.length() - 1);
            isExpression = false;
            if (lastChar == 'B') {
                // binary number
                int len = expr.length() - 1; // the last char is 'B'
                for (int i = 0; i < len; i++) {
                    if (expr.charAt(i) >= '0'  &&  expr.charAt(i) <= '1') {
                        m_literalValue <<= 1;
                        m_literalValue += expr.charAt(i) - '0';
                    } else {
                        isExpression = true;
                        break;
                    }
                }
            } else if (lastChar == 'H') {
                // hex number
                int len = expr.length() - 1; // the last char is 'H'
                for (int i = 0; i < len; i++) {
                    char c = expr.charAt(i);
                    if (c >= '0'  &&  c <= '9') {
                        m_literalValue <<= 4;
                        m_literalValue += c - '0';
                    } else if (c >= 'A'  &&  c <= 'F') {
                        m_literalValue <<= 4;
                        m_literalValue += c - 'A' + 10;
                    } else {
                        isExpression = true;
                        break;
                    }
                }
            } else {
                // decimal number
                int len = expr.length();
                for (int i = 0; i < len; i++) {
                    if (expr.charAt(i) >= '0'  &&  expr.charAt(i) <= '9') {
                        m_literalValue *= 10;
                        m_literalValue += expr.charAt(i) - '0';
                    } else {
                        isExpression = true;
                        break;
                    }
                }
            }
        } else if (expr.charAt(0) == '"'  ||  expr.charAt(0) == '\'') {
            // char literal
            if (expr.length() == 3) {  // 2 quotes and char
                char c = expr.charAt(0);
                if (c == expr.charAt(2)) {  // quote type must be the same
                    m_literalValue = expr.charAt(1); 
                    isExpression = false;
                } else {
                    throw new IllegalArgumentException("Invalid char expression: " + expr);
                }
            } else {
                isExpression = true;
            }
        }

        return !isExpression;
    }    
}
