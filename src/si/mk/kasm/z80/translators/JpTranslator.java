package si.mk.kasm.z80.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.z80.IndexedAddressingArgs;

/** Translates JP instructions. */
class JpTranslator extends TranslatorBaseZ80 {

    private Map<String, OpCode> m_jpInstructions = new HashMap<String, OpCode>();

    public JpTranslator() {
        
        m_jpInstructions.put("JP (HL)", new OpCode(0xe9));
        m_jpInstructions.put("JP (IX)", new OpCode(0xdd, 0xe9));
        m_jpInstructions.put("JP (IY)", new OpCode(0xfd, 0xe9));
    }

    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {

        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();
        OpCode opCode = null;
        
        if (op1 == null) {
            throw new AssemblerException("Missing operand!", asmLine);
        }
        
        if (op2 != null) {
            // JP cc,nn
            opCode = new OpCode(0xc2, 0, 0); //newOpCode(m_opCodeContainer.getJpInstructions(), KAsm.CMD_JP + " cc,nn");
            int conditionIdx = getConditionIndex(op1, asmLine);
            opCode.setRegister8(0, 3, conditionIdx);
            opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_16, op2);
        } else {
            IndexedAddressingArgs args = new IndexedAddressingArgs(op1, m_registerGroups.getRegisters16_SPXY());
            if (args.isIndirectAddressing()) {
                // JP (HL)
                // JP (IX)
                // JP (IY)
                
                // "asmLine.getFormattedInstruction()" must be used, not 'args', because IY+d format is not allowed here
                opCode = newOpCode(m_jpInstructions, asmLine.toNormalizedCmdString());
            } else {
                // JP nn
                opCode = new OpCode(0xc3, 0, 0); //newOpCode(m_opCodeContainer.getJpInstructions(), "JP nn");
                opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_16, op1);
            }
        }
        
        return opCode;
    }
}
