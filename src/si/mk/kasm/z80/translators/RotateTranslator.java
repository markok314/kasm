package si.mk.kasm.z80.translators;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.z80.IndexedAddressingArgs;
import si.mk.kasm.z80.RegisterGroups;

/** Translates rotate (RL, RR, ...) instructions. */
class RotateTranslator extends TranslatorBaseZ80 {

    public static final int ROT_IDX_RLC = 0;
    public static final int ROT_IDX_RL = 2;
    public static final int ROT_IDX_RRC = 1;
    public static final int ROT_IDX_RR = 3;
    public static final int ROT_IDX_SLA = 4;
    public static final int ROT_IDX_SRA = 5;
    public static final int ROT_IDX_SRL = 7;
    private int m_opCodeIdx;

    public RotateTranslator(int opCodeIdx) {
        m_opCodeIdx = opCodeIdx;
    }
    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        checkPresenceOfOperands(asmLine, 1);
        
        String op1 = asmLine.getOperand1();
        
        int regId = m_registerGroups.getRegister8Id(op1);
        OpCode opCode;
        
        if (regId != RegisterGroups.NO_SUCH_REG) {
            // rotate r
            opCode = new OpCode(0xcb, 0x0);// newOpCode(m_opCodeContainer.getRotateInstructions(), "r");
            opCode.setRegister8(1, 0, regId);
            opCode.setRegister8(1, 3, m_opCodeIdx); // instruction index
        } else {
            IndexedAddressingArgs args = new IndexedAddressingArgs(op1, m_registerGroups.getRegisters16_SPXY());
            if (args.isIndirectAddressing()) {
                if (args.isHLAddressing()) {
                    // rotate (HL)
                    opCode = new OpCode(0xcb, 0x06); //newOpCode(m_opCodeContainer.getRotateInstructions(), "(HL)");
                    opCode.setRegister8(1, 3, m_opCodeIdx); // instruction index
                } else if (args.isIX_IY()) {
                    if (args.isIX()) {
                        // rotate (IX + d)
                        opCode = new OpCode(0xdd, 0xcb, 0, 0x06); //newOpCode(m_opCodeContainer.getRotateInstructions(), args.getGenericOperand());
                    } else {
                        // rotate (IY + d)
                        opCode = new OpCode(0xfd, 0xcb, 0, 0x06); 
                    }
                    opCode.setRegister8(3, 3, m_opCodeIdx); // instruction index
                    opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_8_UNSIGNED, args.getExpression()); // d offset
                } else {
                    throw new AssemblerException("Invalid register in addressing mode!", asmLine);
                }
            } else {
                throw new AssemblerException("Invalid addressing mode!", asmLine);
            }
        }
        return opCode;
    }
}
