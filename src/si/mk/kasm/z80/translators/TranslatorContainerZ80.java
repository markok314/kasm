package si.mk.kasm.z80.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AddressProvider;
import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.ExprParser;
import si.mk.kasm.translators.ITranslatorContainer;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.TranslatorBase;

/**
 * This class contains map of translators, which translate assembler command to 
 * opcode. The exception is simple translator, which translates instructions without 
 * parameters and can not have unique key in the map (because there are many different 
 * instructions without parameter). It is handled as a special case. 
 *  
 * @author markok
 *
 */
public class TranslatorContainerZ80 implements ITranslatorContainer{

//    private Map<String, OpCode> m_simpleInstructions = new HashMap<String, OpCode>();
//    private Map<String, OpCode> m_ldInstructions = new HashMap<String, OpCode>();
//    private Map<String, OpCode> m_ld16Instructions = new HashMap<String, OpCode>();
//    private Map<String, OpCode> m_pushPopInstructions = new HashMap<String, OpCode>();
//    private Map<String, OpCode> m_jmpInstructions = new HashMap<String, OpCode>();
//    private Map<String, OpCode> m_arithmetic8Instructions = new HashMap<String, OpCode>();
//    private Map<String, OpCode> m_arithmetic16Instructions = new HashMap<String, OpCode>();
//    private Map<String, OpCode> m_incDec8Instructions = new HashMap<String, OpCode>();
//    private Map<String, OpCode> m_incDec16Instructions = new HashMap<String, OpCode>();
//    private Map<String, OpCode> m_rotateInstructions = new HashMap<String, OpCode>();

//    private Map<String, OpCode> m_bitInstructions = new HashMap<String, OpCode>();
//    private Map<String, OpCode> m_setResInstructions = new HashMap<String, OpCode>();

//    private Map<String, OpCode> m_jpInstructions = new HashMap<String, OpCode>();
//    private Map<String, OpCode> m_jrInstructions = new HashMap<String, OpCode>();

    
    private Map<String, TranslatorBase> m_translators = new HashMap<String, TranslatorBase>();
    private SimpleTranslator m_simpleTranslator = new SimpleTranslator();
    
    /** Initializes internal tables. */
    public TranslatorContainerZ80(AddressProvider addressProvider, ExprParser exprParser) {
        // initOpCodeTables();
        initInstrTranslationMap(addressProvider, exprParser);
    }
    
    
    /**
     * Translates the assembler string in asmLine to op code. Returns true, if 
     * translation was successful, false otherwise. False means, that the given
     * asmLine does not contain valid processor instruction, but it may still 
     * contain valid compiler directive.
     *    
     * @param asmLine
     * @return
     * @throws AssemblerException
     */
    public boolean translateCmdToOpCode(AsmLine asmLine) throws AssemblerException {
        TranslatorBase translator = m_translators.get(asmLine.getCmd());
        if (translator != null) {
            OpCode opCode = translator.createOpCode(asmLine);
            asmLine.setOpCode(opCode);
            return true;
        }  

        // check also for simple instructions, which have no parameters or they are always the same
        OpCode opCode = m_simpleTranslator.createOpCode(asmLine);
        if (opCode != null) {
            asmLine.setOpCode(opCode);
            return true;
        } 
        
        return false;
    }

/*    
    private OpCode createOpCodeForSimpleInstructions(AsmLine asmLine) {
        String formattedAsmInstr = asmLine.toNormalizedCmdString();
        
        // this method does not call newOpCode(), because simple instructions 
        // have constant op code, therefore cloning is not necessary 
        OpCode opCode = m_simpleInstructions.get(formattedAsmInstr);
        return opCode;
    }
    */
    private void initInstrTranslationMap(AddressProvider addressProvider, ExprParser exprParser) {
        m_translators.put("LD", new LdTranslator());
        m_translators.put("INC", new IncDecTranslator(IncDecTranslator.INC_IDX));
        m_translators.put("DEC", new IncDecTranslator(IncDecTranslator.DEC_IDX));
        m_translators.put("PUSH", new PushPopTranslator());
        m_translators.put("POP", new PushPopTranslator());

        m_translators.put("ADD", new ArithmeticTranslator(ArithmeticTranslator.ARITH_IDX_ADD));
        m_translators.put("ADC", new ArithmeticTranslator(ArithmeticTranslator.ARITH_IDX_ADC));
        m_translators.put("SUB", new ArithmeticTranslator(ArithmeticTranslator.ARITH_IDX_SUB));
        m_translators.put("SBC", new ArithmeticTranslator(ArithmeticTranslator.ARITH_IDX_SBC));
        m_translators.put("AND", new ArithmeticTranslator(ArithmeticTranslator.ARITH_IDX_AND));
        m_translators.put("OR", new ArithmeticTranslator(ArithmeticTranslator.ARITH_IDX_OR));
        m_translators.put("XOR", new ArithmeticTranslator(ArithmeticTranslator.ARITH_IDX_XOR));
        m_translators.put("CP", new ArithmeticTranslator(ArithmeticTranslator.ARITH_IDX_CP));
        
        m_translators.put("RLC", new RotateTranslator(RotateTranslator.ROT_IDX_RLC));
        m_translators.put("RL", new RotateTranslator(RotateTranslator.ROT_IDX_RL));
        m_translators.put("RRC", new RotateTranslator(RotateTranslator.ROT_IDX_RRC));
        m_translators.put("RR", new RotateTranslator(RotateTranslator.ROT_IDX_RR));
        m_translators.put("SLA", new RotateTranslator(RotateTranslator.ROT_IDX_SLA));
        m_translators.put("SRA", new RotateTranslator(RotateTranslator.ROT_IDX_SRA));
        m_translators.put("SRL", new RotateTranslator(RotateTranslator.ROT_IDX_SRL));

        m_translators.put("BIT", new BitTranslator());
        m_translators.put("SET", new SetResTranslator(SetResTranslator.IDX_SET));
        m_translators.put("RES", new SetResTranslator(SetResTranslator.IDX_RES));
        
        m_translators.put("JP", new JpTranslator());
        m_translators.put("JR", new JrTranslator(addressProvider));
        m_translators.put("DJNZ", new JrTranslator(addressProvider));

        m_translators.put("CALL", new CallTranslator());
        m_translators.put("RET", new RetTranslator());
        m_translators.put("RST", new RstTranslator(exprParser));

        
        m_translators.put("IN", new InTranslator());
        m_translators.put("OUT", new OutTranslator());
    }
    
    /** 
     * Commented opcodes were moved to translators to avoid map searches.
     */
//    private void initOpCodeTables() {
        // m_ldInstructions.put("LD r,s", new OpCode(0x40));
//        m_ldInstructions.put("LD r,n", new OpCode(0x06, 0));

//        m_ldInstructions.put("LD r,(HL)", new OpCode(0x46));
//        m_ldInstructions.put("LD r,(IX+d)", new OpCode(0xdd, 0x46, 0));
//        m_ldInstructions.put("LD r,(IY+d)", new OpCode(0xfd, 0x46, 0));
        
//        m_ldInstructions.put("LD (HL),r", new OpCode(0x70));
//        m_ldInstructions.put("LD (IX+d),r", new OpCode(0xdd, 0x70, 0));
//        m_ldInstructions.put("LD (IY+d),r", new OpCode(0xfd, 0x70, 0));
        
//        m_ldInstructions.put("LD (HL),n", new OpCode(0x36, 0));
//        m_ldInstructions.put("LD (IX+d),n", new OpCode(0xdd, 0x36, 0, 0));
//        m_ldInstructions.put("LD (IY+d),n", new OpCode(0xfd, 0x36, 0, 0));
        
//        m_ldInstructions.put("LD A,(BC)", new OpCode(0x0a));
//        m_ldInstructions.put("LD A,(DE)", new OpCode(0x1a));
//        m_ldInstructions.put("LD A,(nn)", new OpCode(0x3a, 0, 0));
        
//        m_ldInstructions.put("LD (BC),A", new OpCode(0x02));
//        m_ldInstructions.put("LD (DE),A", new OpCode(0x12));
//         m_ldInstructions.put("LD (nn),A", new OpCode(0x32, 0, 0));
        
//        m_ldInstructions.put("LD A,I", new OpCode(0xed, 0x57));
//        m_ldInstructions.put("LD A,R", new OpCode(0xed, 0x5f));
        
//        m_ldInstructions.put("LD I,A", new OpCode(0xed, 0x47));
//        m_ldInstructions.put("LD R,A", new OpCode(0xed, 0x4f));
        
//         m_ld16Instructions.put("LD dd,nn", new OpCode(0x01, 0, 0));
/*x        m_ld16Instructions.put("LD IX,nn", new OpCode(0xdd, 0x21, 0, 0));
        m_ld16Instructions.put("LD IY,nn", new OpCode(0xfd, 0x21, 0, 0));
//        m_ld16Instructions.put("LD HL,(nn)", new OpCode(0x2a, 0, 0));
        m_ld16Instructions.put("LD BC,(nn)", new OpCode(0xed, 0x4b, 0, 0));
        m_ld16Instructions.put("LD DE,(nn)", new OpCode(0xed, 0x5b, 0, 0));
        m_ld16Instructions.put("LD SP,(nn)", new OpCode(0xed, 0x7b, 0, 0));
        m_ld16Instructions.put("LD IX,(nn)", new OpCode(0xdd, 0x2a, 0, 0));
        m_ld16Instructions.put("LD IY,(nn)", new OpCode(0xfd, 0x2a, 0, 0));
        
        m_ld16Instructions.put("LD (nn),HL", new OpCode(0x22, 0, 0));
        m_ld16Instructions.put("LD (nn),BC", new OpCode(0xed, 0x43, 0, 0));
        m_ld16Instructions.put("LD (nn),DE", new OpCode(0xed, 0x53, 0, 0));
        m_ld16Instructions.put("LD (nn),SP", new OpCode(0xed, 0x73, 0, 0));
        m_ld16Instructions.put("LD (nn),IX", new OpCode(0xdd, 0x22, 0, 0));
        m_ld16Instructions.put("LD (nn),IY", new OpCode(0xfd, 0x22, 0, 0));
        
        m_ld16Instructions.put("LD SP,HL", new OpCode(0xf9));
        m_ld16Instructions.put("LD SP,IX", new OpCode(0xdd, 0xf9));
        m_ld16Instructions.put("LD SP,IY", new OpCode(0xfd, 0xf9));

        m_pushPopInstructions.put("PUSH qq", new OpCode(0xc5));
        m_pushPopInstructions.put("PUSH IX", new OpCode(0xdd, 0xe5));
        m_pushPopInstructions.put("PUSH IY", new OpCode(0xfd, 0xe5));
        m_pushPopInstructions.put("POP qq", new OpCode(0xc1));
        m_pushPopInstructions.put("POP IX", new OpCode(0xdd, 0xe1));
        m_pushPopInstructions.put("POP IY", new OpCode(0xfd, 0xe1));
*/

        // m_arithmetic8Instructions.put("a A,r", new OpCode(0x80));
        // m_arithmetic8Instructions.put("a A,n", new OpCode(0xc6, 0));
/*
        m_arithmetic8Instructions.put("a A,(HL)", new OpCode(0x86));
        m_arithmetic8Instructions.put("a A,(IX+d)", new OpCode(0xdd, 0x86, 0));
        m_arithmetic8Instructions.put("a A,(IY+d)", new OpCode(0xfd, 0x86, 0));
*/
        // m_arithmetic16Instructions.put("ADD HL,ss", new OpCode(0x09));
/*        m_arithmetic16Instructions.put("ADC HL,ss", new OpCode(0xed, 0x4a));
        m_arithmetic16Instructions.put("SBC HL,ss", new OpCode(0xed, 0x42));
        // m_arithmetic16Instructions.put("ADD IX,pp", new OpCode(0xdd, 0x09));
        // m_arithmetic16Instructions.put("ADD IY,rr", new OpCode(0xfd, 0x09));
        // m_incDec8Instructions.put("i r", new OpCode(0));
        m_incDec8Instructions.put("i (HL)", new OpCode(0x30));
        m_incDec8Instructions.put("i (IX+d)", new OpCode(0xdd, 0x30, 0));
        m_incDec8Instructions.put("i (IY+d)", new OpCode(0xfd, 0x30, 0));
        
        // 4 and 5 are values used for op code in 8-bit inc/dec, and they are reused
        // in 16-bit inc/dec
        m_incDec16Instructions.put("4 ss", new OpCode(0x03));
        m_incDec16Instructions.put("4 IX", new OpCode(0xdd, 0x23));
        m_incDec16Instructions.put("4 IY", new OpCode(0xfd, 0x23));
        m_incDec16Instructions.put("5 ss", new OpCode(0x0b));
        m_incDec16Instructions.put("5 IX", new OpCode(0xdd, 0x2b));
        m_incDec16Instructions.put("5 IY", new OpCode(0xfd, 0x2b));
*/        
        
        // m_rotateInstructions.put("r", new OpCode(0xcb, 0x0));
        // m_rotateInstructions.put("(HL)", new OpCode(0xcb, 0x06));
        // m_rotateInstructions.put("(IX+d)", new OpCode(0xdd, 0xcb, 0, 0x06));
        // m_rotateInstructions.put("(IY+d)", new OpCode(0xfd, 0xcb, 0, 0x06));
        
        // m_bitInstructions.put("r", new OpCode(0xcb, 0x40));
        // m_bitInstructions.put("(HL)", new OpCode(0xcb, 0x46));
        // m_bitInstructions.put("(IX+d)", new OpCode(0xdd, 0xcb, 0, 0x46));
        // m_bitInstructions.put("(IY+d)", new OpCode(0xfd, 0xcb, 0, 0x46));
        
        // m_setResInstructions.put("r", new OpCode(0xcb, 0x00));
        // m_setResInstructions.put("(HL)", new OpCode(0xcb, 0x06));
        // m_setResInstructions.put("(IX+d)", new OpCode(0xdd, 0xcb, 0, 0x06));
        // m_setResInstructions.put("(IY+d)", new OpCode(0xfd, 0xcb, 0, 0x06));
//        m_setResInstructions.put("(IX)", new OpCode(0xdd, 0xcb, 0, 0x06));
  //      m_setResInstructions.put("(IY)", new OpCode(0xfd, 0xcb, 0, 0x06));
        
        // m_jpInstructions.put("JP nn", new OpCode(0xc3, 0, 0));
        // m_jpInstructions.put("JP cc,nn", new OpCode(0xc2, 0, 0));
/*        m_jpInstructions.put("JP (HL)", new OpCode(0xe9));
        m_jpInstructions.put("JP (IX)", new OpCode(0xdd, 0xe9));
        m_jpInstructions.put("JP (IY)", new OpCode(0xfd, 0xe9));
*/
/*        m_jrInstructions.put("JR e", new OpCode(0x18, 0));
        m_jrInstructions.put("JR C,e", new OpCode(0x38, 0));
        m_jrInstructions.put("JR NC,e", new OpCode(0x30, 0));
        m_jrInstructions.put("JR Z,e", new OpCode(0x28, 0));
        m_jrInstructions.put("JR NZ,e", new OpCode(0x20, 0));
        m_jrInstructions.put("DJNZ e", new OpCode(0x10, 0));
*/        
        /*
        m_simpleInstructions.put("EX DE,HL", new OpCode(0xeb));
        m_simpleInstructions.put("EX AF,AF'", new OpCode(0x08));
        m_simpleInstructions.put("EXX", new OpCode(0xd9));
        m_simpleInstructions.put("EX (SP),HL", new OpCode(0xE3));
        m_simpleInstructions.put("EX (SP),IX", new OpCode(0xdd, 0xe3));
        m_simpleInstructions.put("EX (SP),IY", new OpCode(0xfd, 0xe3));
        m_simpleInstructions.put("LDI", new OpCode(0xed, 0xa0));
        m_simpleInstructions.put("LDIR", new OpCode(0xed, 0xb0));
        m_simpleInstructions.put("LDD", new OpCode(0xed, 0xa8));
        m_simpleInstructions.put("LDDR", new OpCode(0xed, 0xb8));
        m_simpleInstructions.put("CPI", new OpCode(0xed, 0xa1));
        m_simpleInstructions.put("CPIR", new OpCode(0xed, 0xb1));
        m_simpleInstructions.put("CPD", new OpCode(0xed, 0xa9));
        m_simpleInstructions.put("CPDR", new OpCode(0xed, 0xb9));
        m_simpleInstructions.put("DAA", new OpCode(0x27));
        m_simpleInstructions.put("CPL", new OpCode(0x2f));
        m_simpleInstructions.put("NEG", new OpCode(0xed, 0x44));
        m_simpleInstructions.put("CCF", new OpCode(0x3f));
        m_simpleInstructions.put("SCF", new OpCode(0x37));
        m_simpleInstructions.put("NOP", new OpCode(0x00));
        m_simpleInstructions.put("HALT", new OpCode(0x76));
        m_simpleInstructions.put("DI", new OpCode(0xf3));
        m_simpleInstructions.put("EI", new OpCode(0xfb));
        m_simpleInstructions.put("IM 0", new OpCode(0xed, 0x46));
        m_simpleInstructions.put("IM 1", new OpCode(0xed, 0x56));
        m_simpleInstructions.put("IM 2", new OpCode(0xed, 0x5e));
        
        m_simpleInstructions.put("RETI", new OpCode(0xed, 0x4d));
        m_simpleInstructions.put("RETN", new OpCode(0xed, 0x45));

        m_simpleInstructions.put("RLCA", new OpCode(0x07));
        m_simpleInstructions.put("RLA", new OpCode(0x17));
        m_simpleInstructions.put("RRCA", new OpCode(0x0f));
        m_simpleInstructions.put("RRA", new OpCode(0x1f));
        m_simpleInstructions.put("RLD", new OpCode(0xed, 0x6f));
        m_simpleInstructions.put("RRD", new OpCode(0xed, 0x67));
        
        m_simpleInstructions.put("INI", new OpCode(0xed, 0xa2));
        m_simpleInstructions.put("INIR", new OpCode(0xed, 0xb2));
        m_simpleInstructions.put("IND", new OpCode(0xed, 0xaa));
        m_simpleInstructions.put("INDR", new OpCode(0xed, 0xba));
        
        m_simpleInstructions.put("OUTI", new OpCode(0xed, 0xa3));
        m_simpleInstructions.put("OTIR", new OpCode(0xed, 0xb3));
        m_simpleInstructions.put("OUTD", new OpCode(0xed, 0xab));
        m_simpleInstructions.put("OTDR", new OpCode(0xed, 0xbb));
        */
//    }

    /*
    public Map<String, OpCode> getSimpleInstructions() {
        return m_simpleInstructions;
    } */

/*
    public Map<String, OpCode> getLdInstructions() {
        return m_ldInstructions;
    }
*/
/*
    public Map<String, OpCode> getLd16Instructions() {
        return m_ld16Instructions;
    }
*/
/*
    public Map<String, OpCode> getPushPopInstructions() {
        return m_pushPopInstructions;
    }
*/ 
/*
    public Map<String, OpCode> getJmpInstructions() {
        return m_jmpInstructions;
    }
*/
/*
    public Map<String, OpCode> getArithmetic8Instructions() {
        return m_arithmetic8Instructions;
    }
*/
/*
    public Map<String, OpCode> getArithmetic16Instructions() {
        return m_arithmetic16Instructions;
    }
*/
/*
    public Map<String, OpCode> getIncDec8Instructions() {
        return m_incDec8Instructions;
    }


    public Map<String, OpCode> getIncDec16Instructions() {
        return m_incDec16Instructions;
    }
*/
/*
    public Map<String, OpCode> getRotateInstructions() {
        return m_rotateInstructions;
    }
*/
/*
    public Map<String, OpCode> getBitInstructions() {
        return m_bitInstructions;
    }
*/
/*
    public Map<String, OpCode> getSetResInstructions() {
        return m_setResInstructions;
    }
*/
/*
    public Map<String, OpCode> getJpInstructions() {
        return m_jpInstructions;
    }
*/
/*
    public Map<String, OpCode> getJrInstructions() {
        return m_jrInstructions;
    }
  */  
}
