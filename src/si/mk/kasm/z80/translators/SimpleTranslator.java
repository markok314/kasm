package si.mk.kasm.z80.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;

/** Translates instructions without parameters. */

class SimpleTranslator extends TranslatorBaseZ80 {

    private Map<String, OpCode> m_simpleInstructions = new HashMap<String, OpCode>();

    public SimpleTranslator() {
        
        m_simpleInstructions.put("EX DE,HL", new OpCode(0xeb));
        m_simpleInstructions.put("EX AF,AF'", new OpCode(0x08));
        m_simpleInstructions.put("EXX", new OpCode(0xd9));
        m_simpleInstructions.put("EX (SP),HL", new OpCode(0xE3));
        m_simpleInstructions.put("EX (SP),IX", new OpCode(0xdd, 0xe3));
        m_simpleInstructions.put("EX (SP),IY", new OpCode(0xfd, 0xe3));
        m_simpleInstructions.put("LDI", new OpCode(0xed, 0xa0));
        m_simpleInstructions.put("LDIR", new OpCode(0xed, 0xb0));
        m_simpleInstructions.put("LDD", new OpCode(0xed, 0xa8));
        m_simpleInstructions.put("LDDR", new OpCode(0xed, 0xb8));
        m_simpleInstructions.put("CPI", new OpCode(0xed, 0xa1));
        m_simpleInstructions.put("CPIR", new OpCode(0xed, 0xb1));
        m_simpleInstructions.put("CPD", new OpCode(0xed, 0xa9));
        m_simpleInstructions.put("CPDR", new OpCode(0xed, 0xb9));
        m_simpleInstructions.put("DAA", new OpCode(0x27));
        m_simpleInstructions.put("CPL", new OpCode(0x2f));
        m_simpleInstructions.put("NEG", new OpCode(0xed, 0x44));
        m_simpleInstructions.put("CCF", new OpCode(0x3f));
        m_simpleInstructions.put("SCF", new OpCode(0x37));
        m_simpleInstructions.put("NOP", new OpCode(0x00));
        m_simpleInstructions.put("HALT", new OpCode(0x76));
        m_simpleInstructions.put("DI", new OpCode(0xf3));
        m_simpleInstructions.put("EI", new OpCode(0xfb));
        m_simpleInstructions.put("IM 0", new OpCode(0xed, 0x46));
        m_simpleInstructions.put("IM 1", new OpCode(0xed, 0x56));
        m_simpleInstructions.put("IM 2", new OpCode(0xed, 0x5e));
        
        m_simpleInstructions.put("RETI", new OpCode(0xed, 0x4d));
        m_simpleInstructions.put("RETN", new OpCode(0xed, 0x45));

        m_simpleInstructions.put("RLCA", new OpCode(0x07));
        m_simpleInstructions.put("RLA", new OpCode(0x17));
        m_simpleInstructions.put("RRCA", new OpCode(0x0f));
        m_simpleInstructions.put("RRA", new OpCode(0x1f));
        m_simpleInstructions.put("RLD", new OpCode(0xed, 0x6f));
        m_simpleInstructions.put("RRD", new OpCode(0xed, 0x67));
        
        m_simpleInstructions.put("INI", new OpCode(0xed, 0xa2));
        m_simpleInstructions.put("INIR", new OpCode(0xed, 0xb2));
        m_simpleInstructions.put("IND", new OpCode(0xed, 0xaa));
        m_simpleInstructions.put("INDR", new OpCode(0xed, 0xba));
        
        m_simpleInstructions.put("OUTI", new OpCode(0xed, 0xa3));
        m_simpleInstructions.put("OTIR", new OpCode(0xed, 0xb3));
        m_simpleInstructions.put("OUTD", new OpCode(0xed, 0xab));
        m_simpleInstructions.put("OTDR", new OpCode(0xed, 0xbb));
    }

    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        String formattedAsmInstr = asmLine.toNormalizedCmdString();
        
        // this method does not call newOpCode(), because simple instructions 
        // have constant op code, therefore cloning is not necessary 
        OpCode opCode = m_simpleInstructions.get(formattedAsmInstr);
        return opCode;
    }

}
