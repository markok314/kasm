package si.mk.kasm.z80.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.z80.IndexedAddressingArgs;
import si.mk.kasm.z80.RegisterGroups;

/** Translates 8 and 16-bit LD instructions. */
class LdTranslator extends TranslatorBaseZ80 {

    public static final String CMD_LD = "LD";

    private Map<String, OpCode> m_ld16Instructions = new HashMap<String, OpCode>();
    
    public LdTranslator() {
        
        m_ld16Instructions.put("LD IX,nn", new OpCode(0xdd, 0x21, 0, 0));
        m_ld16Instructions.put("LD IY,nn", new OpCode(0xfd, 0x21, 0, 0));
//        m_ld16Instructions.put("LD HL,(nn)", new OpCode(0x2a, 0, 0));
        m_ld16Instructions.put("LD BC,(nn)", new OpCode(0xed, 0x4b, 0, 0));
        m_ld16Instructions.put("LD DE,(nn)", new OpCode(0xed, 0x5b, 0, 0));
        m_ld16Instructions.put("LD SP,(nn)", new OpCode(0xed, 0x7b, 0, 0));
        m_ld16Instructions.put("LD IX,(nn)", new OpCode(0xdd, 0x2a, 0, 0));
        m_ld16Instructions.put("LD IY,(nn)", new OpCode(0xfd, 0x2a, 0, 0));
        
        m_ld16Instructions.put("LD (nn),HL", new OpCode(0x22, 0, 0));
        m_ld16Instructions.put("LD (nn),BC", new OpCode(0xed, 0x43, 0, 0));
        m_ld16Instructions.put("LD (nn),DE", new OpCode(0xed, 0x53, 0, 0));
        m_ld16Instructions.put("LD (nn),SP", new OpCode(0xed, 0x73, 0, 0));
        m_ld16Instructions.put("LD (nn),IX", new OpCode(0xdd, 0x22, 0, 0));
        m_ld16Instructions.put("LD (nn),IY", new OpCode(0xfd, 0x22, 0, 0));
        
        m_ld16Instructions.put("LD SP,HL", new OpCode(0xf9));
        m_ld16Instructions.put("LD SP,IX", new OpCode(0xdd, 0xf9));
        m_ld16Instructions.put("LD SP,IY", new OpCode(0xfd, 0xf9));
        
    }

    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {

        asmLine.checkNoOfOperands(2);
        
        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();
        OpCode opCode = null;
        
        if (op1 == null  ||  op2 == null) {
            throw new AssemblerException("Missing operand! 'LD' Instruction requires destination and source!", asmLine);
        }

        // handle 8 bit loads first
        int op1RegisterId = m_registerGroups.getRegister8Id(op1);
        int op2RegisterId = m_registerGroups.getRegister8Id(op2);
        if (op1RegisterId != RegisterGroups.NO_SUCH_REG  &&  op2RegisterId != RegisterGroups.NO_SUCH_REG) {
            // LD r, s                
            OpCode opCodeLdRS = new OpCode(0x40); // new OpCodeLdRS();
            opCodeLdRS.setRegister8(0, 3, op1RegisterId);
            opCodeLdRS.setRegister8(0, 0, op2RegisterId);
            opCode = opCodeLdRS;
            
        } else if (op1RegisterId != RegisterGroups.NO_SUCH_REG) {
            
            IndexedAddressingArgs idxArgs = new IndexedAddressingArgs(op2, m_registerGroups.getRegisters16_SPXY());
            if (idxArgs.isIndirectAddressing()) {
                // LD r, (HL | BC | DE | IX+d | IY+d)
                // String reg = idxArgs.getRegister();
                if (idxArgs.isHLAddressing()) {
                    opCode = new OpCode(0x46); // "LD r,(HL)" //m_opCodeContainer.getLdInstructions(), KAsm.CMD_LD + " r,(HL)");
                    opCode.setRegister8(0, 3, op1RegisterId);
                } else if (idxArgs.isBCAddressing()) {
                    if (op1RegisterId == RegisterGroups.REG_A_ID) {
                        opCode = new OpCode(0x0a); // "LD A,(BC)" m_opCodeContainer.getLdInstructions(), KAsm.CMD_LD + " " + op1 + ",(BC)");
                    } else {
                        throw new AssemblerException("The first operand must be register A!", asmLine);
                    }
                } else if (idxArgs.isDEAddressing()) {
                    //opCode = m_opCodeContainer.getLdInstructions(), KAsm.CMD_LD + " " + op1 + ",(DE)");
                    if (op1RegisterId == RegisterGroups.REG_A_ID) {
                        opCode = new OpCode(0x1a); // "LD A,(DE)" m_opCodeContainer.getLdInstructions(), KAsm.CMD_LD + " " + op1 + ",(BC)");
                    } else {
                        throw new AssemblerException("The first operand must be register A!", asmLine);
                    }
                } else if (idxArgs.isIX_IY()) { // check for IX and IY
                        // String instr = KAsm.CMD_LD + " r," + idxArgs.getGenericOperand();
                    if (idxArgs.isIX()) {
                        opCode = new OpCode(0xdd, 0x46, 0);  // "LD r,(IX+d)"
                    } else {
                        opCode = new OpCode(0xfd, 0x46, 0);  // "LD r,(IY+d)"
                    }
                        
                    opCode.setRegister8(1, 3, op1RegisterId);
                    opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_8_SIGNED, idxArgs.getExpression());
                } else {
                    if (op1RegisterId == RegisterGroups.REG_A_ID) {
                        // LD A,(nn)
                        // String instr = KAsm.CMD_LD + " A,(nn)";
                        // opCode = newOpCode(m_opCodeContainer.getLdInstructions(), instr);
                        opCode = new OpCode(0x3a, 0, 0);
                        opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_16, op2);
                    } else {
                        throw new AssemblerException("Unknown addressing mode!", asmLine);
                    }
                }
            } else {
                // LD A, I
                // LD A, R
                if (op2.equals("I")) {
                    opCode = new OpCode(0xed, 0x57);  // "LD A,I"
                } else if (op2.equals("R")) {
                    opCode = new OpCode(0xed, 0x5f);  // "LD A,R"
                } else {
                    // LD r, n
                    // String instr = KAsm.CMD_LD + " r,n";
                    // opCode = newOpCode(m_opCodeContainer.getLdInstructions(), instr);
                    opCode = new OpCode(0x06, 0);
                    opCode.setRegister8(0, 3, op1RegisterId);
                    opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_8_UNSIGNED, op2);
                }
            }
        } else if (op2RegisterId != RegisterGroups.NO_SUCH_REG) {
            IndexedAddressingArgs idxArgs = new IndexedAddressingArgs(op1, m_registerGroups.getRegisters16_SPXY());
            if (idxArgs.isIndirectAddressing()) {
                // LD (HL | BC | DE | IX+d | IY+d), r
                if (idxArgs.isHLAddressing()) {
                    opCode = new OpCode(0x70);
                    // opCode = newOpCode(m_opCodeContainer.getLdInstructions(), KAsm.CMD_LD + " (HL),r");
                    opCode.setRegister8(0, 0, op2RegisterId);
                } else if (idxArgs.isBCAddressing()) {
                    // LD (BC),A
                    // opCode = newOpCode(m_opCodeContainer.getLdInstructions(), KAsm.CMD_LD + " (BC)," + op2);
                    if (op2RegisterId == RegisterGroups.REG_A_ID) {
                        opCode = new OpCode(0x02);
                    } else {
                        throw new AssemblerException("The second operand must be register 'A'!", asmLine);
                    }
                } else if (idxArgs.isDEAddressing()) {
                    // LD (DE),A
                    // opCode = newOpCode(m_opCodeContainer.getLdInstructions(), KAsm.CMD_LD + " (DE)," + op2);
                    if (op2RegisterId == RegisterGroups.REG_A_ID) {
                        opCode = new OpCode(0x12);
                    } else {
                        throw new AssemblerException("The second operand must be register 'A'!", asmLine);
                    }
                } else if (idxArgs.isIX_IY()) { // check for IX and IY
                    // String instr = KAsm.CMD_LD + " " + idxArgs.getGenericOperand() + ",r";
                    if (idxArgs.isIX()) {
                        opCode = new OpCode(0xdd, 0x70, 0); // "LD (IX+d),r"
                    } else {
                        opCode = new OpCode(0xfd, 0x70, 0); // "LD (IY+d),r"
                    }
                    // opCode = newOpCode(m_opCodeContainer.getLdInstructions(), instr);
                    opCode.setRegister8(1, 0, op2RegisterId);
                    opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_8_SIGNED, idxArgs.getExpression());
                } else if (op2RegisterId == RegisterGroups.REG_A_ID) {
                    // LD (nn),A
                    //String instr = KAsm.CMD_LD + " (nn),A";
                    //opCode = newOpCode(m_opCodeContainer.getLdInstructions(), instr);
                    opCode = new OpCode(0x32, 0, 0);  // "LD (nn),A"
                    opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_16, op1);
                } else {
                    throw new AssemblerException("Unknown addressing mode!", asmLine);
                }
            } else {
                // LD I, A
                // LD R, A
                if (op1.equals("I")) {
                    // String instr = KAsm.CMD_LD + " " + op1 + ",A";
                    // opCode = newOpCode(m_opCodeContainer.getLdInstructions(), instr);
                    opCode = new OpCode(0xed, 0x47);  // "LD I,A"
                } else if (op1.equals("R")) {
                    opCode = new OpCode(0xed, 0x4f);  // "LD R,A"
                } else {
                    throw new AssemblerException("Unknown addressing mode!", asmLine);
                }
            }

        } else {
            int op1Register16Id = m_registerGroups.getRegister16_SP_Id(op1);
            int op2Register16Id = m_registerGroups.getRegister16_SP_Id(op2);
            
            if (op1Register16Id == RegisterGroups.NO_SUCH_REG  &&  op2Register16Id == RegisterGroups.NO_SUCH_REG) {
                // it is 8 bit indexed addressing
                IndexedAddressingArgs idxArgs = new IndexedAddressingArgs(op1, m_registerGroups.getRegisters16_SPXY());
                if (idxArgs.isIndirectAddressing()) {
                    // LD (IX+d), n
                    // LD (IY+d), n
                    if (idxArgs.isHLAddressing()) {
                        // LD (HL), n
                        // opCode = newOpCode(m_opCodeContainer.getLdInstructions(), KAsm.CMD_LD + " (HL),n");
                        opCode = new OpCode(0x36, 0);  // "LD (HL),n"
                        opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_8_UNSIGNED, op2);
                    } else if (idxArgs.isIX_IY()) { 
                            // String instr = KAsm.CMD_LD + " (" + addrArgs.getRegister() + "+d),n";
                        if (idxArgs.isIX()) {
                            opCode = new OpCode(0xdd, 0x36, 0, 0);  // "LD (IX+d),n"
                        } else {
                            opCode = new OpCode(0xfd, 0x36, 0, 0);  // "LD (IY+d),n"
                        }
                        // opCode = newOpCode(m_opCodeContainer.getLdInstructions(), instr);
                        opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_8_SIGNED, idxArgs.getExpression());
                        opCode.addArgExpression(3, OpCode.ArgumentSize.BITS_8_UNSIGNED, op2);
                    } else {
                        throw new AssemblerException("Invalid operands! Left operand should be IX, IY or HL, or the right operand should be register A!", asmLine);
                    }
                } else {
                    throw new AssemblerException("Invalid operands!", asmLine);
                }
            } else if (op1Register16Id == RegisterGroups.REG_SP_ID  &&  op2Register16Id != RegisterGroups.NO_SUCH_REG) {
                // LD SP, HL
                // LD SP, IX
                // LD SP, IY
                opCode = newOpCode(m_ld16Instructions, CMD_LD + " SP," + op2);
            } else if (op1Register16Id != RegisterGroups.NO_SUCH_REG) {
                if (isIndirectAddressing(op2)) {
                    if (op1Register16Id == RegisterGroups.REG_HL_ID) {
                        // LD HL,(nn)
                        opCode = new OpCode(0x2a, 0, 0); // "LD HL,(nn)"
                        opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_16, op2);
                    } else { // BC, DE, SP, IX and IY
                        // LD dd,(nn)
                        // LD IX,(nn)
                        // LD IY,(nn)
                        String inst = "LD " + op1 + ",(nn)";
                        opCode = newOpCode(m_ld16Instructions, inst);
                        opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_16, op2);
                    }
                } else {
                    if (op1Register16Id == RegisterGroups.REG_IX_ID) {
                        // LD IX,nn
                        opCode = new OpCode(0xdd, 0x21, 0, 0); //"LD IX,nn" 
                        opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_16, op2);
                    } else if (op1Register16Id == RegisterGroups.REG_IY_ID) {
                        // LD IY,nn
                        opCode = new OpCode(0xfd, 0x21, 0, 0); //"LD IY,nn" 
                        opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_16, op2);
                    } else {
                        // LD dd,nn
                        opCode = new OpCode(0x01, 0, 0); // "LD dd,nn"
                        opCode.setRegister16(0, 4, op1Register16Id);
                        opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_16, op2);
                    }
                    // String inst = KAsm.CMD_LD + " " + op1Generic + op2Generic;
                    // opCode = newOpCode(m_opCodeContainer.getLd16Instructions(), inst);
                }
            } else if (op2Register16Id != RegisterGroups.NO_SUCH_REG) {
                if (isIndirectAddressing(op1)) {
                    // LD (nn),HL
                    // LD (nn),dd
                    // LD (nn),IX
                    // LD (nn),IY
                    String inst = CMD_LD + " (nn)," + op2;
                    opCode = newOpCode(m_ld16Instructions, inst);
                    if (op2Register16Id == RegisterGroups.REG_HL_ID) {
                        opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_16, op1);
                    } else { // BC, DE, SP, IX and IY
                        opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_16, op1);
                    }
                } else {
                    throw new AssemblerException("Invalid operands!", asmLine);
                }
            } else {
                throw new AssemblerException("Internal error - illegal program state!", asmLine);
            }
        }
        
        return opCode;
    }
    
}
