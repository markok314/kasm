package si.mk.kasm.z80.translators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.z80.RegisterGroups;

/** Translates OUT instructions. */
class OutTranslator extends TranslatorBaseZ80 {

    private Pattern m_cRegPattern = Pattern.compile("\\(\\s*C\\s*\\)");
    private OpCode m_outNa = new OpCode(0xd3, 0);
    private OpCode m_outCr = new OpCode(0xed, 0x41);
    
    public OutTranslator() {
    }

    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {

        checkPresenceOfOperands(asmLine, 2);
        
        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();

        OpCode opCode = null;
        Matcher matcher = m_cRegPattern.matcher(op1);
        if (matcher.matches()) {
            int regId = m_registerGroups.getRegister8Id(op2);
            if (regId != RegisterGroups.NO_SUCH_REG) {
                // OUT (C), r
                opCode = newOpCode(m_outCr);
                opCode.setRegister8(1, 3, regId);
            } else {
                throw new AssemblerException("Invalid register: " + op1, asmLine);
            }
        } else {
            int regId = m_registerGroups.getRegister8Id(op2);
            if (regId == RegisterGroups.REG_A_ID) {
                // OUT n,A
                opCode = newOpCode(m_outNa);
                opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_8_UNSIGNED, op1);
            } else {
                throw new AssemblerException("Invalid register: " + op1, asmLine);
            }
        }
        
        return opCode;
    }
    
}
