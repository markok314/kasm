package si.mk.kasm.z80.translators;

import java.util.HashMap;
import java.util.Map;

import org.cheffo.jeplite.ParseException;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.ExprParser;
import si.mk.kasm.translators.OpCode;

/**
 * Handles RST instruction. Vector may be given as:
 * <pre>
 * - constant: 08h
 * - expression of constants defined by EQO: VECTOR_8
 * - label defined BEFORE this line, which can be used to detect invalid routine
 *   address, for example: RST LBL_VECTOR_A will report an error if LBL_VECTOR_A
 *   is not a valid address
 * </pre>
 * @author markok
 *
 */
class RstTranslator extends TranslatorBaseZ80 {

    private OpCode m_rst = new OpCode(0xc7);  // "RST p"
    private ExprParser m_parser;
    private static final Map<Integer, Integer> m_vectorMap = new HashMap<Integer, Integer>();
    
    static {
        m_vectorMap.put(0, 0);
        m_vectorMap.put(8, 1);
        m_vectorMap.put(16, 2);
        m_vectorMap.put(24, 3);
        m_vectorMap.put(32, 4);
        m_vectorMap.put(40, 5);
        m_vectorMap.put(48, 6);
        m_vectorMap.put(56, 7);
    }
    
    public RstTranslator(ExprParser parser) {
        m_parser = parser;
    }

    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {

        checkPresenceOfOperands(asmLine, 1);
        
        String op1 = asmLine.getOperand1();
        OpCode opCode = null;
    
        try {
            int vector = m_parser.evaluate(op1);
            opCode = newOpCode(m_rst);
            Integer bitVector = m_vectorMap.get(vector);
            if (bitVector == null) {
                throw new AssemblerException("Invalid value for RST vector: " + vector, asmLine);
            }
            opCode.setRegister8(0, 3, bitVector);
        } catch (ParseException ex) {
            throw new AssemblerException(ex.getMessage(), asmLine);
        }
        
        return opCode;
    }
}
