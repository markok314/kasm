package si.mk.kasm.z80.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.z80.RegisterGroups;

/** Translates PUSH/POP instructions. */
class PushPopTranslator extends TranslatorBaseZ80 {

    private Map<String, OpCode> m_pushPopInstructions = new HashMap<String, OpCode>();
    
    public PushPopTranslator() {
        
        m_pushPopInstructions.put("PUSH qq", new OpCode(0xc5));
        m_pushPopInstructions.put("PUSH IX", new OpCode(0xdd, 0xe5));
        m_pushPopInstructions.put("PUSH IY", new OpCode(0xfd, 0xe5));
        m_pushPopInstructions.put("POP qq", new OpCode(0xc1));
        m_pushPopInstructions.put("POP IX", new OpCode(0xdd, 0xe1));
        m_pushPopInstructions.put("POP IY", new OpCode(0xfd, 0xe1));
    }
    
    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 1);
        String cmd = asmLine.getCmd();
        String op1 = asmLine.getOperand1(); 
        
        OpCode opCode;
        if (op1 != null) {
            int reg16Idx = m_registerGroups.getRegister16_AF_Id(op1);
            if (reg16Idx != RegisterGroups.NO_SUCH_REG) {
                String instr;
                if (reg16Idx == RegisterGroups.REG_IX_ID  ||  reg16Idx == RegisterGroups.REG_IY_ID) {
                    instr = cmd + " " + op1;

                    opCode = newOpCode(m_pushPopInstructions, instr);
                } else {
                    // it is one of BC, DE, HL, AF
                    instr = cmd + " qq";
                    opCode = newOpCode(m_pushPopInstructions, instr);
                    opCode.setRegister16(0, 4, reg16Idx);
                }

            } else {
                throw new AssemblerException(cmd + " requires 16 bit register BC, DE, HL, AF, IX, or IY as operand!", asmLine);
            }
        } else {
            throw new AssemblerException(cmd + " requires 16 bit register as operand!", asmLine);
        }
        
        return opCode;
    }
}
