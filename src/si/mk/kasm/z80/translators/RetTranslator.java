package si.mk.kasm.z80.translators;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;

/** Translates RET instructions. */
class RetTranslator extends TranslatorBaseZ80 {

    private OpCode m_retOpCode = new OpCode(0xc9);  // RET
    private OpCode m_retCCOpCode = new OpCode(0xc0); // RET cc

    public RetTranslator() {
    }

    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {

        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();
        
        if (op2 != null) {
            throw new AssemblerException("To many operands!", asmLine);
        }
        
        if (op1 == null) {  // simple RET instruction, it does not make sense to 
                            // do map search
            return m_retOpCode;
        } 
        
        int condition = getConditionIndex(op1, asmLine);
        
        OpCode opCode = newOpCode(m_retCCOpCode);
        opCode.setRegister8(0, 3, condition);
        
        return opCode;
    }
}
