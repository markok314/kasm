package si.mk.kasm.z80.translators;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;

/** Translates CALL instructions. */
class CallTranslator extends TranslatorBaseZ80 {

    private OpCode m_callOpCode = new OpCode(0xcd, 0, 0);  // CALL nn
    private OpCode m_callCCOpCode = new OpCode(0xc4, 0, 0); // CALL cc,nn

   
    public CallTranslator() {
    }

    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {

        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();
        OpCode opCode = null;
        
        if (op1 == null) {
            throw new AssemblerException("Missing operand(s)!", asmLine);
        }
        
        if (op2 != null) {
            // CALL cc,nn
            opCode = newOpCode(m_callCCOpCode);
            int conditionIdx = getConditionIndex(op1, asmLine);
            opCode.setRegister8(0, 3, conditionIdx);
            opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_16, op2);
        } else {
            // CALL nn
            opCode = newOpCode(m_callOpCode);
            opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_16, op1);
        }
        
        return opCode;
    }
    
}
