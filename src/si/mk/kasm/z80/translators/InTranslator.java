package si.mk.kasm.z80.translators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.z80.RegisterGroups;

/** Translates IN instructions. */
class InTranslator extends TranslatorBaseZ80 {

    private Pattern m_cRegPattern = Pattern.compile("\\(\\s*C\\s*\\)");
    private OpCode m_inAn = new OpCode(0xdb, 0);
    private OpCode m_inrC = new OpCode(0xed, 0x40);

    
    public InTranslator() {
    }

    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {

        checkPresenceOfOperands(asmLine, 2);
        
        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();

        OpCode opCode = null;
        Matcher matcher = m_cRegPattern.matcher(op2);
        if (matcher.matches()) {
            int regId = m_registerGroups.getRegister8Id(op1);
            if (regId != RegisterGroups.NO_SUCH_REG) {
                // IN r,(C)
                opCode = newOpCode(m_inrC);
                opCode.setRegister8(1, 3, regId);
            } else {
                throw new AssemblerException("Invalid register: " + op1, asmLine);
            }
        } else {
            int regId = m_registerGroups.getRegister8Id(op1);
            if (regId == RegisterGroups.REG_A_ID) {
                opCode = newOpCode(m_inAn);
                opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_8_UNSIGNED, op2);
            } else {
                throw new AssemblerException("Invalid register: " + op1, asmLine);
            }
        }
        
        return opCode;
    }
}
