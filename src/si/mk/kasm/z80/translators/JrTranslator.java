package si.mk.kasm.z80.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AddressProvider;
import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;

/** Translates JR instructions. */
class JrTranslator extends TranslatorBaseZ80 {

    private AddressProvider m_addressProvider;
    
    private Map<String, OpCode> m_jrInstructions = new HashMap<String, OpCode>();
    
    public JrTranslator(AddressProvider addressProvider) {
        m_addressProvider = addressProvider;
        
        m_jrInstructions.put("JR e", new OpCode(0x18, 0));
        m_jrInstructions.put("JR C,e", new OpCode(0x38, 0));
        m_jrInstructions.put("JR NC,e", new OpCode(0x30, 0));
        m_jrInstructions.put("JR Z,e", new OpCode(0x28, 0));
        m_jrInstructions.put("JR NZ,e", new OpCode(0x20, 0));
        m_jrInstructions.put("DJNZ e", new OpCode(0x10, 0));
    }


    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {

        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();
        OpCode opCode = null;
        
        if (op1 == null) {
            throw new AssemblerException("Missing operand!", asmLine);
        }
        
        String cmd = asmLine.getCmd();  // may be JR or DJNZ
        
        if (op2 == null) {
            // JR e
            opCode = newOpCode(m_jrInstructions, cmd + " e");
            opCode.addArgExpression(1, 
                                    OpCode.ArgumentSize.BITS_8_SIGNED, 
                                    op1 + " - 2 - " + m_addressProvider.getCurrentAddress());
            // -2 because currentAddress in KAsm is incremented AFTER this method returns
        } else {
            // JR NZ,e
            // JR Z,e
            // JR NC,e
            // JR C,e
            opCode = newOpCode(m_jrInstructions, cmd + " " + op1 + ",e");
            opCode.addArgExpression(1, 
                                    OpCode.ArgumentSize.BITS_8_SIGNED, 
                                    op2 + " - 2 - " + m_addressProvider.getCurrentAddress());
        }
        
        return opCode;
    }
}
