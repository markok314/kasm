package si.mk.kasm.z80.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.TranslatorBase;

abstract class TranslatorBaseZ80 extends TranslatorBase {

    // map containing values of conditions as stored in op code
    protected final static Map<String, Integer> m_conditionsMap = new HashMap<String, Integer>(); 

    static {
        m_conditionsMap.put("NZ", 0);
        m_conditionsMap.put("Z", 1);
        m_conditionsMap.put("NC", 2);
        m_conditionsMap.put("C", 3);

        m_conditionsMap.put("PO", 4);
        m_conditionsMap.put("PE", 5);
        m_conditionsMap.put("P", 6);
        m_conditionsMap.put("M", 7);
    }
    
    
    public TranslatorBaseZ80() {
        super();
    }

    
    @Override
    protected boolean isIndirectAddressing(String operand) {
        {
            if (operand.isEmpty()) {
                return false;
            }

            if (operand.charAt(0) == '('  &&  operand.charAt(operand.length() - 1) == ')') {
                return true;
            }

            return false;
        }
    }

    
    /** Returns condition (C, NC, Z, NZ, ...) bits to be stored to opcode. */
    protected int getConditionIndex(String op1, AsmLine asmLine) throws AssemblerException {
        Integer conditionIdx = m_conditionsMap.get(op1);
        if (conditionIdx == null) {
            throw new AssemblerException("Unknown condition!", asmLine);
        }
        return conditionIdx;
    }
}
