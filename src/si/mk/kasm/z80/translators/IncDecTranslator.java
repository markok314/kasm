package si.mk.kasm.z80.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.z80.IndexedAddressingArgs;
import si.mk.kasm.z80.RegisterGroups;


/** Translates 8 and 16 bit INC DEC instructions. */
class IncDecTranslator extends TranslatorBaseZ80 {

    
    public static final int INC_IDX = 4;
    public static final int DEC_IDX = 5;
    private final int m_cmdIdx;

    private Map<String, OpCode> m_incDec8Instructions = new HashMap<String, OpCode>();
    private Map<String, OpCode> m_incDec16Instructions = new HashMap<String, OpCode>();

    public IncDecTranslator(int cmdIdx) {
        m_cmdIdx = cmdIdx;
        
        m_incDec8Instructions.put("i (HL)", new OpCode(0x30));
        m_incDec8Instructions.put("i (IX+d)", new OpCode(0xdd, 0x30, 0));
        m_incDec8Instructions.put("i (IY+d)", new OpCode(0xfd, 0x30, 0));
        
        // 4 and 5 are values used for op code in 8-bit inc/dec, and they are reused
        // in 16-bit inc/dec
        m_incDec16Instructions.put("4 ss", new OpCode(0x03));
        m_incDec16Instructions.put("4 IX", new OpCode(0xdd, 0x23));
        m_incDec16Instructions.put("4 IY", new OpCode(0xfd, 0x23));
        m_incDec16Instructions.put("5 ss", new OpCode(0x0b));
        m_incDec16Instructions.put("5 IX", new OpCode(0xdd, 0x2b));
        m_incDec16Instructions.put("5 IY", new OpCode(0xfd, 0x2b));
    }

    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {

        checkPresenceOfOperands(asmLine, 1);

        // if we get here, we can be sure, that cmd is one of INC or DEC
        String op1 = asmLine.getOperand1(); 

        OpCode opCode;
        int reg8Id = m_registerGroups.getRegister8Id(op1);
        if (reg8Id != RegisterGroups.NO_SUCH_REG) {
            // it is 8 bit inc/dec
            opCode = new OpCode(0); //newOpCode(m_opCodeContainer.getIncDec8Instructions(), "i r");
            opCode.setRegister8(0, 0, m_cmdIdx); // set inc/dec opcode
            opCode.setRegister8(0, 3, reg8Id);
        } else {
            IndexedAddressingArgs idxArgs = new IndexedAddressingArgs(op1, m_registerGroups.getRegisters16_SPXY());
            if (idxArgs.isIndirectAddressing()) {
                opCode = newOpCode(m_incDec8Instructions, "i " + idxArgs.getGenericOperand());
                if (idxArgs.isIX_IY()) {
                    opCode.setRegister8(1, 0, m_cmdIdx); // set inc/dec opcode
                    opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_8_UNSIGNED, idxArgs.getExpression());
                } else {
                    // it is (HL) addressing
                    opCode.setRegister8(0, 0, m_cmdIdx); // set inc/dec opcode
                }
            } else {
                // it is 16 bit inc dec or error
                int reg16Id = m_registerGroups.getRegister16_SP_Id(op1);
                if (reg16Id == RegisterGroups.NO_SUCH_REG) {
                    throw new AssemblerException("Unknown register for INC/DEC command.", asmLine);
                } 
                if (reg16Id == RegisterGroups.REG_IX_ID  ||  reg16Id == RegisterGroups.REG_IY_ID) {
                    opCode = newOpCode(m_incDec16Instructions, m_cmdIdx + " " + op1);
                } else {
                    // inc/dec BC, DE, HL, SP 
                    opCode = newOpCode(m_incDec16Instructions, m_cmdIdx + " ss");
                    opCode.setRegister16(0, 4, reg16Id); 
                }
            }
        }
        
        return opCode;
    }
}
