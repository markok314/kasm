package si.mk.kasm.z80.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.z80.IndexedAddressingArgs;
import si.mk.kasm.z80.RegisterGroups;


/** Translates arithmetic operations, for example ADD, ADC, SUB, ... */
class ArithmeticTranslator extends TranslatorBaseZ80 {

    public static final int CMD_ADD_ID = 0;
    public static final int CMD_ADC_ID = 1;
    public static final int CMD_SBC_ID = 3; 
    
    public final static int ARITH_IDX_ADD = 0;
    public final static int ARITH_IDX_ADC = 1;
    public final static int ARITH_IDX_SUB = 2;
    public final static int ARITH_IDX_SBC = 3;
    public final static int ARITH_IDX_AND = 4;
    public final static int ARITH_IDX_OR = 6;
    public final static int ARITH_IDX_XOR = 5;
    public final static int ARITH_IDX_CP = 7;
    private final int m_cmdIdx;

    private Map<String, OpCode> m_arithmetic8Instructions = new HashMap<String, OpCode>();
    private Map<String, OpCode> m_arithmetic16Instructions = new HashMap<String, OpCode>();
    
    
    /** Translates arithmetic operations, for example ADD, ADC, SUB, ... */
    public ArithmeticTranslator(int cmdIdx) {
        
        m_cmdIdx = cmdIdx;
        
        m_arithmetic8Instructions.put("a A,(HL)", new OpCode(0x86));
        m_arithmetic8Instructions.put("a A,(IX+d)", new OpCode(0xdd, 0x86, 0));
        m_arithmetic8Instructions.put("a A,(IY+d)", new OpCode(0xfd, 0x86, 0));
        
        m_arithmetic16Instructions.put("ADC HL,ss", new OpCode(0xed, 0x4a));
        m_arithmetic16Instructions.put("SBC HL,ss", new OpCode(0xed, 0x42));
        
    }
    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {

        // should always succeed, because we know it is one of arithmetic commands
        // int cmdIdx = m_arithmeticCmdIndices.get(asmLine.getCmd());
        String op1 = asmLine.getOperand1(); 
        String op2 = asmLine.getOperand2();
        OpCode opCode = null;
        
        int arithCmdIdx = m_cmdIdx;
        if (op2 == null  ||  RegisterGroups.REG_A.equals(op1)) {
            // it is 8 bit arithmetic
            String adderOperand;
            if (op2 == null) {  // reg A is implicit in 8 bit arithmetic
                adderOperand = op1;
            } else {
                adderOperand = op2;
            }
            int adderRegIdx = m_registerGroups.getRegister8Id(adderOperand);
            if (adderRegIdx != RegisterGroups.NO_SUCH_REG) {
                opCode = new OpCode(0x80); //newOpCode(m_opCodeContainer.getArithmetic8Instructions(), "a A,r");
                opCode.setRegister8(0, 3, arithCmdIdx); // set arithmetic opcode
                opCode.setRegister8(0, 0, adderRegIdx);
            } else {
                IndexedAddressingArgs idxArgs = new IndexedAddressingArgs(adderOperand, m_registerGroups.getRegisters16_SPXY());
                if (idxArgs.isIndirectAddressing()) {
                    opCode = newOpCode(m_arithmetic8Instructions, "a A," + idxArgs.getGenericOperand());
                    if (idxArgs.isIX_IY()) {
                        // a A,(IX+d)
                        // a A,(IY+d)
                        opCode.setRegister8(1, 3, arithCmdIdx); // set arithmetic opcode
                        opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_8_SIGNED, idxArgs.getExpression());
                    } else {
                        // a A,(HL)
                        opCode.setRegister8(0, 3, arithCmdIdx); // set arithmetic opcode
                    }
                } else {
                    // immediate addressing
                    opCode = new OpCode(0xc6, 0); // newOpCode(m_opCodeContainer.getArithmetic8Instructions(), "a A,n");
                    opCode.setRegister8(0, 3, arithCmdIdx); // set arithmetic opcode
                    opCode.addArgExpression(1, OpCode.ArgumentSize.BITS_8_UNSIGNED, adderOperand);
                }
            }
        } else {
            // it is 16 bit arithmetic
            if (op2 == null) {
                throw new AssemblerException("Missing the second operand for arithmetic instruction!", asmLine);
            }
            int reg16Id1 = m_registerGroups.getRegister16_SP_Id(op1);
            int reg16Id2 = m_registerGroups.getRegister16_SP_Id(op2);

            if (reg16Id1 == RegisterGroups.NO_SUCH_REG  ||  reg16Id2 == RegisterGroups.NO_SUCH_REG) {
                throw new AssemblerException("Operands should be register!", asmLine);
            }

            switch (arithCmdIdx) {
            case CMD_ADD_ID:

                switch (reg16Id1) {
                case RegisterGroups.REG_HL_ID:
                    // ADD HL,ss
                    if (reg16Id2 == RegisterGroups.REG_IX_ID  ||  reg16Id2 == RegisterGroups.REG_IY_ID) {
                        throw new AssemblerException("Can not add HL and IX or IY!", asmLine);
                    }
                    opCode = new OpCode(0x09);// newOpCode(m_opCodeContainer.getArithmetic16Instructions(), "ADD HL,ss");
                    opCode.setRegister16(0, 4, reg16Id2);
                    break;
                case RegisterGroups.REG_IX_ID:
                    // ADD IX,pp
                    if (reg16Id2 == RegisterGroups.REG_HL_ID  ||  reg16Id2 == RegisterGroups.REG_IY_ID) {
                        throw new AssemblerException("Can not add IX and HL or IY!", asmLine);
                    }
                    if (reg16Id2 == RegisterGroups.REG_IX_ID) {
                        reg16Id2 = RegisterGroups.REG_HL_ID;  // see Mostek manual
                    }
                    opCode = new OpCode(0xdd, 0x09);// newOpCode(m_opCodeContainer.getArithmetic16Instructions(), "ADD IX,pp");
                    opCode.setRegister16(1, 4, reg16Id2);
                    break;
                case RegisterGroups.REG_IY_ID:
                    // ADD IY,rr
                    if (reg16Id2 == RegisterGroups.REG_HL_ID  ||  reg16Id2 == RegisterGroups.REG_IX_ID) {
                        throw new AssemblerException("Can not add IY and HL or IX!", asmLine);
                    }
                    if (reg16Id2 == RegisterGroups.REG_IY_ID) {
                        reg16Id2 = RegisterGroups.REG_HL_ID;  // see Mostek manual
                    }
                    opCode = new OpCode(0xfd, 0x09);// newOpCode(m_opCodeContainer.getArithmetic16Instructions(), "ADD IY,rr");
                    opCode.setRegister16(1, 4, reg16Id2);
                    break;
                }
                break;
            case CMD_ADC_ID:
            case CMD_SBC_ID:
                // ADC HL,ss
                // SBC HL,ss
                if (reg16Id1 != RegisterGroups.REG_HL_ID) {
                    throw new AssemblerException("Invalid operand for ADC!", asmLine); 
                }
                if (reg16Id2 == RegisterGroups.REG_IX_ID  ||  reg16Id2 == RegisterGroups.REG_IY_ID) {
                    throw new AssemblerException("Invalid operand 2 for ADC - con not add HL and IX or IY!", asmLine); 
                }
                opCode = newOpCode(m_arithmetic16Instructions, asmLine.getCmd() + " HL,ss");
                opCode.setRegister16(1, 4, reg16Id2);
                break;
            default:
                throw new AssemblerException("Invalid argument for arithmetic command!", asmLine);
            }
            
        }
        
        if (opCode == null) {
            throw new AssemblerException("Invalid syntax!", asmLine);
        }
        
        return opCode;
    }
}
