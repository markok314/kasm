package si.mk.kasm.z80.translators;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.z80.IndexedAddressingArgs;
import si.mk.kasm.z80.RegisterGroups;

/** Translates SET, RES bit instructions. */
class SetResTranslator extends TranslatorBaseZ80 {

    public static final int IDX_SET = 3;  // see Mostek
    public static final int IDX_RES = 2;
    private int m_cmdIdx;


    public SetResTranslator(int cmdIdx) {
        m_cmdIdx = cmdIdx;
    }

    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        checkPresenceOfOperands(asmLine, 2);
        
        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();
        
        int regId = m_registerGroups.getRegister8Id(op2);
        OpCode opCode;
        
        if (regId != RegisterGroups.NO_SUCH_REG) {
            opCode = new OpCode(0xcb, 0x00); //newOpCode(m_opCodeContainer.getSetResInstructions(), "r");
            opCode.setRegister8(1, 0, regId);
            opCode.addArgExpression3(1, 3, op1); // bit index
            opCode.setRegister16(1, 6, m_cmdIdx); // instruction index
        } else {
            IndexedAddressingArgs args = new IndexedAddressingArgs(op2, m_registerGroups.getRegisters16_SPXY());
            if (args.isIndirectAddressing()) {
                if (args.isHLAddressing()) {
                    opCode = new OpCode(0xcb, 0x06); //newOpCode(m_opCodeContainer.getSetResInstructions(), "(HL)");
                    opCode.addArgExpression3(1, 3, op1); // bit index
                    opCode.setRegister16(1, 6, m_cmdIdx); // instruction index
                } else if (args.isIX_IY()) {
                    if (args.isIX()) {
                        opCode = new OpCode(0xdd, 0xcb, 0, 0x06); //newOpCode(m_opCodeContainer.getSetResInstructions(), args.getGenericOperand());
                    } else {
                        opCode = new OpCode(0xfd, 0xcb, 0, 0x06); 
                    }
                    opCode.addArgExpression3(3, 3, op1); // bit index
                    opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_8_UNSIGNED, args.getExpression()); // d offset
                    opCode.setRegister16(3, 6, m_cmdIdx); // instruction index
                } else {
                    throw new AssemblerException("Invalid register in addressing mode!", asmLine);
                }
            } else {
                throw new AssemblerException("Invalid addressing mode!", asmLine);
            }
        }
        return opCode;
    }    
}
