package si.mk.kasm.z80.translators;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.z80.IndexedAddressingArgs;
import si.mk.kasm.z80.RegisterGroups;

/** This class translates BIT testing instructions. */
class BitTranslator extends TranslatorBaseZ80 {

    public BitTranslator() {
    }

    
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        checkPresenceOfOperands(asmLine, 2);
        
        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();
        
        int regId = m_registerGroups.getRegister8Id(op2);
        OpCode opCode = null;
        
        if (regId != RegisterGroups.NO_SUCH_REG) {
            // BIT b,r
            opCode = new OpCode(0xcb, 0x40); //newOpCode(m_opCodeContainer.getBitInstructions(), "r");
            opCode.setRegister8(1, 0, regId);
            opCode.addArgExpression3(1, 3, op1); // bit index
        } else {
            IndexedAddressingArgs args = new IndexedAddressingArgs(op2, m_registerGroups.getRegisters16_SPXY());
            if (args.isIndirectAddressing()) {
                if (args.isHLAddressing()) {
                    // BIT b,(HL)
                    opCode = new OpCode(0xcb, 0x46); //newOpCode(m_opCodeContainer.getBitInstructions(), "(HL)");
                    opCode.addArgExpression3(1, 3, op1); // bit index
                } else if (args.isIX_IY()) {
                    // BIT b,(IX+d)
                    // BIT b,(IY+d)
                    if (args.isIX()) {
                        opCode = new OpCode(0xdd, 0xcb, 0, 0x46); //newOpCode(m_opCodeContainer.getBitInstructions(), args.getGenericOperand());
                    } else { 
                        // it is IY
                        opCode = new OpCode(0xfd, 0xcb, 0, 0x46); //newOpCode(m_opCodeContainer.getBitInstructions(), args.getGenericOperand());
                    } 
                    opCode.addArgExpression3(3, 3, op1); // bit index
                    opCode.addArgExpression(2, OpCode.ArgumentSize.BITS_8_UNSIGNED, args.getExpression()); // d offset
                } else {
                    throw new AssemblerException("Invalid register in addressing mode!", asmLine);
                }
            } else {
                throw new AssemblerException("Invalid addressing mode!", asmLine);
            }
        }
        return opCode;
    }
    
}
