/*
  KAsm - assembler for Z80
  Copyright (C) 2008  Marko Klopcic

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


  You can contact me at Google mail with the following address: markok3.14
 */

package si.mk.kasm.z80;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class parses indexed and indirect addressing operands, such as:<pre>
 * - (expr)
 * - (HL)
 * - (BC)
 * - (DE)
 * - (IX + expr)
 * - (IY + expr)
 * </pre>
 * 
 * @author markok
 */
public class IndexedAddressingArgs {

    private String m_register;
    private String m_expr; // can be expression of constants defined by EQU
    private String m_genericOperand; // (IX+d) or (IY+d)
    private int m_registerIndex = -1;

    // pattern without '(' and  ')'
    private static final Pattern m_indexedAddressingSimplePattern = 
        Pattern.compile("(I[XY])\\s*([\\+\\-])\\s*(.*)"); 


    /**
     * Checks if indexed or indirect addressing is used in the operand. If it 
     * is, then it is parsed and various getters may be called to get parameters 
     * of the addressing operand, such as register used and displacement expression.  
     * 
     * @param operand
     * @return
     */
    public IndexedAddressingArgs(String operand, Map<String, Integer> registers16) {
        if (operand.isEmpty()) {
            return;
        }
        
        if (operand.charAt(0) == '('  &&  operand.charAt(operand.length() - 1) == ')') {

            // remove '(' and ')'
            String reg = operand.substring(1, operand.length() - 1).trim();
            if (reg.length() == 2) {
                // (HL), (BC), (DE), (IX), (IY)
                Integer reg16Index = registers16.get(reg);
                if (reg16Index != null) {  
                    m_register = reg;
                    m_registerIndex = reg16Index;
                    
                    if (isIX_IY()) {  // syntax   SET 4, (IY)  is valid, so make it full expression
                        m_genericOperand = '(' + reg + "+d)";
                        m_expr = "0";
                        
                    } else {
                        m_genericOperand = '(' + reg + ')';
                    }
                } else {
                    m_expr = operand;
                    m_genericOperand = "(nn)";
                }
            } else {
                // (IY + d) or (IX + d)
                Matcher matcher = m_indexedAddressingSimplePattern.matcher(reg);
                if (matcher.matches()) {
                    m_register = matcher.group(1);
                    m_expr = matcher.group(2) + matcher.group(3);  // sign and expression
                    m_genericOperand = '(' + m_register + "+d)";

                    Integer reg16Index = registers16.get(m_register);
                    if (reg16Index != null) {  // if null, m_registerIndex is not set, and this is detected later
                        m_registerIndex = reg16Index;
                    }
                } else {
                    // (nn)
                    m_expr = operand;
                    m_genericOperand = "(nn)";
                }
            }
        }
    }
    
    /**
     * Returns true, if indirect or indexed addressing was used in operand given 
     * in ctor.
     */
    public boolean isIndirectAddressing() {
        return m_register != null  ||  m_expr != null;
    }
    
    /** @return true, if register is used in addressing operand (it is not (nn)). */
    public boolean isIndirectRegisterAddressing() {
        return m_register != null;
    }
    
    /** @return expression in case of (nn) or (IX + expr). */
    public String getExpression() {
        return m_expr;
    }

    /** @return register used in addressing, or null in case of (nn) addressing. */
    public String getRegister() {
        return m_register;
    }

    /** @return index or the register used in addressing, as used in op code. */
    public int getRegisterIndex() {
        return m_registerIndex;
    }

    /** @return generic form of indexed addressing, which is (IX+d) or (IY+d) */
    public String getGenericOperand() {
        return m_genericOperand;
    }

    /** @return true, if any of the 16 bit registers from function name is used. */
    public boolean isBCDEHLSPAddressing() {
        return m_registerIndex >= 0  &&  m_registerIndex < RegisterGroups.REG_IX_ID;
    }
    
    /** @return true if (HL) is used for addressing. */
    public boolean isHLAddressing() {
        return m_registerIndex == RegisterGroups.REG_HL_ID;
    }
    
    /** @return true if (BC) is used for addressing. */
    public boolean isBCAddressing() {
        return m_registerIndex == RegisterGroups.REG_BC_ID;
    }
    
    /** @return true if (DE) is used for addressing. */
    public boolean isDEAddressing() {
        return m_registerIndex == RegisterGroups.REG_DE_ID;
    }
    
    /** @return true, if IX or IY is used for addressing. */
    public boolean isIX_IY() {
        return m_registerIndex == RegisterGroups.REG_IX_ID  ||  m_registerIndex == RegisterGroups.REG_IY_ID;
    }
    
    /** @return true, if IX is used for addressing. */
    public boolean isIX() {
        return m_registerIndex == RegisterGroups.REG_IX_ID;
    }
    
    /** @return true, if IY is used for addressing. */
    public boolean isIY() {
        return m_registerIndex == RegisterGroups.REG_IY_ID;
    }
    
    
}
