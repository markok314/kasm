package si.mk.kasm.z80;

import java.util.HashMap;
import java.util.Map;

/**
 * This class contains groups of registers in maps, together with numbers of 
 * registers used in opcodes. This class is a singleton.
 * 
 * @author markok
 */
public class RegisterGroups {

    
    private static RegisterGroups m_instance;

    private Map<String, Integer> m_registers8 = new HashMap<String, Integer>();
    private Map<String, Integer> m_registers16_AF = new HashMap<String, Integer>();
    private Map<String, Integer> m_registers16_AFXY = new HashMap<String, Integer>();
    private Map<String, Integer> m_registers16_SP = new HashMap<String, Integer>();
    private Map<String, Integer> m_registers16_SPXY = new HashMap<String, Integer>();
    
    public static final String REG_A = "A";

    public final static int NO_SUCH_REG = -1; // not a register

    public static final int REG_A_ID = 7;

    public static final int REG_BC_ID = 0x00; // see page 51 in Mostek manual
    public static final int REG_DE_ID = 0x01; // see page 51 in Mostek manual
    public static final int REG_HL_ID = 0x02; // see page 51 in Mostek manual
    public static final int REG_SP_ID = 0x03;

    public static final int REG_IX_ID = 100; // these indices are not used in opcodes
    public static final int REG_IY_ID = 101;

    
    private RegisterGroups() {
    }
    
    public static RegisterGroups getInstance() {
        if (m_instance == null) {
            m_instance = new RegisterGroups();
            m_instance.initGroups();
        }
        return m_instance;
    }

    
    private void initGroups() {
        // for constants used for each register, see Mostek Z80 manual
        m_registers8.put("B", 0);        
        m_registers8.put("C", 1);        
        m_registers8.put("D", 2);        
        m_registers8.put("E", 3);        
        m_registers8.put("H", 4);        
        m_registers8.put("L", 5);        
        m_registers8.put("A", 7);        

        m_registers16_AF.put("BC", 0);        
        m_registers16_AF.put("DE", 1);        
        m_registers16_AF.put("HL", 2);        
        m_registers16_AF.put("AF", 3);
        
        m_registers16_AFXY.put("BC", 0);        
        m_registers16_AFXY.put("DE", 1);        
        m_registers16_AFXY.put("HL", 2);        
        m_registers16_AFXY.put("AF", 3);
        m_registers16_AFXY.put("IX", RegisterGroups.REG_IX_ID);        
        m_registers16_AFXY.put("IY", RegisterGroups.REG_IY_ID);        
        
        m_registers16_SP.put("BC", 0);        
        m_registers16_SP.put("DE", 1);        
        m_registers16_SP.put("HL", 2);        
        m_registers16_SP.put("SP", 3);        
        
        m_registers16_SPXY.put("BC", 0);        
        m_registers16_SPXY.put("DE", 1);        
        m_registers16_SPXY.put("HL", 2);        
        m_registers16_SPXY.put("SP", 3);        
        m_registers16_SPXY.put("IX", RegisterGroups.REG_IX_ID);        
        m_registers16_SPXY.put("IY", RegisterGroups.REG_IY_ID);
    }

    public Map<String, Integer> getRegisters8() {
        return m_registers8;
    }

    public Map<String, Integer> getRegisters16_AF() {
        return m_registers16_AF;
    }

    public Map<String, Integer> getRegisters16_SP() {
        return m_registers16_SP;
    }

    public Map<String, Integer> getRegisters16_AFXY() {
        return m_registers16_AFXY;
    }

    public Map<String, Integer> getRegisters16_SPXY() {
        return m_registers16_SPXY;
    }

    
    public int getRegister8Id(String operand) {

        Integer idx = m_registers8.get(operand);
        if (idx != null) {
            return idx;
        }

        return NO_SUCH_REG;
    }


    public int getRegister16_AF_Id(String operand) {

        Integer idx = m_registers16_AFXY.get(operand);
        if (idx != null) {
            return idx;
        }

        return NO_SUCH_REG;
    }


    public int getRegister16_SP_Id(String operand) {

        Integer idx = m_registers16_SPXY.get(operand);
        if (idx != null) {
            return idx;
        }

        return NO_SUCH_REG;
    }
    
    
    public boolean isIxOrIy(int regId) {
        return regId == RegisterGroups.REG_IX_ID  ||  regId == RegisterGroups.REG_IY_ID;
    }
    
    
}
