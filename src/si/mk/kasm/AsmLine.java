/*
  KAsm - assembler for Z80
  Copyright (C) 2008  Marko Klopcic

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


  You can contact me at Google mail with the following address: markok3.14
 */

package si.mk.kasm;

import java.util.List;

import si.mk.kasm.translators.OpCode;

/**
 * This data class contains all information about one source line, including machine
 * code after translation is done.
 * There is one instance of this class for each source line,
 * even if it is empty or contains only comment.
 * 
 * @author markok
 *
 */
public class AsmLine {
    private String m_srcLine; // unmodified source line
    private String m_label;   
    private String m_cmd;     // assembler mnemonic
    private int m_address;    // address, where this line will generate code
    private int m_sourceLineNumber; // line number in source file
    private String m_sourceFile;    // source file, where this line was read from
    private OpCode m_opCode;        // compiled binary data
    private String m_errorMsg;
    private String m_warnMsg;
    private List<String> m_operands = null; // string arguments
    private boolean isDirective = false; // true, if this object contains assembler directive
    private boolean m_isOperandsToUpperCase = true;
    
    // instructions, can be more for DB and DW directves
    
    
    public int getAddress() {
        return m_address;
    }
    
    public void setAddress(int address) {
        m_address = address;
    }
    
    public String getCmd() {
        return m_cmd;
    }
    
    public void setCmd(String cmd) {
        m_cmd = cmd;
    }
    
    public String getLabel() {
        return m_label;
    }
    
    public void setLabel(String label) {
        m_label = label;
    }
    
    public short[] getMachineCode() {
        return m_opCode.getMachineCode(); // m_machineCode;
    }
    
    public void setOpCode(OpCode opCode) {
        m_opCode = opCode;
    }

    public OpCode getOpCode() {
        return m_opCode;
    }
    
    
    public String getOperand1() {
        if (m_operands.size() > 0) {
            return m_operands.get(0);
        }
        
        return null;
    }
    
    public String getOperand2() {
        if (m_operands.size() > 1) {
            return m_operands.get(1);
        }
        
        return null;
    }
    
    public String getOperand(int idx) {
        if (m_operands.size() > idx) {
            return m_operands.get(idx);
        }
        
        return null;
    }
    
    public int getSourceLineNumber() {
        return m_sourceLineNumber;
    }
    
    public void setSourceLineNumber(int sourceLineNumber) {
        m_sourceLineNumber = sourceLineNumber;
    }
    
    public String getSrcLine() {
        return m_srcLine;
    }
    
    public void setSrcLine(String srcLine) {
        m_srcLine = srcLine;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("src (").append(m_sourceLineNumber).append("): ").append(m_srcLine);
        sb.append("\n   addr: ");
        sb.append(m_address).append("  ");
        
        if (m_opCode != null  &&  m_opCode.getMachineCode() != null) {
            for (short code : m_opCode.getMachineCode()) {
                sb.append(Integer.toHexString(code));
            }
        }
        
        sb.append("  lbl: ");
        sb.append(m_label).append("  cmd: ").append(m_cmd).append("  op1:")
        .append(getOperand1()).append(", op2: ").append(getOperand2());
        
        sb.append("\n");
        return sb.toString();
    }


    public int getMachineCodeLen() {
        // not all lines generate machine code
        return m_opCode == null ? 0 : m_opCode.size();
    }

    public boolean hasLabel() {
        return m_label != null;
    }

    public void setErrorMsg(String errorMsg) {
        if (m_errorMsg == null) {
            m_errorMsg = errorMsg;
        } else {
            m_errorMsg += errorMsg;
        }
    }

    public String getErrorMsg() {
        return m_errorMsg;
    }

    public String getWarnMsg() {
        return m_warnMsg;
    }

    public void setWarnMsg(String warnMsg) {
        if (m_warnMsg == null) {
            m_warnMsg = warnMsg;
        } else {
            m_errorMsg += warnMsg;
        }
    }
    
    /**
     * Returns cmd string with all letters capitalized (except in literal 
     * strings of course) and no redundant spaces.
     * 
     * @return
     */
    public String toNormalizedCmdString() {
        StringBuilder sb = new StringBuilder(m_cmd);
        
        int noOfOperands = m_operands.size();

        if (noOfOperands > 0) {

            sb.append(" ").append(m_operands.get(0));
            
            for (int i = 1; i < noOfOperands; i++) {
                sb.append(",").append(m_operands.get(1));
            }
        }

        
        return sb.toString();

    }

    public void setOperands(List<String> operands) {
        m_operands  = operands;
    }

    public List<String> getOperands() {
        return m_operands;
    }


    public void setOperand(int idx, String operand) {
        m_operands.set(idx, operand);
    }

    
    public void checkNoOfOperands(int expectedNumberOfOperands) throws AssemblerException {
        if (m_operands.size() != expectedNumberOfOperands) {
            throw new AssemblerException("Invalid number of operands! Expected: " +
                                         expectedNumberOfOperands + ",  has: " +
                                         m_operands.size(), this);                             
        }
    }

    public boolean isDirective() {
        return isDirective;
    }

    public void setDirective(boolean isDirective) {
        this.isDirective = isDirective;
    }

    /**
     * 
     * @param b if true, operands should be converted to upper case, except 
     * strings. This is true for most operands of assembler instructions and
     * directives, except file names, for example in '$include' directive.
     */
    public void setOperandsToUpperCase(boolean b) {
        m_isOperandsToUpperCase = b;
    }

    public boolean isOperandsToUpperCase() {
        return m_isOperandsToUpperCase;
    }

    public String getSourceFile() {
        return m_sourceFile;
    }

    public void setSourceFile(String sourceFile) {
        m_sourceFile = sourceFile;
    }
    
}

