package si.mk.kasm.translators;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;

public interface ITranslatorContainer {

    public boolean translateCmdToOpCode(AsmLine asmLine) throws AssemblerException;
}
