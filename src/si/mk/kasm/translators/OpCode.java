/*
  KAsm - assembler for Z80
  Copyright (C) 2008  Marko Klopcic

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


  You can contact me at Google mail with the following address: markok3.14
 */

package si.mk.kasm.translators;

import java.util.ArrayList;
import java.util.List;

import org.cheffo.jeplite.ParseException;

import si.mk.kasm.ExprParser;

/**
 * This class contains one machine instruction. It contains array of bytes, but also 
 * arguments, which are evaluated and applied to the existing machine code in the
 * second pass of compilation. 
 * 
 * @author markok
 */
public class OpCode {
    
    /** 
     * This class contains one argument of assembler instruction. There may be more than one argument
     * per assembler operation.
     * 
     * @author markok
     */
    public class Arg {
        int m_byteIdx;  // index of byte in opcode to be modified by this argument
        ArgumentSize m_argSize;  // argument size in bits, used only for 8 and 16 bit operands
        String m_expression; // expression to be evaluated in the second pass, when labels are defined
        
        int m_shiftCount;  // defines the shift count for the if result of expression 
        int m_bitPattern;  // bit pattern to be used for bit masking
        boolean m_isByteArg; // true, if argument defines one or two bytes, not just few bits
        
        // the following item was added to verify if PC is in the same 2k block as label for AJMP and ACALL on 8051 
        ArgumentAdapter m_adapter;  // if not null, it is called during the second pass to validate the value
        
        Arg(int byteIdx, ArgumentSize argSize, String expression) {
            m_isByteArg = true;
            m_byteIdx = byteIdx;
            m_argSize = argSize;
            m_expression = expression;
        }
        
        Arg(int byteIdx, int shiftCount, String expression, int bitPattern) {
            m_isByteArg = false;
            m_byteIdx = byteIdx;
            m_shiftCount = shiftCount;
            m_bitPattern = bitPattern;
            m_expression = expression;
        }
        
        /**
         * 
         * @param byteIdx
         * @param shiftCount
         * @param expression
         * @param bitPattern
         * @param validatingExpr expression, which should evaluate to true. Otherwise
         *                       it means assembler error
         * @param errorMsg message to be reported when validatingExpr == false
         */
        public Arg(int byteIdx, int shiftCount, String expression, int bitPattern, ArgumentAdapter adapter) {
            m_isByteArg = false;
            m_byteIdx = byteIdx;
            m_shiftCount = shiftCount;
            m_bitPattern = bitPattern;
            m_expression = expression;
            m_adapter = adapter;
        }

        public String getExpression() {
            return m_expression;
        }
        
        @Override
        public String toString() {
            return "Arg: byteIdx = " + m_byteIdx + ", expr: " + m_expression + 
                   ", isEvaluated: " + '\n';
        }
    }


    public enum ArgumentSize{BITS_8_UNSIGNED, BITS_8_SIGNED, BITS_16, BITS_16_BIG_ENDIAN};
    public static final int MAX_BYTES_PER_ASM_LINE = 256; // should be more than 
    // enough, because the source must be readable
    
    protected short[] m_machineCode;
    protected List<Arg> m_args = new ArrayList<Arg>();


    /**
     * This ctor is used for arguments of compiler directives DB and DW.
     * @param args list of arguments as strings
     * @param bitSize 8 or 16 bit args
     */
    public OpCode(List<String> args, ArgumentSize bitSize) {

        short[]machineCode = new short[MAX_BYTES_PER_ASM_LINE];
        int mcIdx = 0;  // index to machine code array
        
        for (String arg : args) {
            if (mcIdx >= MAX_BYTES_PER_ASM_LINE) {
                throw new IllegalArgumentException("To many space required by arguments! Max allowed bytes: " + MAX_BYTES_PER_ASM_LINE);
            }
            if (arg.charAt(0) == '"'  ||  arg.charAt(0) == '\'') {
                // it is string argument
                int argLen = arg.length() - 1;
                for (int i = 1; i < argLen; i++) {
                    machineCode[mcIdx++] = (short)arg.charAt(i);
                }
            } else {
                m_args.add(new Arg(mcIdx++, bitSize, arg));
                if (bitSize == ArgumentSize.BITS_16  ||  bitSize == ArgumentSize.BITS_16_BIG_ENDIAN) {
                    mcIdx++;
                }
            }
        }
        
        m_machineCode = new short[mcIdx];
        System.arraycopy(machineCode, 0, m_machineCode, 0, mcIdx);
    }
    
    /** Creates op code 1 byte long and initializes it. */
    public OpCode(int byte0) {
        m_machineCode = new short[1];
        m_machineCode[0] = (short)byte0;
    }
                              
    
    /** Creates op code 2 bytes long and initializes it. */
    public OpCode(int byte0, int byte1) {
        m_machineCode = new short[2];
        m_machineCode[0] = (short)byte0;
        m_machineCode[1] = (short)byte1;
    }
                              
    
    /** Creates op code 3 bytes long and initializes it. */
    public OpCode(int byte0, int byte1, int byte2) {
        m_machineCode = new short[3];
        m_machineCode[0] = (short)byte0;
        m_machineCode[1] = (short)byte1;
        m_machineCode[2] = (short)byte2;
    }
                              
    
    /** Creates op code 4 bytes long and initializes it. */
    public OpCode(int byte0, int byte1, int byte2, int byte3) {
        m_machineCode = new short[4];
        m_machineCode[0] = (short)byte0;
        m_machineCode[1] = (short)byte1;
        m_machineCode[2] = (short)byte2;
        m_machineCode[3] = (short)byte3;
    }
                              
    
    /** Creates op code from the given array. */
    public OpCode(short[] machineCode) {
        m_machineCode = machineCode;
    }

    
    /**
     * This method sets argument value in machine code at the given index.
     * @param arg
     */
    public String setArgumentValue(int index, ArgumentSize argSize, int arg) {
        switch (argSize) {
        case BITS_8_SIGNED:
            m_machineCode[index] = (short)arg;
            if (arg < -128  ||  arg > 127) {
                throw new IllegalArgumentException("8-bit Signed argument out of range: " + arg);
            }
            break;
        case BITS_8_UNSIGNED:
            m_machineCode[index] = (short)arg;
            if (arg < -128  ||  arg > 255) {
                return "8-bit Unsigned argument out of range: " + arg;
            }
            break;
        case BITS_16:
            m_machineCode[index] = (short)(arg & 0xff);
            m_machineCode[index + 1] = (short)((arg >> 8) & 0xff);
            if (arg < -32768  ||  arg > 65535) {
                return "16-bit argument out of range: " + arg;
            }
            break;
        case BITS_16_BIG_ENDIAN: // used by 8051 LCALL, LJMP, and mov DPTR, #data
            m_machineCode[index + 1] = (short)(arg & 0xff);
            m_machineCode[index] = (short)((arg >> 8) & 0xff);
            if (arg < -32768  ||  arg > 65535) {
                return "16-bit argument out of range: " + arg;
            }
            break;
        }
        return null;
    }

    
    /** This method sets bits identifying 8-bit register in op code. */ 
    public void setRegister8(int index, int shiftCount, int register) {
        m_machineCode[index] &= ~(0x07 << shiftCount); // first clear the bits
        m_machineCode[index] |= (register << shiftCount);
    }
    
    
    /**
     * This method sets bits identifying 16-bit register in op code. It does not check, if the 
     * code is valid (not all 16-bit registers can be used in any instruction).
     *  
     * @param index
     * @param shiftCount
     * @param register
     */
    public void setRegister16(int index, int shiftCount, int register) {
        m_machineCode[index] &= ~(0x03 << shiftCount); // first clear the bits
        m_machineCode[index] |= (register << shiftCount);
    }


    /** Returns array of bytes representing Z80 machine code. */
    public short[] getMachineCode() {
        return m_machineCode;
    }
    

    /** Adds argument to the list of arguments to be evaluated in the second pass. */
    public void addArgExpression(int byteIdx, ArgumentSize argSize, String expression) {
        if (expression == null) {
            throw new NullPointerException("Expression must not be null!");
        }
        m_args.add(new Arg(byteIdx, argSize, expression));
    }
    
    
    public void addArgExpression(Arg argument) {
        if (argument == null) {
            throw new NullPointerException("Argument must not be null!");
        }
        m_args.add(argument);
    }
    
    
    public Arg getArg(int idx) {
        return m_args.get(idx);
    }
    
    
    /** Evaluates args. Called during the second pass. */
    public String evaluateArgs(ExprParser parser) throws ParseException {
        StringBuilder sb = new StringBuilder();
        for (Arg arg : m_args) {

            if (arg.m_adapter != null) {
                String wrnMsg = arg.m_adapter.adapt(this, parser);
                if (wrnMsg != null) {
                    sb.append(wrnMsg);
                    sb.append('\n');
                }
            }
            
            int result = parser.evaluate(arg.m_expression);
            if (arg.m_isByteArg) {
                String wrnMsg = setArgumentValue(arg.m_byteIdx, arg.m_argSize, result);
                if (wrnMsg != null) {
                    sb.append(wrnMsg);
                    sb.append('\n');
                }
            } else {
                if (arg.m_shiftCount >= 0) {
                    m_machineCode[arg.m_byteIdx] &= ~(arg.m_bitPattern << arg.m_shiftCount); // first clear the bits
                    m_machineCode[arg.m_byteIdx] |= (result & arg.m_bitPattern) << arg.m_shiftCount;
                } else {  // negative argument means right shift
                    m_machineCode[arg.m_byteIdx] &= ~(arg.m_bitPattern >> -arg.m_shiftCount); // first clear the bits
                    m_machineCode[arg.m_byteIdx] |= (result & arg.m_bitPattern) >> -arg.m_shiftCount;
                }
            }
        }
        
        if (sb.length() > 0) {
            return sb.toString();
        }
        
        return null;
    }


    public int size() {
        return m_machineCode.length;
    }


    /**
     * This method sets expression, which evaluates to 3 bits only. These 3 bits 
     * are then shifted and ORed to the byte at index byteIdx.
     *  
     */
    public void addArgExpression3(int byteIdx, int shiftCount, String expression) {
        m_args.add(new Arg(byteIdx, shiftCount, expression, 7));
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("mc: ");
        for (int i = 0; i < m_machineCode.length; i++) {
            sb.append(m_machineCode[i]);
            sb.append(", ");
        }
        sb.append('\n');
        for (Arg arg: m_args) {
            sb.append(arg.toString());
        }
        return sb.toString();
    }
}
