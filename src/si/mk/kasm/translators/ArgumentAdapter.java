package si.mk.kasm.translators;

import org.cheffo.jeplite.ParseException;

import si.mk.kasm.ExprParser;

public interface ArgumentAdapter {

    /**
     * Validates op. code. Throws exception in case of error, returns string
     * with warning message in case of warning.
     * 
     * @param opCode op code to be verified
     * @param exprParser parser with labels and constants defined
     * @return null if everything is OK
     */
    String adapt(OpCode opCode, ExprParser exprParser) throws ParseException;
}
