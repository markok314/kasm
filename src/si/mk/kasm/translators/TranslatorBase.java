package si.mk.kasm.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.z80.RegisterGroups;
import si.mk.kasm.z80.translators.TranslatorContainerZ80;


/**
 *  This is base class for translators. Translators were introduced, because
 *  each group of commands has specific requirements. The other possible approach
 *  is storing of all commands to maps and then performing simple lookup. This
 *  approach requires much more typing (for example all LD r,s instructions), and is
 *  also slower than translators.
 */
abstract public class TranslatorBase {

//    protected TranslatorContainer m_opCodeContainer;
    protected RegisterGroups m_registerGroups;

    public TranslatorBase() {
//        m_opCodeContainer = opCodeContainer;
        m_registerGroups = RegisterGroups.getInstance();
    }


    /**
     * The default method translates assembler instructions, which do not have
     * variable parameters, for example registers or immediate values. This method
     * should be (and it is) overridden by all derived classes.
     *  
     * @param asmLine
     * @return
     * @throws AssemblerException
     */
    abstract public OpCode createOpCode(AsmLine asmLine) throws AssemblerException;
    
    
    /**
     * Returns a copy of opcode referenced by the given key in the given map. 
     * @param map
     * @param key
     * @return
     */
    protected OpCode newOpCode(Map<String, OpCode> map, String key) {
        OpCode opCode = map.get(key);
   
        if (opCode == null) {
            return null;
        }
        
        return new OpCode(opCode.getMachineCode().clone());
    }
    
    /**
     * This is OpCode copy method. It copies bytes, but not arguments.
     * @param opCode
     * @return
     */
    protected OpCode newOpCode(OpCode opCode) {
        if (opCode == null) {
            return null;
        }
        
        return new OpCode(opCode.getMachineCode().clone());
    }
    
    /** Returns true, if the operand specifies indexed or indirect addressing. */ 
    abstract protected boolean isIndirectAddressing(String operand); 

    /**
     * Checks if the number of operands in asmLine matches the required number 
     * of operands. Throws exception if it does not.
     * 
     * @param asmLine
     * @param noOfOperands the number of operands, which must be present
     * @throws AssemblerException
     */
    public static void checkPresenceOfOperands(AsmLine asmLine, int noOfOperands) throws AssemblerException {

        switch (noOfOperands) {
        case 0:
            if (asmLine.getOperand1() != null  ||  asmLine.getOperand2() != null) {
                throw new AssemblerException("No operands allowed!", 
                                             asmLine);
            }
            break;
        /* case 1:
            if (asmLine.getOperand1() == null) {
                throw new AssemblerException("One operand requested!", 
                                             asmLine);
            }
            if (asmLine.getOperand2() != null) {
                throw new AssemblerException("Only one operand allowed!", 
                                             asmLine);
            }
            break;
        case 2:
            if (asmLine.getOperand1() == null  ||  asmLine.getOperand2() == null) { 
                throw new AssemblerException("Two operands requested!", 
                                             asmLine);
            }
            break; */
        default:
            if (asmLine.getOperand(noOfOperands - 1) == null  ||  asmLine.getOperand(noOfOperands) != null) {
                throw new AssemblerException(noOfOperands + " operands requested!", 
                                             asmLine);
            }
        }
    }
    
}
