package si.mk.kasm.i8051.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.ExprParser;
import si.mk.kasm.translators.ITranslatorContainer;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.TranslatorBase;

public class TranslatorContainer8051 implements ITranslatorContainer {

    private Map<String, TranslatorBase> m_translators = new HashMap<String, TranslatorBase>();
    
    private SimpleInstructionTranslator m_simpleTranslator = new SimpleInstructionTranslator();
    
    public TranslatorContainer8051(ExprParser exprParser) {
        initInstrTranslationMap(exprParser);
    }

    // @Override
    public boolean translateCmdToOpCode(AsmLine asmLine) throws AssemblerException {
        
        TranslatorBase translator = m_translators.get(asmLine.getCmd());
        
        if (translator != null) {
            OpCode opCode = translator.createOpCode(asmLine);
            asmLine.setOpCode(opCode);
            return true;
        }

        // check also for simple instructions, which have no parameters or they are always the same
        OpCode opCode = m_simpleTranslator.createOpCode(asmLine);
        if (opCode != null) {
            asmLine.setOpCode(opCode);
            return true;
        } 
        
        return false;
    }

    private void initInstrTranslationMap(ExprParser exprParser) {

        m_translators.put("AJMP", new AbsJmpCallTranslator(AbsJmpCallTranslator.OP_CODE_AJMP));
        m_translators.put("ACALL", new AbsJmpCallTranslator(AbsJmpCallTranslator.OP_CODE_ACALL));

        m_translators.put("ADD", new ArithmeticTranslator(ArithmeticTranslator.HIGH_BITS_ADD));
        m_translators.put("ADDC", new ArithmeticTranslator(ArithmeticTranslator.HIGH_BITS_ADDC));
        m_translators.put("SUBB", new ArithmeticTranslator(ArithmeticTranslator.HIGH_BITS_SUBB));
        
        m_translators.put("ANL", new LogicTranslator(LogicTranslator.HIGH_BITS_ANL));
        m_translators.put("ORL", new LogicTranslator(LogicTranslator.HIGH_BITS_ORL));
        m_translators.put("XRL", new LogicTranslator(LogicTranslator.HIGH_BITS_XRL));
        
        m_translators.put("SETB", new BitTranslator());
        m_translators.put("CLR", new BitTranslator());
        m_translators.put("CPL", new BitTranslator());
        
        m_translators.put("CJNE", new CjneTranslator());
        
        m_translators.put("DJNZ", new DjnzTranslator());
        
        m_translators.put("INC", new IncDecTranslator(IncDecTranslator.HIGH_BITS_INC));
        m_translators.put("DEC", new IncDecTranslator(IncDecTranslator.HIGH_BITS_DEC));

        m_translators.put("JBC", new JrBitTranslator(JrBitTranslator.OP_CODE_JBC));
        m_translators.put("JB", new JrBitTranslator(JrBitTranslator.OP_CODE_JB));
        m_translators.put("JNB", new JrBitTranslator(JrBitTranslator.OP_CODE_JNB));
        
        m_translators.put("POP", new JrPushPopTranslator(JrPushPopTranslator.OP_CODE_POP, false));
        m_translators.put("PUSH", new JrPushPopTranslator(JrPushPopTranslator.OP_CODE_PUSH, false));
        m_translators.put("SJMP", new JrPushPopTranslator(JrPushPopTranslator.OP_CODE_SJMP, true));
        m_translators.put("JC", new JrPushPopTranslator(JrPushPopTranslator.OP_CODE_JC, true));
        m_translators.put("JNC", new JrPushPopTranslator(JrPushPopTranslator.OP_CODE_JNC, true));
        m_translators.put("JZ", new JrPushPopTranslator(JrPushPopTranslator.OP_CODE_JZ, true));
        m_translators.put("JNZ", new JrPushPopTranslator(JrPushPopTranslator.OP_CODE_JNZ, true));
        
        m_translators.put("LJMP", new LongJmpCallTranslator(LongJmpCallTranslator.OP_CODE_LJMP));
        m_translators.put("LCALL", new LongJmpCallTranslator(LongJmpCallTranslator.OP_CODE_LCALL));
        
        m_translators.put("MOV", new MovTranslator());
        
        m_translators.put("JMP", new JmpCallTranslator(exprParser, AbsJmpCallTranslator.OP_CODE_AJMP));
        m_translators.put("CALL", new JmpCallTranslator(exprParser, AbsJmpCallTranslator.OP_CODE_ACALL));
        
        m_translators.put("XCH", new XchTranslator());
        
        
    }
}
