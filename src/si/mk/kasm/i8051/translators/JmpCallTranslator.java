package si.mk.kasm.i8051.translators;

import org.cheffo.jeplite.ParseException;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.ExprParser;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates JMP instructions. One instruction is JMP @A+DPTR, other is
 * JMP addr, which synonym for SJMP, AJMP, and LJMP. Which of jumps is used depends 
 * on address range. If addr is forward referenced, LJMP is used. If address is 
 * already defined, first SJMP is tried, then AJMP and finally LJMP. It is also 
 * important, that address is given as label, not as direct value or an expression.
 * In the later case LJMP is used.
 * 
 * @author markok
 *
 */
public class JmpCallTranslator extends TranslatorBase8051 {

    private ExprParser m_exprParser;
    private int m_opCode;


    public JmpCallTranslator(ExprParser exprParser, int opCode) {
        
        m_exprParser = exprParser;
        m_opCode = opCode;
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        String op1 = asmLine.getOperand1();
        
        if (op1.equals("@A+DPTR")) {
            return new OpCode(0x73);
        }
        
        try {
            if (m_exprParser.getSymbolTable().containsKey(op1)) {
                int addrVal = m_exprParser.evaluate(op1); 
                int pcAddr = asmLine.getAddress() + 2;  // PC is incremented to the end of instruction before jump

                int offset = addrVal - pcAddr; 
                if (offset > -129  &&  offset < 128  &&  asmLine.getCmd().equals("JMP")) {  // call has no SJMP equivalent
                    OpCode opCode = new OpCode(0x80, 0);  // SJMP
                    opCode.setArgumentValue(1, ArgumentSize.BITS_8_SIGNED, offset);
                    return opCode;
                } else if ((addrVal & 0xf800) == (pcAddr & 0xf800)) {  // AJMP and ACALL

                    OpCode opCode = new OpCode(m_opCode, 0);
                    // see 8051 manual for description or AJMP and ACALL instructions
                    opCode.setArgumentValue(0, 
                                            ArgumentSize.BITS_8_UNSIGNED, 
                                            ((addrVal & 0x0700) >> 3) | opCode.getMachineCode()[0]);

                    opCode.setArgumentValue(1, ArgumentSize.BITS_8_UNSIGNED, addrVal & 0xff);
                    return opCode;
                } 
            } 

            // LJMP and LCALL
         // opcode for AJMP = 0x01, ACALL 0x11, LJMP 0x02, LCALL 0x12, so invert bits 0 and 1
            OpCode opCode = new OpCode(m_opCode ^ 0x03, 0, 0); 
            opCode.addArgExpression(1, ArgumentSize.BITS_16_BIG_ENDIAN, op1);
            return opCode;

        } catch (ParseException ex) {
            // should never happen if parser contains the key
            throw new AssemblerException("Can not evaluate: " + op1, asmLine);
        }
    }

}
