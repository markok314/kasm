package si.mk.kasm.i8051.translators;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates long jumps and calls - LJMP and LCALL.
 * 
 * @author markok
 *
 */
public class LongJmpCallTranslator extends TranslatorBase8051 {

    private int m_opCode;

    public static final int OP_CODE_LJMP = 0x02;
    public static final int OP_CODE_LCALL = 0x12;
    

    /**
     * 
     */
    public LongJmpCallTranslator(int opCode) {
        
        m_opCode = opCode;
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 1);

        String jmpAddr = asmLine.getOperand1();
        
        OpCode opCode = null;
        
        opCode = new OpCode(m_opCode, 0, 0);
        opCode.addArgExpression(1, ArgumentSize.BITS_16_BIG_ENDIAN, jmpAddr);
        
        return opCode;
    }
}

