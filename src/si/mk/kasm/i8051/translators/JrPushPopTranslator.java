package si.mk.kasm.i8051.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates relative jumps with single parameter and PUSH and POP instructions.
 * 
 * @author markok
 *
 */
public class JrPushPopTranslator extends TranslatorBase8051 {

    public static final int OP_CODE_POP = 0xd0;
    public static final int OP_CODE_PUSH = 0xc0;
    public static final int OP_CODE_SJMP = 0x80;

    public static final int OP_CODE_JC = 0x40;
    public static final int OP_CODE_JNC = 0x50;
    public static final int OP_CODE_JZ = 0x60;
    public static final int OP_CODE_JNZ = 0x70;

    private int m_opCode;
    private boolean m_isSigned;
    
    /**
     * 
     * @param highBits high bits of instructions, for example HIGH_BITS_INC or HIGH_BITS_DEC.
     */
    public JrPushPopTranslator(int opCode, boolean isSigned) {
        
        m_opCode = opCode;
        m_isSigned = isSigned;
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 1);

        String op1 = asmLine.getOperand1();
        
        OpCode opCode = new OpCode(m_opCode, 0);
        if (m_isSigned) {
            opCode.addArgExpression(1, ArgumentSize.BITS_8_SIGNED, 
                                    op1 + " - 2 - " + asmLine.getAddress());
        } else {
            opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, 
                                    op1);
        }
        
        return opCode;
    }
}

