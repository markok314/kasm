package si.mk.kasm.i8051.translators;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates relative jumps with bit address.
 * 
 * @author markok
 *
 */
public class JrBitTranslator extends TranslatorBase8051 {

    private int m_opCode;

    public static final int OP_CODE_JBC = 0x10;
    public static final int OP_CODE_JB = 0x20;
    public static final int OP_CODE_JNB = 0x30;

    
    /**
     * 
     * @param opCode op code of jump, one of OP_CODE_JBC, ....
     */
    public JrBitTranslator(int opCode) {
        
        m_opCode = opCode;
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 2);

        String bitAddr = replaceDotInBitAddresses(asmLine.getOperand1());
        String jmpRelAddr = asmLine.getOperand2() + " - 3 - " + asmLine.getAddress();
        
        OpCode opCode = null;
        
        opCode = new OpCode(m_opCode, 0, 0);
        opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, bitAddr);
        opCode.addArgExpression(2, ArgumentSize.BITS_8_SIGNED, 
                                jmpRelAddr);
        
        return opCode;
    }
}

