package si.mk.kasm.i8051.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates arithmetic and logical instructions.
 * 
 * @author markok
 *
 */
public class LogicTranslator extends TranslatorBase8051 {

    private Map<String, OpCode> m_instructions = new HashMap<String, OpCode>();
    private int m_highBits;
    
    public static final int HIGH_BITS_ORL = 0x40;
    public static final int HIGH_BITS_ANL = 0x50;
    public static final int HIGH_BITS_XRL = 0x60;
    /**
     * 
     * @param highBits high bits of instructions, one of HIGH_BITS_ constants.
     */
    public LogicTranslator(int highBits) {
        
        m_highBits = highBits;
        
        m_instructions.put("@R0", new OpCode(0x06 | highBits));
        m_instructions.put("@R1", new OpCode(0x07 | highBits));
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 2);

        String op1 = asmLine.getOperand1();
        
        if (op1.equals("C")) {
            return processLogicalOperationsOnCarryFlag(asmLine);
        }
        

        if (!op1.equals("A")) {
            return processLogicalOperationsOnDirectAddress(asmLine);
            // throw new AssemblerException("The first operand should be register 'A', but it is: " + op1, asmLine); 
        }

        OpCode opCode = null;
        String op2 = asmLine.getOperand2();

        int regIdx = getIndexOfRn(op2); 
        
        if (regIdx != NOT_RN) {
            // it is one of R0..R7
            opCode = new OpCode(0x08 | m_highBits);
            opCode.setRegister8(0, 0, regIdx);
        } else {
            opCode = m_instructions.get(op2);
            if (opCode == null) {
                // it is not one of instructions defined in ctor, but it 
                // is "CMD A, DATA_ADDR" or "CMD A, #DATA"
                
                if (op2.charAt(0) == '#') {
                    opCode = new OpCode(0x04 | m_highBits, 0);
                    opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op2.substring(1));
                } else {
                    opCode = new OpCode(0x05 | m_highBits, 0);
                    opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op2);
                }
            }
        }
        
        return opCode;
    }


    private OpCode processLogicalOperationsOnDirectAddress(AsmLine asmLine) throws AssemblerException {

        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();
        
        OpCode opCode = null;
        
        if (op2.equals("A")) {
            // orl direct, A
            opCode = new OpCode(m_highBits | 0x02, 0);
            opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op1);
        } else if (op2.charAt(0) == '#') {
            // orl direct, #data
            opCode = new OpCode(m_highBits | 0x03, 0, 0);
            opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op1);
            opCode.addArgExpression(2, ArgumentSize.BITS_8_UNSIGNED, op2.substring(1));
        } else {
            throw new AssemblerException("Invalid operands!", asmLine);
        }
        
        return opCode;
    }


    private OpCode processLogicalOperationsOnCarryFlag(AsmLine asmLine) throws AssemblerException {

        OpCode opCode = null;
        String op2 = asmLine.getOperand2();
        
        if (m_highBits == HIGH_BITS_ANL) {
        
            if (op2.charAt(0) == '/') {
                opCode = new OpCode(0xb0, 0);
                op2 = op2.substring(1);
            } else {
                opCode = new OpCode(0x82, 0);
            }

            
        } else if (m_highBits == HIGH_BITS_ORL) {
        
            if (op2.charAt(0) == '/') {
                opCode = new OpCode(0xa0, 0);
                op2 = op2.substring(1);
            } else {
                opCode = new OpCode(0x72, 0);
            }
            
        } else {
            throw new AssemblerException("Operand 'C' may only be used with ORL or ANL instructions!", asmLine); 
        }
        
        String bitAddress = replaceDotInBitAddresses(op2);

        opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, bitAddress);
        
        return opCode;
    }

}
