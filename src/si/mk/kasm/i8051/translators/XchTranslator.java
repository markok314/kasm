package si.mk.kasm.i8051.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.ExprParser;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates XCH instructions.
 * 
 * @author markok
 *
 */
public class XchTranslator extends TranslatorBase8051 {

    private Map<String, OpCode> m_instructions = new HashMap<String, OpCode>();
    
    /**
     * 
     */
    public XchTranslator() {
        
        m_instructions.put("@R0", new OpCode(0xc6));
        m_instructions.put("@R1", new OpCode(0xc7));
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 2);

        String op1 = asmLine.getOperand1();
        
        if (!op1.equals("A")) {
            throw new AssemblerException("The first argument should be 'A', but it is: " + op1, asmLine);
        }
        
        OpCode opCode = null;
        
        String op2 = asmLine.getOperand2();
        int regIdx = getIndexOfRn(op2); 
        
        if (regIdx != NOT_RN) {
            // it is one of R0..R7
            opCode = new OpCode(0xc8 | regIdx);
        } else {
            opCode = m_instructions.get(op2);
            if (opCode == null) {
                // it is not one of instructions defined in ctor, but it is "CMD DATA_ADDR"
                opCode = new OpCode(0xc5, 0);
                opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op2);
            }
        }
        
        return opCode;
    }
}
