package si.mk.kasm.i8051.translators;

import org.cheffo.jeplite.ParseException;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.ExprParser;
import si.mk.kasm.translators.ArgumentAdapter;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates absolute jumps and calls - AJMP and ACALL.
 * 
 * @author markok
 */
public class AbsJmpCallTranslator extends TranslatorBase8051 {

    private int m_opCode;

    public final static int OP_CODE_AJMP = 0x01;
    public final static int OP_CODE_ACALL = 0x11;
    
    public AbsJmpCallTranslator(int opCode) {
        
        m_opCode = opCode;
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 1);

        String jmpAddr = asmLine.getOperand1();
        
        OpCode opCode = new OpCode(m_opCode, 0);
        
        // PC is incremented twice
        ArgumentAdapter adapter = new AJmpACallAdapter(asmLine.getAddress() + 2);
        opCode.addArgExpression(opCode.new Arg(0, -3, jmpAddr, 0x0700, adapter));
        opCode.addArgExpression(opCode.new Arg(1, 0, jmpAddr, 0xff, null));
        
        return opCode;
    }

    
    class AJmpACallAdapter implements ArgumentAdapter {

        private int m_pcAddress;

        AJmpACallAdapter(int address) {
            m_pcAddress = address;
        }
        
        
        // @Override
        public String adapt(OpCode opCode, ExprParser exprParser) throws ParseException {

            OpCode.Arg jmpAddr = opCode.getArg(0);
            int addr = exprParser.evaluate(jmpAddr.getExpression());
            
            if ((addr & 0xf800) != (m_pcAddress & 0xf800)) {
                throw new ParseException("Label not in the same 2k block as instruction! label = " + 
                                         addr + ",  instr. addr. = " + m_pcAddress);
            }

            // see 8051 manual for description or AJMP and ACALL instructions
            /*  set in OpCode.evaluateArgs();
             *  opCode.setArgumentValue(0, 
                                    ArgumentSize.BITS_8_UNSIGNED, 
                                    ((addr & 0x0700) >> 3) | opCode.getMachineCode()[0]);
            
            opCode.setArgumentValue(1, ArgumentSize.BITS_8_UNSIGNED, addr & 0xff); */
            
            return null;
        }
        
    }
}
