package si.mk.kasm.i8051.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates arithmetic and logical instructions.
 * 
 * @author markok
 *
 */
public class ArithmeticTranslator extends TranslatorBase8051 {

    private Map<String, OpCode> m_instructions = new HashMap<String, OpCode>();
    private int m_highBits;
    
    public static final int HIGH_BITS_ADD = 0x20;
    public static final int HIGH_BITS_ADDC = 0x30;
    public static final int HIGH_BITS_SUBB = 0x90;
    
    /**
     * 
     * @param highBits high bits of instructions, one of HIGH_BITS_ constants.
     */
    public ArithmeticTranslator(int highBits) {
        
        m_highBits = highBits;
        
        m_instructions.put("@R0", new OpCode(0x06 | highBits));
        m_instructions.put("@R1", new OpCode(0x07 | highBits));
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 2);

        String op1 = asmLine.getOperand1();
        
        if (!op1.equals("A")) {
            throw new AssemblerException("The first operand should be register 'A', but it is: " + op1, asmLine); 
        }

        OpCode opCode = null;
        String op2 = asmLine.getOperand2();

        int regIdx = getIndexOfRn(op2); 
        
        if (regIdx != NOT_RN) {
            // it is one of R0..R7
            opCode = new OpCode(0x08 | m_highBits);
            opCode.setRegister8(0, 0, regIdx);
        } else {
            opCode = m_instructions.get(op2);
            if (opCode == null) {
                // it is not one of instructions defined in ctor, but it 
                // is "CMD A, DATA_ADDR" or "CMD A, #DATA"
                
                if (op2.charAt(0) == '#') {
                    opCode = new OpCode(0x04 | m_highBits, 0);
                    opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op2.substring(1));
                } else {
                    opCode = new OpCode(0x05 | m_highBits, 0);
                    opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op2);
                }
            }
        }
        
        return opCode;
    }


    private OpCode processLogicalOperationsOnCarryFlag(AsmLine asmLine) throws AssemblerException {

        OpCode opCode = null;
        String cmd = asmLine.getCmd();
        String op2 = asmLine.getOperand2();
        
        if (cmd.equals("ANL")) {
        
            if (op2.charAt(0) == '/') {
                opCode = new OpCode(0xb0, 0);
                op2 = op2.substring(1);
            } else {
                opCode = new OpCode(0x82, 0);
            }

            
        } else if (cmd.equals("ORL")) {
        
            if (op2.charAt(0) == '/') {
                opCode = new OpCode(0xa0, 0);
                op2 = op2.substring(1);
            } else {
                opCode = new OpCode(0x72, 0);
            }
            
        } else {
            throw new AssemblerException("Operand 'C' may only be used with ORL or ANL instructions!", asmLine); 
        }
        
        opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op2);
        
        return opCode;
    }

}
