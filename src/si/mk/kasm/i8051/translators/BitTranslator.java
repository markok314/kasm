package si.mk.kasm.i8051.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates Bit instructions SETB, CLR, and CPL.
 * 
 * @author markok
 *
 */
public class BitTranslator extends TranslatorBase8051 {

    private Map<String, OpCode> m_instructions = new HashMap<String, OpCode>();
    
    /**
     * 
     * @param highBits high bits of instructions, for example HIGH_BITS_INC or HIGH_BITS_DEC.
     */
    public BitTranslator() {
        
        m_instructions.put("SETB bitaddr", new OpCode(0xd2, 0));
        m_instructions.put("SETB C", new OpCode(0xd3));
        
        m_instructions.put("CLR A", new OpCode(0xe4));
        m_instructions.put("CLR bitaddr", new OpCode(0xc2, 0));
        m_instructions.put("CLR C", new OpCode(0xc3));
        
        m_instructions.put("CPL A", new OpCode(0xf4));
        m_instructions.put("CPL bitaddr", new OpCode(0xb2, 0));
        m_instructions.put("CPL C", new OpCode(0xb3));
        
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 1);

        OpCode opCode = null;
        
        String asmStr = asmLine.toNormalizedCmdString();

        opCode = m_instructions.get(asmStr);
        if (opCode == null) {
            // it is not one of instructions with bitAddr parameter
            opCode = newOpCode(m_instructions.get(asmLine.getCmd() + " bitaddr"));
            if (opCode != null) {
                String bitAddress = replaceDotInBitAddresses(asmLine.getOperand1());
                opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, bitAddress);
            }
        }
        
        return opCode;
    }
}
