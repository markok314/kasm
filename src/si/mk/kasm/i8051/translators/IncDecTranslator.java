package si.mk.kasm.i8051.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.ExprParser;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates INC and DEC statements.
 * 
 * @author markok
 *
 */
public class IncDecTranslator extends TranslatorBase8051 {

    private Map<String, OpCode> m_instructions = new HashMap<String, OpCode>();
    private int m_highBits;
    
    public static final int HIGH_BITS_INC = 0;
    public static final int HIGH_BITS_DEC = 0x10;
    
    /**
     * 
     * @param highBits high bits of instructions, for example HIGH_BITS_INC or HIGH_BITS_DEC.
     */
    public IncDecTranslator(int highBits) {
        
        m_highBits = highBits;
        
        m_instructions.put("A", new OpCode(0x04 | highBits));
        m_instructions.put("@R0", new OpCode(0x06 | highBits));
        m_instructions.put("@R1", new OpCode(0x07 | highBits));
        
        if (highBits == HIGH_BITS_INC) {  // there is no command "DEC DPTR" 
            m_instructions.put("DPTR", new OpCode(0xa3));
        }
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 1);

        // if we get here, we can be sure, that cmd is one of INC or DEC
        String op1 = asmLine.getOperand1(); 

        OpCode opCode = null;
        
        int regIdx = getIndexOfRn(op1); 
        
        if (regIdx != NOT_RN) {
            // it is one of R0..R7
            opCode = new OpCode(0x08 | m_highBits);
            opCode.setRegister8(0, 0, regIdx);
        } else {
            opCode = m_instructions.get(op1);
            if (opCode == null) {
                // it is not one of instructions defined in ctor, but it is "CMD DATA_ADDR"
                opCode = new OpCode(0x05 | m_highBits, 0);
                opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op1);
            }
        }
        
        return opCode;
    }

}
