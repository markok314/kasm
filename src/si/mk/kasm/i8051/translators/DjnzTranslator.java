package si.mk.kasm.i8051.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AddressProvider;
import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates DJNZ.
 * 
 * @author markok
 *
 */
public class DjnzTranslator extends TranslatorBase8051 {

    
    /**
     * 
     */
    public DjnzTranslator() {
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 2);

        String op1 = asmLine.getOperand1();
        
        OpCode opCode = null;
        
        int regIdx = getIndexOfRn(op1); 
        String addr = asmLine.getOperand2();
        
        if (regIdx != NOT_RN) {
            // it is one of R0..R7
            opCode = new OpCode(0xd8 | regIdx, 0);
            // The following line assumes, that relative address is given as
            // label. It fails, if it is given as literal number specifying relative offset
            // TODO: pass addr to ExprParser without any labels defined (but with equ and constants). 
            // If it parses without exception to value inside range, it is literal offset. Otherwise it may be label.
            opCode.addArgExpression(1, 
                                    ArgumentSize.BITS_8_UNSIGNED, 
                                    addr  + " - 2 - " + asmLine.getAddress());
        } else {
            opCode = new OpCode(0xd5, 0, 0);
            // The following line assumes, that relative address is given as
            // label. It fails, if it is given as literal number specifying relative offset
            // TODO: pass addr to ExprParser without any labels defined (but with equ and constants). 
            // If it parses without exception to value inside range, it is literal offset. Otherwise it may be label.
            opCode.addArgExpression(1, 
                                    ArgumentSize.BITS_8_UNSIGNED, 
                                    op1);
            
            opCode.addArgExpression(2, 
                                    ArgumentSize.BITS_8_SIGNED, 
                                    addr  + " - 3 - " + asmLine.getAddress());
        }

        
        return opCode;
    }
}

