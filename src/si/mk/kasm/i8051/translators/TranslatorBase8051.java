package si.mk.kasm.i8051.translators;

import si.mk.kasm.ExprParser;
import si.mk.kasm.translators.TranslatorBase;

abstract class TranslatorBase8051 extends TranslatorBase {


    protected final int NOT_RN = -1;
    
    // protected ExprParser m_exprParser;

    TranslatorBase8051(/* ExprParser exprParser */) {
        super();
        // m_exprParser = exprParser;
    }

    /**
     * Returns index of Rn, or NOT_RN if the given parameter is not one of R registers.
     * 
     * @param asmParameter parameter of asm instruction
     * @return if asmParameter is one of R0..R7, its index is returned, this.NOT_RN otherwise
     */
    protected int getIndexOfRn(String asmParameter) {
        if (asmParameter.length() == 2  &&  asmParameter.charAt(0) == 'R') {
            int idx = asmParameter.charAt(1) - '0';
            if (idx >= 0  &&  idx < 8) {
                return idx;
            }
        }
        
        return NOT_RN;
    }
    
    
    /**
     * Processor 8051 has several instructions, which operate on bits. Bits in 
     * assembler are addressed as <ADDRESS>.<bit_number>, for example 'P4.5'.
     * This, so called bit addressing, is handled by replacing expression with
     * <ADDRESS>+<bit_number>, for example 'P4+5', which is later evaluated by 
     * expression parser. This method is called from translators for bit 
     * instructions.
     * 
     * @param operand
     * @return
     */
    protected String replaceDotInBitAddresses(String operand) {
        StringBuilder sb = new StringBuilder(operand);
        
        for (int i = 0; i < sb.length(); i++) {
            if (sb.charAt(i) == '.') {
                sb.setCharAt(i, '+');
            }
        }
        
        return sb.toString();
    }
    
    
    @Override
    protected boolean isIndirectAddressing(String operand) {
        // TODO Auto-generated method stub
        return false;
    }

}
