package si.mk.kasm.i8051.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.ExprParser;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates INC and DEC statements.
 * 
 * @author markok
 *
 */
public class MovTranslator extends TranslatorBase8051 {

    private Map<String, OpCode> m_instructions = new HashMap<String, OpCode>();

    /**
     * 
     */
    public MovTranslator() {
        
        m_instructions.put("A,#", new OpCode(0x74, 0));
        m_instructions.put("@R0,#", new OpCode(0x76, 0));
        m_instructions.put("@R1,#", new OpCode(0x77, 0));
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 2);

        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();

        // MOV <dest>, #data
        if (op2.charAt(0) == '#') {
            return immediateData(asmLine, op2.substring(1));
        }
        
        // MOV A, <src>
        if (op1.equals("A")) {
            return movRegReg(0xe0, op2); 
        }

        // MOV <dest>, A
        if (op2.equals("A")) {
            return movRegReg(0xf0, op1); 
        }

        // MOV C, bit
        if (op1.equals("C")) {
            return movCBit(0xa2, op2);
        }            

        if (op2.equals("C")) {
            return movCBit(0x92, op1);
        }            

        
        // MOV <dest>, direct
        // MOV direct, <dest>
        return movDirect(op1, op2);
        
        /*
        OpCode opCode = null;
        String op2 = asmLine.getOperand1();

        int regIdx = getIndexOfRn(op2); 
        
        if (regIdx != NOT_RN) {
            // it is one of R0..R7
            opCode = new OpCode(0x08 | m_highBits);
            opCode.setRegister8(0, 0, regIdx);
        } else {
            opCode = m_instructions.get(op2);
            if (opCode == null) {
                // it is not one of instructions defined in ctor, but it 
                // is "CMD A, DATA_ADDR" or "CMD A, #DATA"
                
                if (op2.charAt(0) == '#') {
                    opCode = new OpCode(0x04 | m_highBits, 0);
                    opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op2.substring(1));
                } else {
                    opCode = new OpCode(0x05 | m_highBits, 0);
                    opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op2);
                }
            }
        }
        
        return opCode; */
    }


    private OpCode movDirect(String op1, String op2) {

        OpCode opCode = null;
        String directAddr;
        
        int idx = getIndexOfRn(op1);
        if (idx != NOT_RN) {
            // MOV Rn, direct
            opCode = new OpCode(0xa8 | idx, 0);
            directAddr = op2;
        } else {
            idx = getIndexOfRn(op2);
            if (idx != NOT_RN) {
                // MOV direct, Rn
                opCode = new OpCode(0x88 | idx, 0);
                directAddr = op1;
            } else {
                // MOV @Ri, direct
                if (op1.equals("@R0")) {
                    opCode = new OpCode(0xa6, 0);
                    directAddr = op2;
                } else if (op1.equals("@R1")) {
                    opCode = new OpCode(0xa7, 0);
                    directAddr = op2;
                } else if (op2.equals("@R0")) {
                    // MOV direct, @Ri
                    opCode = new OpCode(0x86, 0);
                    directAddr = op1;
                } else if (op2.equals("@R1")) {
                    opCode = new OpCode(0x87, 0);
                    directAddr = op1;
                } else {
                    // MOV direct_dest, direct_src:  0x85, src, dest
                    opCode = new OpCode(0x85, 0, 0);
                    opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op2);
                    opCode.addArgExpression(2, ArgumentSize.BITS_8_UNSIGNED, op1);
                    return opCode;
                }
            }
        }
        
        opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, directAddr);
        
        return opCode;
    }


    private OpCode movRegReg(int highBits, String op) {

        OpCode opCode = null;
        
        int regIdx = getIndexOfRn(op); 

        if (regIdx != NOT_RN) {
            // it is one of R0..R7
            opCode = new OpCode(highBits | 0x08 | regIdx);
        } else {
            if (op.equals("@R0")) {
                opCode = new OpCode(highBits | 0x06);
            } else if (op.equals("@R1")) {
                opCode = new OpCode(highBits | 0x07);
            } else {
                // direct
                opCode = new OpCode(highBits | 0x05, 0);
                opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op);
            }
        }

        return opCode;
    }


    private OpCode movCBit(int opCodeCmd, String op) {
        OpCode opCode = new OpCode(opCodeCmd, 0);

        String bitAddress = replaceDotInBitAddresses(op);

        opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, bitAddress);
        
        return opCode;
    }


    private OpCode immediateData(AsmLine asmLine, String immData) {
        
        String op1 = asmLine.getOperand1();
        
        OpCode opCode = null;
   
        // MOV DPTR, #data
        if (op1.equals("DPTR")) {
            opCode = new OpCode(0x90, 0, 0);
            opCode.addArgExpression(1, ArgumentSize.BITS_16_BIG_ENDIAN, immData);
            return opCode;
        }
        
        int regIdx = getIndexOfRn(op1); 
        
        if (regIdx != NOT_RN) {
            // it is one of R0..R7
            opCode = new OpCode(0x78, 0);
            opCode.setRegister8(0, 0, regIdx);
        } else {
            String generalParams = op1 + ",#";
            opCode = newOpCode(m_instructions, generalParams);
            if (opCode == null) {
                // MOV direct, #data
                opCode = new OpCode(0x75, 0, 0);
                opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op1);
                opCode.addArgExpression(2, ArgumentSize.BITS_8_UNSIGNED, immData);
                return opCode;
            }
        }

        opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, immData);
        
        return opCode;
    }

}
