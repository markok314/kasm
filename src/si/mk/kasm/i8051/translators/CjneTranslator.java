package si.mk.kasm.i8051.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates CJNE statements.
 * 
 * @author markok
 *
 */
public class CjneTranslator extends TranslatorBase8051 {

    private Map<String, OpCode> m_instructions = new HashMap<String, OpCode>();
    
    /**
     * 
     */
    public CjneTranslator() {
        
        m_instructions.put("@R0", new OpCode(0xb6, 0, 0));
        m_instructions.put("@R1", new OpCode(0xb7, 0, 0));
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {
        
        checkPresenceOfOperands(asmLine, 3);

        String op1 = asmLine.getOperand1();
        String op2 = asmLine.getOperand2();
        
        OpCode opCode = null;
        
        int regIdx = getIndexOfRn(op1); 
        
        if (regIdx != NOT_RN) {
            // it is one of R0..R7
            opCode = new OpCode(0xb8 | regIdx, 0, 0);
            op2 = op2.substring(1);  // remove leading #
        } else if (op1.equals("A")) {
            if (op2.charAt(0) == '#') {
                opCode = new OpCode(0xb4, 0, 0);
                op2 = op2.substring(1);
            } else {
                opCode = new OpCode(0xb5, 0, 0);
            }
        } else {
            opCode = newOpCode(m_instructions.get(op1));
            op2 = op2.substring(1);
            if (opCode == null) {
                throw new AssemblerException("Invalid first operand: " + op1, asmLine);
            }
        }

        opCode.addArgExpression(1, ArgumentSize.BITS_8_UNSIGNED, op2);
        
        // The following line assumes, that cjne relative address is given as
        // label. It fails, if it is given as literal number specifying relative offset
        // TODO: pass addr to ExprParser without any labels defined (but with equ and constants). 
        // If it parses without exception to value inside range, it is literal offset. Otherwise it may be label.
        String addr = asmLine.getOperand(2);
        opCode.addArgExpression(2, 
                                ArgumentSize.BITS_8_SIGNED, 
                                addr  + " - 3 - " + asmLine.getAddress());
        
        return opCode;
    }
}
