package si.mk.kasm.i8051.translators;

import java.util.HashMap;
import java.util.Map;

import si.mk.kasm.AsmLine;
import si.mk.kasm.AssemblerException;
import si.mk.kasm.ExprParser;
import si.mk.kasm.translators.OpCode;
import si.mk.kasm.translators.OpCode.ArgumentSize;

/**
 * Translates instructions with constant parameters.
 * 
 * @author markok
 *
 */
public class SimpleInstructionTranslator extends TranslatorBase8051 {

    private Map<String, OpCode> m_instructions = new HashMap<String, OpCode>();
    
    /**
     * 
     */
    public SimpleInstructionTranslator() {
        
        m_instructions.put("NOP", new OpCode(0x00));
        m_instructions.put("RR A", new OpCode(0x03));
        m_instructions.put("RRC A", new OpCode(0x13));
        m_instructions.put("RET", new OpCode(0x22));
        m_instructions.put("RL A", new OpCode(0x23));
        m_instructions.put("RETI", new OpCode(0x32));
        m_instructions.put("RLC A", new OpCode(0x33));

        m_instructions.put("MOVC A,@A+PC", new OpCode(0x83));
        m_instructions.put("MOVC A,@A+DPTR", new OpCode(0x93));
        
        m_instructions.put("MOVX A,@DPTR", new OpCode(0xe0));
        m_instructions.put("MOVX A,@R0", new OpCode(0xe2));
        m_instructions.put("MOVX A,@R1", new OpCode(0xe3));
        
        m_instructions.put("DIV AB", new OpCode(0x84));
        m_instructions.put("MUL AB", new OpCode(0xa4));

        m_instructions.put("SWAP A", new OpCode(0xc4));
        
        m_instructions.put("DA A", new OpCode(0xd4));
        
        m_instructions.put("XCHD A,@R0", new OpCode(0xd6));
        m_instructions.put("XCHD A,@R1", new OpCode(0xd7));

        m_instructions.put("MOVX @DPTR,A", new OpCode(0xf0));
        m_instructions.put("MOVX @R0,A", new OpCode(0xf2));
        m_instructions.put("MOVX @R1,A", new OpCode(0xf3));
        
    }
    
    
    @Override
    public OpCode createOpCode(AsmLine asmLine) throws AssemblerException {

        if (asmLine.getCmd().equals("JMP")) {
            removeSpacesAroundPlus(asmLine, 1);
        } else if (asmLine.getCmd().equals("MOVC")) {
            removeSpacesAroundPlus(asmLine, 2);
        }
            

        String asmCmd = asmLine.toNormalizedCmdString(); 

        OpCode opCode = m_instructions.get(asmCmd);

        return opCode;
    }


    private void removeSpacesAroundPlus(AsmLine asmLine, int idx) {
        
        String op = asmLine.getOperand(idx);
        
        if (op != null) {
            // operands can not be strings for instructions handled by this 
            // class, so we are safe to remove any spaces around '+' (required 
            // for JMP and MOVC)
            op = op.replaceAll(" *\\+ *", "+");  
            asmLine.setOperand(idx, op);
        }
    }

}
