/*
  KAsm - assembler for Z80
  Copyright (C) 2008  Marko Klopcic

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


  You can contact me at Google mail with the following address: markok3.14
 */

package si.mk.kasm;

/**
 * This class specifies excptions, which also contain reference to instance of
 * AsmLine, which caused the exception.
 *  
 * @author markok
 *
 */
public class AssemblerException extends Exception {

    private AsmLine m_asmLine;

    public AssemblerException(String description, AsmLine asmLine) {
        super(description);
        
        m_asmLine = asmLine;
        asmLine.setErrorMsg(description);
    }

    public AsmLine getAsmLine() {
        return m_asmLine;
    }
}
