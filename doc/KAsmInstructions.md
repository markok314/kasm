<!-- This is HTML doc, but extension was changed to .md, because only this
way GitLab shows it as html when linked from README.md.
 -->
<html>
<body>

<center>
<p>&nbsp;
</p>
<p>&nbsp;
</p>
<p>&nbsp;
</p>
<h1>KAsm</h1>
<p>&nbsp;
</p>
<h2>Assembler for <br>
</h2>
<h2>Z80 and 8051 family of microcontrollers</h2>
<br>
<p>&nbsp;
</p>
<p>&nbsp;
</p>
<p>&nbsp;
</p>
<p>&nbsp;
</p>
<h1>User's Guide</h1>
ver. 1.1
<p>Author: Marko Klopcic
</p>
<p>&nbsp;
</p>
<p>&nbsp;
</p>
<p>&nbsp;
</p>
<p>&nbsp;
</p>
<p>&nbsp;
</p>
<p>&nbsp;
</p>
</center>
<hr>
<h1>License</h1>
<pre>  This program is free software; you can redistribute it and/or<br>  modify it under the terms of the GNU General Public License<br>  as published by the Free Software Foundation; either version 2<br>  of the License, or (at your option) any later version.<br><br>  This program is distributed in the hope that it will be useful,<br>  but WITHOUT ANY WARRANTY; without even the implied warranty of<br>  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the<br>  GNU General Public License for more details.<br><br>  You should have received a copy of the GNU General Public License<br>  along with this program; if not, write to the Free Software<br>  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.<br></pre>
<h1>Introduction</h1>
The main reason (requirement) for <b>KAsm</b> implementation was
compilation
of tons of existing Z80 assembler programs. The next requirement
defined the format
of two output files. The first one is LST file, which contains compiled
hex codes and source. The second output file should be in Intel Hex
format.
<p>Java was chosen as implementation language, because of high
productivity and portability. The speed is not a problem, since 22000
lines of assembler program
generating 93 kB Intel hex file is compiled in about 700 ms on Athlon
64, 3000+.
</p>
<p>The code is well commented and easy to customize, so it is easy to
adapt
syntax, produce additional output, or even implement assembler for
another
microprocessor.</p>
<p>The later statement has been proven in March 2010 (ver. 1.1), when
assembler for Intel's 8051 and its derivatives was implemented.
Furthermore three new directives were added also to Z80 assembler.</p>
<h1>Installation and Running</h1>
<p>Since KAsm is written in Java, it should run on any machine or
operating system, where Java runs. The distribution is compiled with
Java 1.6, but sources can also be compiled with Java 1.5, if needed.
<i>Ant</i> build file is included in the source distribution. <br>
</p>
<p>To use the program, unzip it to local disk. Then you can run it as
executable jar file, for example:</p>
<pre>$ java -jar /myprograms/kasm/KAsm.jar myProgram.asm<br><br><br></pre>
<h1>Usage</h1>
All command line arguments can be displayed with option -h. Version 1.1
prints the following text:
<pre><b>$ java -jar KAsm-1.1.jar -h</b><br><br>KAsm, Z80 Assembler<br>(c) Marko Klopcic, Mar 2009<br><br>Usage  : KAsm [options] &lt;sourceFileName&gt;<br>Examples: KAsm -f -c -e COMPILE_ALL=1 -e COMPILE_FLOAT=0 myZ80prog.asm<br>          KAsm -s 8051 -f -c -e COMPILE_ALL=1 -e COMPILE_FLOAT=0 my8051prog.asm<br>Options:<br>-h  --help         prints this help<br>-s  --processor    &lt;processorType&gt; : target processor. Currently 'z80' and<br>                                    '8051' are supported. If not<br>                                    specified, 'z80' is assumed.<br>-l  --lstname      &lt;lstFileName&gt; : name of LST output file.<br>                                   Default: &lt;filename&gt;.lst<br>-x  --hexname      &lt;hexFileName&gt; : name of HEX output file.<br>                                   Default: &lt;filename&gt;.hex<br>-b  --binname      &lt;binFileName&gt; : name of binary output file. If not specified,<br>                                   no binary file is created.<br>-c  --constants    add std constants to expression parser: 'e' and 'pi'<br>-f  --functions    add std functions to expression parser: ln, acos, atan,<br>                   asin, cos, sin, tan, sqrt, sum, log, add umin, rand<br>-p  --profile      prints time spent in each step of compilation<br>-r  --reserved     reserved words (reg. names) may be used for labels and<br>                   constants.<br>-e  --equ          &lt;const definition&gt; : define constant. There must be no spaces<br>                                        next to '='. Example: -e COMPILE_ALL=1<br>-t  --truncate     &lt;len of bin file&gt; : truncate bin file at the specified offset<br>                                       Example: -t 32768<br><br></pre>
If standard constants and/or functions are added to expression parser,
then they
may be used in expressions, for example:
<pre>        ld HL, AMPLITUDE * sin(pi/4) <br></pre>
If option <code>-r, --reserved</code> is not specified, then names of
registers are not allowed for labels and constants. For example, the
following line will
issue an error:
<pre>HL:     LD A, C<br></pre>
The option <code>-r, --reserved</code> should be used only for
compatibility reasons.
<h1>Assembler syntax</h1>
<a name="LabelsAndConstants">
<h2>Labels and constants</h2>
</a>Many rules for labels and constants defined with EQU directive, are
the same:
<ol>
  <li>All labels must be followed by ':', while for constants, which
are defind by EQU and similar directives,&nbsp; ':' is optional.<br>
  </li>
  <li>It is not required that labels and constants begin in column 1 -
they may be preceeded by spaces or tabs.</li>
  <li>Labels and constants are not case sensitive.</li>
  <li>Labels and constants must start with letter or underscore
character ('_'). They may contain letters, numbers, underscore and dot.</li>
  <li>Labels and constants may not be equal to register names.</li>
</ol>
<p>
<b>Examples</b></p>
<p>Valid definitions:
</p>
<pre>HEADER_LEN    equ 256<br>noOfPages:    equ 10<br>      LBL1:   ld A, 10<br>      <br>_LBL.2: <br>              ret<br></pre>
Invalid definitions:
<pre>#COMM_DELAY: EQU 100   ; invalid first character<br>LABEL        ld a, 5   ; label must be terminated with ':'<br>.LBL1:                 ; label must not start with '.'<br>9LBLA:                 ; label must not start with number<br>HL:                    ; HL is register, may not be used as a label<br></pre>
Definitions of constants may not include labels, which are defined
later in the program, because constants must
be evaluated in compilation pass 1.
<p>Labels may be defined only once, otherwise an error is reported. If
constant is
defined many times, only warning is reported, because sometimes one may
want to redefine
the constant (which should normally remain constant, that's why the
warning is issued).
</p>
<p> Constants may be defined with expressions, which may also include
other constants, but only those which are defined in lines before the
current one.</p>
<p><b>Examples</b></p>
<p>Valid constant definitions:</p>

<pre>TRUE          EQU 1<br>BUFFER_SIZE   EQU 128<br>SECTION_SIZE  EQU BUFFER_SIZE/8<br></pre>
Invalid constant definitions
<pre>CHECKSUM_OFFSET: EQU HEADER_LEN - 1  ; HEADER_LEN not defined before this line <br>HEADER_LEN: EQU 100   <br></pre>
<h2>Comments</h2>
Comments start with ';' and extend to the end of the line.
<p><b>Examples</b>
</p>
<pre>; comment line<br>  ld a, 20  ; comment<br>lbl1:  ; comment<br></pre>
<h2>Literal constants</h2>
<h3>Numbers</h3>
Numbers may be written in binary, decimal, and hexadecimal notation.
Binary numbers may contain only 0s and 1s and the last character must
be 'B' or 'b'.
Hexadecimal numbers must begin with a number character (0-9) and end
with character 'H' or 'h'.
<b>Examples</b>
<pre>10   - decimal number ten<br>10B  - binary number two<br>10H  - hexadecimal number sixteen<br>0ffh - hexadecimal number (255). Note the leading 0, which must be present, because the <br>       number would otherwise start with a letter<br></pre>
<h3>Charactes</h3>
Characters are decoded as 8 bit ASCII values. They must be written
between single or double quotes.
<p> <b>Examples</b>
</p>
<pre>   'a' - single qotes<br>   "B" - double quotes<br></pre>
<h3>Strings</h3>
Strings can be used only as arguments of DB directive. They are written
as a set
of characters between single or double quotes. Each character in a
string compiles
as one 8-bit value. Escaping is not supported, so if you want to write
double quote character in a string, then use single quotes as string
delimiters,
and the other way around.
<p><b>Examples</b>
</p>
<pre>   'He said: "Hello!"'<br>   "Hi there! I'm letter 'A'!"<br></pre>
<h2>Expressions</h2>
Expressions can be used anywhere, where literal value can be used:
<ul>
  <li>when loading value into register</li>
  <li>when specifying address</li>
  <li>when defining constant</li>
  <li>when specifying index in indexed addressing</li>
  <li>in DB and DW directives</li>
</ul>
<b>Examples</b>
<pre>              ld HL, 10*205 +3<br>              ld a, (HELLO_TEXT + 3)<br>NO_OF_ITEMS  equ 100*3        <br>               <br>              ld  a, (ix + HEADER_LEN * 2)<br>              db  ' ' + 20 * 3<br></pre>
<h2>Compiler directives</h2>
<h3>&lt;constant&gt; EQU &lt;expr&gt;<br>
</h3>
This directive is used to define constants, which may be used in
expressions as
arguments of assembler instructions, or for conditional compilation.
See section
<a href="#LabelsAndConstants">Labels and Constants</a> for detailed
description and
examples.
<h3>ORG &lt;expr&gt;<br>
</h3>
This directive specifies the memory address, where instructions
following it should be compiled.
<p><b>Example:</b>
</p>
<pre>        org 250<br>        ld A, 10 ; this instruction will generate code starting at address 250<br></pre>
<h3>END</h3>
This directive marks the end of the program. Everything after line with
this directive is ignored. This directive is not mandatory. If not
present, everyting to the end of the file is compiled.
<h3>IF &lt;expr&gt;, ELSEIF &lt;expr&gt;, ELSE, ENDIF</h3>
These three directives can be used for conditional compilation. It is
also possible to use nested IF, ELSEIF, ELSE, ENDIF directives.
If statement is executed, if the expression evaluates to non-zero value.<br>
<b><br>
Example:</b>
<pre>isFloatingPoint EQU 1  <br>mode EQU 2<br><br>        ...<br>        IF isFloatingPoint<br>           IF mode = 1<br>               call CONFIGURE_MODE_1<br>           ELSEIF mode = 2<br>               call CONFIGURE_MODE_2<br>           ELSEIF mode = 3<br>               call CONFIGURE_MODE_3<br>           ELSE<br>               call CONFIGURE_DEFAULT_MODE<br>          &nbsp;ENDIF<br>        ENDIF<br>        ...<br>            <br></pre>
<h3>DB &lt;bytes&gt;, DW &lt;words&gt;<br>
</h3>
These two directives can be used to define arbitrary data used by the
program.
Arguments must be separated by commas. They can be anything from
literal constants to characters, expressions, or strings (<code>DB</code>
only).
<p>The number of items in DB directive is limited to 512, for DW it is
256. This should be more than enough, otherwise the internal buffer
should be increased and
the program recompiled (see <code>OpCode.MAX_BYTES_PER_ASM_LINE</code>).
</p>
<p><b>Example:</b>
</p>
<pre>        DB 42, 'b', "hello!", ' ' + 34<br>        DW 42, 'b', ' ' + 34<br></pre>
<h3>DS &lt;expr&gt;<br>
</h3>
This directive tells compiler to leave &lt;expr&gt; number of bytes of
uninitialized space. <br>
<span style="font-weight: bold;">Example:</span><br>
<pre>&nbsp;&nbsp;&nbsp;     DS&nbsp; 20&nbsp; <br><br></pre>
<h3>$INCLUDE (&lt;filename&gt;)</h3>
This directive includes other file. Instructions and directives are
inserted into this file and compiled. It has the same effect as manual
copy-paste operation. Nested includes are allowed. &lt;filename&gt; may
specify path to include file in relative or absolute way. Relative
paths should be relative to current directory, not relative to
assembler file.<br>
<span style="font-weight: bold;">Example:</span><br>
<pre>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $include (../src/regs500.i)</pre>
<br>

<h1>Z80 assembler specifics<br>
</h1>
<h2>RST instructions <br>
</h2>
Arguments of RST instructios may be given as:
<ul>
  <li>literal value, for ecample: <code>RST 08h</code></li>
  <li>expression of constants defined by <code>EQO</code>, for
example: <code>RST VECTOR_8</code></li>
  <li>label defined <b>before</b> line with <code>RST</code>
instruction. This can be used to detect invalid routine address, for
example:
    <p> <code>RST LBL_VECTOR_A</code> </p>
    <p>will report an error if <code>LBL_VECTOR_A</code> is not a
valid address for <code>RST</code> instruction. </p>
  </li>
</ul>
<p><br>
</p>
<h1>8051 assembler specifics</h1>
<h2>Constants</h2>
Constants in 8051 can be defined also with directives DATA, IDATA,
XDATA, CODE and BIT. However, all constants are definfed in the same
namespace so there is no difference between those constants, and
constants defined with EQU directive.<br>
<br>
<h3>JMP and CALL instructions</h3>
8051 familiy of microprocessors has three instructions for jumps -
SJMP, AJMP, and LJMP. When compiler sees instruction JMP, if first
checks if label is laready defined. If it is, and it is less than 128
bytes away, SJMP is used. Otherwise the compiler checks if the address
is located in the same 2k blok. If it is, AJMP instruction is used. If
even that is not tha ces, LJMP is used. <br>
If label is not defined yet (it is forward jump), then LJMP is always
used. <br>
According to these rules, it is recommended to use JMP instruction for
backward jumps, while for forward jumps the programmer sbould use the
best instruction.<br>
<p><br>
</p>
<h1>Eclipse Error Parser</h1>
To configure Eclipse error parser, use the following regular
expression:

 <pre>
   Error \((.*):(.*)\):(.*)
 </pre>

Enter <CODE>$1</CODE>, <CODE>$2</CODE>, and <CODE>$3</CODE> for
columns <i>File</I>, <I>Line</I> and <I>Description</I> respectively.
  
<br>
<p>&nbsp;
</p>
<hr>
<p>&nbsp;
(c) Marko Klopcic, 2009, 2010<br>
</p>


</body>
</html>

