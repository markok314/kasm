Date        Version

2012-01-27   1.1.4 
    Invalid instruction 
      LD   (LBL_A), LBL_B
    did not report any error - no op. code was generated. Error is reported now.

2010-06-05   1.1.3
    'call' instructions were not recognized. Now they compile either
    to 'acall' or 'lcall', depending on address range.
    

2010-04-19   1.1.2
    Assembler type in the first printed line after startup was always Z80,
    regardless of actual type. Fixed, so that the first printed line now
    contains correct assembler type.   
