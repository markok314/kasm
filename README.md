# KAsm

KAsm is assembler for Z80 and 8051 family of microprocessors. 

To build it, you need Java Development Kit installed on your machine and _ant_. Use `build.sh` as build script.

More info: [Manual](doc/KAsmInstructions.md)

